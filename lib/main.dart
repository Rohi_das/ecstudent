import 'package:ec_student/providers/great_place.dart';
import 'package:ec_student/screens/booking_screen/transaction_details_booked.dart';
import 'package:ec_student/screens/booking_screen/transaction_details_confirmed.dart';
import 'package:ec_student/screens/booking_screen/transaction_details_rejected.dart';
import 'package:ec_student/screens/error_screen/data_not_found.dart';
import 'package:ec_student/screens/error_screen/no_connection.dart';
import 'package:ec_student/screens/others_screen/about_us.dart';
import 'package:ec_student/screens/others_screen/onboarding_screen.dart';
import 'package:ec_student/screens/others_screen/privacy_policy.dart';
import 'package:ec_student/screens/others_screen/term_of_use.dart';
import 'package:ec_student/screens/payments_screen/payment_details.dart';
import 'package:ec_student/screens/payments_screen/payments.dart';
import 'package:ec_student/screens/profile_screen/add_student.dart';
import 'package:ec_student/screens/profile_screen/student_profile.dart';
import 'package:ec_student/screens/profile_screen/update_profile.dart';
import 'package:ec_student/screens/refer_and_earn/refer_and_earn.dart';
import 'package:ec_student/screens/rewards_offers_screens/offers.dart';
import 'package:ec_student/screens/rewards_offers_screens/rewards_and_offers.dart';
import 'package:ec_student/screens/search_screen/search_categories.dart';
import 'package:ec_student/screens/search_screen/on_map_search_screen.dart';
import 'package:ec_student/screens/search_screen/select_category.dart';
import 'package:ec_student/screens/search_screen/user_location_and_address.dart';
import 'package:ec_student/widgets/calendar/calendar.dart';
import 'package:ec_student/widgets/student_dashboard/booked_teachers.dart';
import 'package:ec_student/widgets/student_dashboard/cart.dart';
import 'package:ec_student/widgets/student_dashboard/upcoming.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './screens/booking_screen/booking.dart';
import './screens/help_and_support.dart/faq.dart';
import './screens/help_and_support.dart/help_support.dart';
import './screens/watch_videos_screen/orientation.dart';
import './screens/watch_videos_screen/useful_info.dart';
import './widgets/drawer/student_drawer.dart';
import './screens/tab_screens/calender_screen.dart';
import './screens/tab_screens/tab_screen.dart';
import './screens/tab_screens/cart_screen.dart';
import './screens/tab_screens/profile_screen.dart';
import './screens/login_screens/login.dart';
import './screens/login_screens/signup.dart';
import './screens/login_screens/otp.dart';
import './screens/login_screens/forgot_password.dart';
import './screens/login_screens/mobile_no_verificaion.dart';
import 'package:shared_preferences/shared_preferences.dart';
void main() {
  SharedPreferences.setMockInitialValues({
    
  });
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Color c = const Color(0xFF629AFB);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: GreatPlace(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'educhamp',

        theme: ThemeData(
          primaryColor: Color(0xFF629AFB),
          // primarySwatch: Color(0xFFE376BD),0xFFAD5CA9
          canvasColor: Color(0xFFF5F5F5),
          accentColor: Colors.white,
          hintColor: Colors.grey,
          buttonColor: Color(0xFFFD974D),
          disabledColor: Colors.grey[500],
          primaryColorDark: Colors.blue[800],
          textSelectionColor: Colors.blue[800],
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),

        initialRoute: '/',
        routes: {
          '/': (context) => Login(),
          TabScreen.routeName: (context) => TabScreen(),
          CalenderScreeen.routeName: (context) => CalenderScreeen(),
          CartScreen.routeName: (context) => CartScreen(),
          ProfileScreen.routeName: (context) => ProfileScreen(),
          Login.routeName: (context) => Login(),
          Signup.routeName: (context) => Signup(),
          OTP.routeName: (context) => OTP(),
          ForgotPassword.routeName: (context) => ForgotPassword(),
          MobileNoVerification.routeName: (context) => MobileNoVerification(),
          StudentDrawer.routeName: (context) => StudentDrawer(),
          RewardsAndOffers.routeName: (context) => RewardsAndOffers(),
          Offers.routeName: (context) => Offers(),
          UsefulInformation.routeName: (context) => UsefulInformation(),
          OrientationScreen.routeName: (context) => OrientationScreen(),
          FAQ.routeName: (context) => FAQ(),
          HelpAndSupport.routeName: (context) => HelpAndSupport(),
          Booking.routeName: (context) => Booking(),
          TransactionDetailsBooked.routeName: (context) =>
              TransactionDetailsBooked(),
          TransactionDetailsConfimed.routeName: (context) =>
              TransactionDetailsConfimed(),
          TransactionDetailsRejected.routeName: (context) =>
              TransactionDetailsRejected(),
          Payments.routeName: (context) => Payments(),
          PaymentDetails.routeName: (context) => PaymentDetails(),
          ReferAndEarn.routeName: (context) => ReferAndEarn(),
          StudentProfile.routeName: (context) => StudentProfile(),
          AboutUs.routeName: (context) => AboutUs(),
          PrivacyPolicy.routeName: (context) => PrivacyPolicy(),
          TermOfUse.routeName: (context) => TermOfUse(),
          DataNotFound.routeName: (context) => DataNotFound(),
          NoConnection.routeName: (context) => NoConnection(),
          SearchScreen.routeName: (context) => SearchScreen(),
          OnbordingScreen.routeName: (context) => OnbordingScreen(),
          UserLocationAndAddress.routeName: (context) =>
              UserLocationAndAddress(),
          AddStudent.routeName:(context)=>AddStudent(),
          SearchCategories.routeName:(context)=>SearchCategories(),
          SelectCategory.routeName:(context)=>SelectCategory(),
          Upcoming.routeName:(context)=>Upcoming(),
          BookedTeacher.routeName:(context)=>BookedTeacher(),
          Calendar.routeName:(context)=>Calendar(),
          Cart.routeName:(context)=>Cart(),
          UpdateProfile.routeName:(context)=>UpdateProfile(),
        },
        // home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

