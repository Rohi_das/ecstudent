import 'package:ec_student/providers/dummy_data.dart';
import 'package:ec_student/widgets/layouts/app_bar_back_button.dart';
import 'package:ec_student/widgets/layouts/app_bar_title.dart';
import 'package:ec_student/widgets/layouts/side_screen.dart';
import 'package:flutter/material.dart';
class Cart extends StatefulWidget {
  static const routeName='/cart-detail';
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  final data=currentTeacher;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Your Cart'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top,
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 50,
                left: 15,
              ),
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(),
                  height: MediaQuery.of(context).size.height * 0.80,
                  child: ListView.builder(
                    itemCount: data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        elevation: 2.0,
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0, right: 10, top: 20, bottom: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    width: 80,
                                    height: 80,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image:
                                                NetworkImage(data[index].image),
                                            fit: BoxFit.fill)),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Text(
                                    data[index].studentName,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              // VerticalDivider(
                              //   width: 10,
                              //   thickness: 2,
                              //   color: Colors.red[350],
                              //   indent: 20.0,
                              //   endIndent: 23.0,
                              // ),
                              Padding(
                                padding: const EdgeInsets.only(top: 15,bottom: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Subject : ' + data[index].subject,
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(
                                      'Class Type : ' + data[index].classType,
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(
                                      'End Date: ' + data[index].endDate,
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w500),
                                    ),
                                   
                                   
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 15,bottom: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                     Text(
                                      'Days: ' + data[index].days,
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(
                                      'Time: ' + data[index].time,
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    
                                    // Padding(
                                    //   padding: const EdgeInsets.all(8.0),
                                    //   child: SizedBox(
                                    //     height: 20,
                                    //                                       child: FlatButton(
                                    //       onPressed: () {},
                                    //       child: Text(
                                    //         'VIEW PROFILE',
                                    //         style: TextStyle(
                                    //             fontSize: 12.0,
                                    //             fontWeight: FontWeight.bold,
                                    //             color: Colors.blue[900]),
                                    //       ),
                                    //     ),
                                    //   ),
                                    // ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}