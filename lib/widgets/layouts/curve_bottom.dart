import 'package:flutter/material.dart';
class CurveBottom extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    // return Stack(
    //   children: [
    //     Container(
    //       width: 5,
    //       height: 5,
    //       color: Theme.of(context).primaryColor,
    //       // color: Colors.transparent,
          
    //     ),
    //     Container(
    //       width: 5,
    //       height: 5,
    //       decoration: BoxDecoration(
    //         color: Theme.of(context).accentColor,
    //         borderRadius: BorderRadius.only(bottomLeft: Radius.circular(25), topRight: Radius.circular(0),),
    //       ),
    //     )
    //   ],
    // );

        return Container(
          height: 5.0,
          width: 5.0,
          color: Theme.of(context).primaryColor,
          child: new Container(
            decoration: new BoxDecoration(
              color: Theme.of(context).accentColor,
              borderRadius: BorderRadius.only(bottomRight: Radius.circular(0), topLeft: Radius.circular(25),),
            ),
            
         ),
        );
  }
}