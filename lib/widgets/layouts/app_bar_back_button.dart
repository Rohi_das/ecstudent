import 'package:ec_student/widgets/drawer/student_drawer.dart';
import 'package:flutter/material.dart';

class AppBarBackButton extends StatelessWidget {
  final bool _isSignup;
  final bool _isMenuIcon;
  AppBarBackButton(this._isSignup, this._isMenuIcon);
  @override
  Widget build(BuildContext context) {

    return ButtonTheme(
      minWidth: 50,
      child: FlatButton(padding: EdgeInsets.only(left: 8.0),
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0)),
        child: _isMenuIcon? 
        Icon(
          Icons.menu,
          size: 20.0,
          color: _isSignup?Theme.of(context).primaryColor :Theme.of(context).accentColor,
        )
        :Icon(
          Icons.arrow_back,
          size: 30.0,
          color: _isSignup?Theme.of(context).primaryColor :Theme.of(context).accentColor,
        ),
        color: _isSignup?Theme.of(context).canvasColor:Theme.of(context).primaryColor,
        textColor: Colors.white,
        onPressed: () {

          _isMenuIcon? Scaffold.of(context).openDrawer():Navigator.of(context).pop();
        },
      ),
    );
  }
}
