import 'package:flutter/material.dart';

class SideScreen extends StatelessWidget {
  final bool isCanvas;

  SideScreen( this.isCanvas);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 15,
      height: MediaQuery.of(context).size.height * .8,
      decoration: BoxDecoration(
        color: isCanvas?Theme.of(context).primaryColor:Theme.of(context).canvasColor,
        borderRadius: BorderRadius.only(
          topRight: Radius.elliptical(100, 150),
          bottomRight: Radius.elliptical(100, 150),
        ),
      ),
    );
  }
}
