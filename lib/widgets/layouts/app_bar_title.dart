import 'package:flutter/material.dart';

class AppBarTitle extends StatelessWidget {
  final bool _isSignup;
  final String _title;
  AppBarTitle(this._isSignup, this._title);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      color: _isSignup?Theme.of(context).primaryColor:Theme.of(context).accentColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(_title, style: TextStyle(fontWeight:FontWeight.bold, fontSize: 18.0),),
        ],
      ),
    );
  }
}