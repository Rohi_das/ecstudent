import 'package:flutter/material.dart';

class CommonButton extends StatelessWidget {

  final String text;
  final Function opration;
  final bool isEnabled;
  CommonButton(this.text, this.opration, this.isEnabled);
  
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(text, style: TextStyle(color:Theme.of(context).accentColor),),
      color: Theme.of(context).buttonColor,
      elevation: 10,
      shape: StadiumBorder(),
      onPressed:isEnabled? opration:null,
    );

  }
}
