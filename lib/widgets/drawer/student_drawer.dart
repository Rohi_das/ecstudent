import 'package:ec_student/providers/dummy_data.dart';
import 'package:ec_student/widgets/drawer/student_drawer_followus.dart';
import 'package:ec_student/widgets/drawer/student_drawer_header.dart';
import '../drawer/student_drawer_body.dart';
import 'package:flutter/material.dart';

class StudentDrawer extends StatefulWidget {
  static const routeName = '/drawer';
  @override
  _StudentDrawerState createState() => _StudentDrawerState();
}

class _StudentDrawerState extends State<StudentDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top:MediaQuery.of(context).padding.top),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
        child: Drawer(
        
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              
              StudentDrawerHeader(),
              StudentDrawerBody(),
              SizedBox(
                height: MediaQuery.of(context).size.height*0.14,
              ),
              StudentDrawerFollowus()
            ],
          ),
        ),
      ),
    );
  }
}
