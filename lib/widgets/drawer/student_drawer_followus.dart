import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../providers/dummy_data.dart';


class StudentDrawerFollowus extends StatelessWidget {
  _launchURL(index) async {
    
    String url = launchUrl[index];
    print(url);
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }




  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Text('Follow us :'),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 30.0, left: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              InkWell(
                onTap: () {
                  _launchURL(0);
                },
                child: FaIcon(
                  FontAwesomeIcons.youtube,
                  color: Color(0xFFFF0000),
                  size: 26,
                ),
              ),
              InkWell(
                onTap: () {
                  _launchURL(1);
                },
                child: FaIcon(
                  FontAwesomeIcons.facebookSquare,
                  color: Color(0xFF3B5998),
                  size: 26,
                ),
              ),
              InkWell(
                onTap: () {
                  _launchURL(2);
                },
                child: FaIcon(
                  FontAwesomeIcons.instagramSquare,
                  color: Color(0xFF3F729B),
                  size: 26,
                ),
              ),
              InkWell(
                onTap: () {
                  _launchURL(3);
                },
                child: FaIcon(
                  FontAwesomeIcons.twitterSquare,
                  color: Color(0xFF00ACEE),
                  size: 26,
                ),
              ),
              InkWell(
                onTap: () {
                  _launchURL(4);
                },
                child: FaIcon(
                  FontAwesomeIcons.pinterest,
                  color: Color(0xFFE60023),
                  size: 26,
                ),
              ),
              InkWell(
                onTap: () {
                  _launchURL(5);
                },
                child: FaIcon(
                  FontAwesomeIcons.linkedin,
                  color: Color(0xFF0E76A8),
                  size: 26,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
