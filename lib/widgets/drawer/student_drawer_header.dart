import 'package:ec_student/screens/profile_screen/student_profile.dart';
import 'package:flutter/material.dart';

class StudentDrawerHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140,
      padding: EdgeInsets.zero,
      child: DrawerHeader(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
          color: Color(0xFFE376BD),
        ))),
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(35.0),
                        child: Image.asset(
                          'assets/login.jpg',
                          height: 70.0,
                          width: 70.0,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 3.0),
                            child: Text(
                              'Name',
                              style: TextStyle(fontSize: 18),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 3.0),
                            child: Text(
                              'Mobile No',
                            ),
                          ),
                          Text('Email'),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  width: 40,
                  child: RaisedButton(
                    color: Theme.of(context).primaryColor,
                    onPressed: () {
                      Navigator.of(context).pushNamed(StudentProfile.routeName);
                    },
                    child: Icon(
                      Icons.edit,
                      size: 20.0,
                      color: Theme.of(context).accentColor,
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      bottomLeft: Radius.circular(25),
                    )),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Row(
                children: [
                  Text(
                    'Student Code : ',
                    style: TextStyle(fontWeight: FontWeight.w400),
                  ),
                  Text('12121545'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
