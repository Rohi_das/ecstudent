import 'package:flutter/material.dart';

import 'package:ec_student/providers/dummy_data.dart';

class StudentDrawerBody extends StatefulWidget {
  @override
  _StudentDrawerBodyState createState() => _StudentDrawerBodyState();
}

class _StudentDrawerBodyState extends State<StudentDrawerBody> {
  @override
  Widget build(BuildContext context) {
    return Column(

      children: [
      Column(
        children: drawer.map((d) {
          return SizedBox(
            height: 40,
            child: ListTile(
              dense: false,
              title: Row(
                children: <Widget>[
                  Icon(d.icon),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(d.title),
                  ),
                 
                ],
              ),
              onTap: () {
                Navigator.of(context).pushNamed(d.routeName);
              },
            ),
          );
        }).toList(),
      ),
     
      
    ]);
  }
}
