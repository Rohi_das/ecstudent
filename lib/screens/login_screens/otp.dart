import 'dart:convert';

import 'package:ec_student/providers/apiCall.dart';
import 'package:ec_student/screens/login_screens/forgot_password.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/side_screen.dart';
import '../../screens/login_screens/login.dart';
import '../../widgets/layouts/common_button.dart';
import '../../screens/tab_screens/tab_screen.dart';

class OTP extends StatefulWidget {
  static const routeName = '/otp';

  @override
  _OTPState createState() => _OTPState();
}

class _OTPState extends State<OTP> {
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _otpForm = GlobalKey<FormState>();
   final _otp = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    final Map _arguments = ModalRoute.of(context).settings.arguments as Map;
    final bool _isForgotPassword = _arguments['isForgotPassword'];
    final String _mobileNo = _arguments['mobile_no'];
    print(_isForgotPassword);
    _verifyOTP() async {
      print('Verify OTP Called........');
      final _isValid = _otpForm.currentState.validate();
      if (!_isValid) {
        return;
      }
      _otpForm.currentState.save();
      print(_isForgotPassword);
    final String url = _isForgotPassword?'api/sign_up_otp':'api/otp_forgot_password';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final data = jsonEncode(<String, dynamic>{
      'token': prefs.getString('token'),
      'otp': _otp.text,
    });

    final response = await postData(url, data, false);
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      SharedPreferences prefs = await SharedPreferences.getInstance();
 
      await prefs.setString('token', data['token']);

      _isForgotPassword
          ? Navigator.of(context)
              .pushNamed(Login.routeName)
          : Navigator.of(context)
              .pushNamed(ForgotPassword.routeName);
    } else {
      final data = jsonDecode(response.body);
      print(data);
      final snackBar = SnackBar(
        content: Text(
          data['message'], textAlign: TextAlign.center,
          // style: TextStyle(color: Colors.red),
        ),
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    }

    Future<void> _resendOTP() async {
      final String url = 'api/forgot_password';

      final data = jsonEncode(<String, dynamic>{
        'mobile_number': _mobileNo,
        'login_for':1
      });

      final response = await postData(url, data, false);

      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        print(data);
        SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('token', data['token']);
      } else {
        final data = jsonDecode(response.body);
        final snackBar = SnackBar(
          content: Text(
            data['message'], textAlign: TextAlign.center,
            // style: TextStyle(color: Colors.red),
          ),
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    }

    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            Container(
              // margin: EdgeInsets.only( top:MediaQuery.of(context).padding.top),
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/educhamp.png',
                          width: 240, alignment: Alignment.center,
                          // height: 100,
                        ),
                      ],
                    ),
                  ),
                  AppBarBackButton(false, false),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 50),
                    height: 420,
                    padding: EdgeInsets.symmetric(
                      horizontal: 60.0,
                    ),
                    child: Form(
                      key:_otpForm,
                                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/login.jpg',
                            width: 120,
                            height: 120,
                          ),
                          Text(
                            'Verification',
                            style: TextStyle(
                              fontSize: 30,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'You will get a OTP via sms',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(
                                'on your mobile no '+ _mobileNo,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          TextFormField(
                            controller: _otp,
                            validator: (value) {
                              print('object');
                              if (value.isEmpty) {
                                return 'OTP required';
                              }

                              return null;
                            },
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              hintText: "Enter OTP here",
                              isDense: true,
                              contentPadding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(50),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(50),
                                borderSide: const BorderSide(
                                    color: Color(0xFFC77DC1), width: 1.5),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(50),
                                borderSide:
                                    BorderSide(color: Colors.red, width: 1.5),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(50),
                                borderSide:
                                    BorderSide(color: Colors.red, width: 1.5),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 130,
                            child: CommonButton('VERIFY', _verifyOTP,true),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Didn't receive OTP code?",
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              InkWell(
                                child: Text(
                                  'Resend OTP',
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor),
                                ),
                                onTap: _resendOTP,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SideScreen(true),
          ],
        ),
      ),
    );
  }
}
