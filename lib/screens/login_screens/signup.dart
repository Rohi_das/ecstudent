import 'dart:convert';

import 'package:ec_student/screens/login_screens/otp.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../models/user_info/user_signup.dart';
import '../../providers/apiCall.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/side_screen.dart';
import '../../widgets/layouts/common_button.dart';
import '../../screens/login_screens/login.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
    'https://www.googleapis.com/auth/user.phonenumbers.read'
  ],
);

class Signup extends StatelessWidget {
  static const routeName = '/signup';
  final _signupForm = GlobalKey<FormState>();
  final _fnameCtrl = TextEditingController();
  final _lnameCtrl = TextEditingController();
  final _mobileNumberCtrl = TextEditingController();
  final _emailCtrl = TextEditingController();
  final _passwordCtrl = TextEditingController();

    GoogleSignInAccount _currentUser;

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
      print(_googleSignIn.currentUser.displayName);
      print(_googleSignIn.currentUser.email);
      print(_googleSignIn.currentUser.photoUrl);
    } catch (error) {
      print(error);
    }
  }
  @override
  Widget build(BuildContext context) {
    _submit() async {
      print('Submit Called.....');
      final _isValid = _signupForm.currentState.validate();
      if (!_isValid) {
        return;
      }
      _signupForm.currentState.save();
      // String fName=_fnameCtrl.text.replaceAll(new RegExp(r"\s+\b|\b\s"), "")
      UserSignup signupData = UserSignup(
          name: _fnameCtrl.text+ ' ' + _lnameCtrl.text,
          mobileNumber: _mobileNumberCtrl.text,
          email: _emailCtrl.text,
          password: _passwordCtrl.text,
          loginFor: '1');
      final String url = 'api/sign_up';
      try {
        String data = jsonEncode(signupData);
        final response = await postData(url, data,false);
        if (response.statusCode == 200) {
          print(jsonDecode(response.body));
          final data = jsonDecode(response.body);
          SharedPreferences prefs = await SharedPreferences.getInstance();
                await prefs.setString('token', data['token']);
                    Navigator.of(context).pushNamed(
                  OTP.routeName,
                  arguments: <String, dynamic>{'isForgotPassword': true,'mobile_no': _mobileNumberCtrl.text,},
                );
                  } else {
                    throw Exception('Failed to load album');
                  }
                } catch (error) {
                  print(error);
                }
          
                
              }
          
              _borders(hintText, iconName) {
                return InputDecoration(
                  hintText: hintText,
                  isDense: true,
                  contentPadding: EdgeInsets.fromLTRB(10, 30, 10, 0),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: const BorderSide(color: Color(0xFFC77DC1), width: 1.5),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: const BorderSide(color: Colors.red, width: 1.5),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: const BorderSide(color: Colors.red, width: 1.5),
                  ),
                  prefixIcon: Icon(
                    iconName,
                    color: Colors.grey,
                  ),
                );
              }
          
              return Scaffold(
                body: SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                    height: MediaQuery.of(context).size.height,
                    color: Theme.of(context).primaryColor,
                    child: Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).padding.top + 5.0),
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/educhamp.png',
                                      width: 240, alignment: Alignment.center,
                                      // height: 100,
                                    ),
                                  ],
                                ),
                              ),
                              AppBarBackButton(true, false),
                            ],
                          ),
                        ),
                        SideScreen(false),
                        Container(
                          height: MediaQuery.of(context).size.height -
                              MediaQuery.of(context).padding.top,
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).padding.top + 60),
                          child: Form(
                            key: _signupForm,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'assets/login.jpg',
                                  width: 120,
                                  height: 100,
                                ),
                                Text(
                                  'For all your needs in School & College',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context).accentColor),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 30),
                                  padding: EdgeInsets.only(
                                    left: 20.0,
                                    right: 34,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).canvasColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      bottomLeft: Radius.circular(10),
                                    ),
                                  ),
                                  height: 400,
                                  // width: double.infinity,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            'Already have an account?',
                                            style: TextStyle(fontSize: 12.0),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          SizedBox(
                                            height: 20,
                                            width: 100,
                                            child: OutlineButton(
                                              child: Text(
                                                'Login here',
                                                style: TextStyle(
                                                  color: Theme.of(context).primaryColor,
                                                ),
                                              ),
                                              shape: StadiumBorder(),
                                              onPressed: () {
                                                Navigator.of(context)
                                                    .pushNamed(Login.routeName);
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        children: [
                                          Expanded(
                                            child: TextFormField(
                                              controller: _fnameCtrl,
                                              validator: (value) {
                                                String newData=value.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
                                                RegExp regex = new RegExp(r'^[a-zA-Z]+$');
                                                if (value.isEmpty) {
                                                  return 'First name required';
                                                }
                                                if (!(regex.hasMatch(newData))) {
                                                  return 'Enter valid first name';
                                                }
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                hintText: "First Name",
                                                isDense: true,
                                                contentPadding:
                                                    EdgeInsets.fromLTRB(20, 30, 0, 0),
                                                enabledBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(50),
                                                  borderSide: const BorderSide(
                                                      color: Colors.grey, width: 1.0),
                                                ),
                                                focusedBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(50),
                                                  borderSide: const BorderSide(
                                                      color: Color(0xFFC77DC1),
                                                      width: 1.5),
                                                ),
                                                focusedErrorBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(50),
                                                  borderSide: const BorderSide(
                                                      color: Color(0xFFC77DC1),
                                                      width: 1.5),
                                                ),
                                                errorBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(50),
                                                  borderSide: const BorderSide(
                                                      color: Colors.red, width: 1.5),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                            child: TextFormField(
                                              controller: _lnameCtrl,
                                              validator: (value) {
                                                String lastData=value.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
                                                RegExp regex = new RegExp(r'^[a-zA-Z]+$');
                                                if (value.isEmpty) {
                                                  return 'Last name required';
                                                }
                                                if (!(regex.hasMatch(lastData))) {
                                                  return 'Enter valid last name';
                                                }
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                hintText: "Last Name",
                                                isDense: true,
                                                contentPadding:
                                                    EdgeInsets.fromLTRB(20, 30, 0, 0),
                                                enabledBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(50),
                                                  borderSide: const BorderSide(
                                                      color: Colors.grey, width: 1.0),
                                                ),
                                                focusedBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(50),
                                                  borderSide: const BorderSide(
                                                      color: Color(0xFFC77DC1),
                                                      width: 1.5),
                                                ),
                                                focusedErrorBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(50),
                                                  borderSide: const BorderSide(
                                                      color: Color(0xFFC77DC1),
                                                      width: 1.5),
                                                ),
                                                errorBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(50),
                                                  borderSide: const BorderSide(
                                                      color: Colors.red, width: 1.5),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      TextFormField(
                                        controller: _mobileNumberCtrl,
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return 'Mobile no. required';
                                          }
                                          if (value.length > 10 || value.length < 10) {
                                            return 'Please enter valid mobile no.';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.phone,
                                        decoration: _borders(
                                            'India Mobile No.(10 Digits)',
                                            Icons.smartphone),
                                      ),
                                      TextFormField(
                                        controller: _emailCtrl,
                                        validator: (value) {
                                          Pattern pattern =
                                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                          RegExp regex = new RegExp(pattern);
                                          if (value.isEmpty) {
                                            return 'Email required';
                                          }
                                          if (!(regex.hasMatch(value))) {
                                            return 'Enter valid email';
                                          }
                                          return null;
                                        },
                                        decoration: _borders('Email', Icons.mail_outline),
                                      ),
                                      TextFormField(
                                        controller: _passwordCtrl,
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return 'Password required';
                                          }
                                          if (value.length < 8) {
                                            return 'Enter 8 digit password';
                                          }
                                          return null;
                                        },
                                        obscureText: true,
                                        decoration: _borders(
                                            'Create Password', Icons.lock_outline),
                                      ),
                                      Text('Have a referral code?'),
                                      SizedBox(
                                          width: 130,
                                          child: CommonButton('SUBMIT', _submit,true)),
                                      Row(
                                        children: [
                                          Text(
                                            'By Submitting, I accept the ',
                                            style: TextStyle(
                                              fontSize: 12,
                                            ),
                                          ),
                                          Text(
                                            'term of use & privacy policy.',
                                            style: TextStyle(
                                              fontSize: 12,
                                              color: Theme.of(context).primaryColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Text(
                                  'Or Sign in with',
                                  style: TextStyle(color: Theme.of(context).accentColor),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 60,
                                    right: 60,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      RaisedButton.icon(
                                        shape: new RoundedRectangleBorder(
                                            borderRadius: new BorderRadius.circular(8.0)),
                                        color: Color(0xFF3B5998),
                                        onPressed: () {},
                                        icon: FaIcon(
                                          FontAwesomeIcons.facebookF,
                                          color: Theme.of(context).accentColor,
                                          size: 14,
                                        ),
                                        label: Text(
                                          'Facebook',
                                          style: TextStyle(
                                              color: Theme.of(context).accentColor),
                                        ),
                                      ),
                                      RaisedButton.icon(
                                        shape: new RoundedRectangleBorder(
                                            borderRadius: new BorderRadius.circular(8.0)),
                                        onPressed: _handleSignIn,
                                        color: Color(0xFFDB4A39),
                                        icon: FaIcon(
                                          FontAwesomeIcons.google,
                                          color: Theme.of(context).accentColor,
                                          size: 14,
                                        ),
                                        label: Text(
                                          'Google',
                                          style: TextStyle(
                                              color: Theme.of(context).accentColor),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }
          }
          

