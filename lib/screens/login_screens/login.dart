import 'dart:async';
import 'dart:convert';

import 'package:ec_student/providers/apiCall.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../widgets/layouts/curve_top_right.dart';
import '../../widgets/layouts/curve_bottom_right.dart';
import '../../widgets/layouts/common_button.dart';
import '../../screens/login_screens/signup.dart';
import '../../screens/login_screens/mobile_no_verificaion.dart';
import '../../screens/tab_screens/tab_screen.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
    'https://www.googleapis.com/auth/user.phonenumbers.read'
  ],
);

class Login extends StatefulWidget {
  static const routeName = '/login';

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _loginForm = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GoogleSignInAccount _currentUser;
  final _mobile_no = TextEditingController();
  final _password = TextEditingController();
  bool _isButtonEnabled = true;

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
      print(_googleSignIn.currentUser.displayName);
      print(_googleSignIn.currentUser.email);
      print(_googleSignIn.currentUser.photoUrl);
    } catch (error) {
      print(error);
    }
  }

  // _handleSignIn(){
  //   // _currentUser.
  //   print(_currentUser..displayName);
  //     print(_currentUser..email);
  //     print(_currentUser..photoUrl);
  // }

  @override
  Widget build(BuildContext context) {
    _logIn() async {
      print(_currentUser);
      final isValid = _loginForm.currentState.validate();

      if (!isValid) {
        return;
      }
      print('Log Form');
      _loginForm.currentState.save();

      final _isValid = _loginForm.currentState.validate();
      if (!_isValid) {
        return;
      }
      _loginForm.currentState.save();

      final String url = 'api/login';

      final data = jsonEncode(<String, dynamic>{
        'mobile_number': _mobile_no.text,
        'password': _password.text,
        'login_for': 1
      });

      final response = await postData(url, data, false);

      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        SharedPreferences prefs = await SharedPreferences.getInstance();

        await prefs.setString('token', data['token']);
// CommonButton('LOG IN', null, false);
        Navigator.of(context).pushNamed(TabScreen.routeName);
      } else {
        final data = jsonDecode(response.body);
        // print(data['message']);
// CommonButton('LOG IN', null, true);
        final snackBar = SnackBar(
          content: Text(
            data['message'], textAlign: TextAlign.center,
            // style: TextStyle(color: Colors.red),
          ),
          backgroundColor: Theme.of(context).primaryColor,
        );

        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    }

    _forgotPassword() {
      print('Forgot Password in called...');
      Navigator.of(context).pushNamed(MobileNoVerification.routeName);
    }

    _border(hintText, icon) {
      return InputDecoration(
        hintText: hintText,
        isDense: true,
        contentPadding: EdgeInsets.fromLTRB(30, 30, 30, 0),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: const BorderSide(color: Color(0xFFC77DC1), width: 1.5),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: BorderSide(color: Colors.red, width: 1.5),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: BorderSide(color: Colors.red, width: 1.5),
        ),
        prefixIcon: Icon(
          icon,
          color: Colors.grey,
        ),
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Positioned(
              bottom: 61,
              right: 14,
              child: CurveBottomRight(),
            ),
            Positioned(
              bottom: 101,
              right: 15,
              child: CurveTopRight(),
            ),
            Positioned(
              bottom: 60,
              right: 1,
              width: 120,
              child: RaisedButton(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text(
                      'Register',
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  ),
                  color: Theme.of(context).primaryColor,
                  shape: StadiumBorder(),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Signup.routeName);
                  }),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  margin:
                      EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  width: 15,
                  height: MediaQuery.of(context).size.height * .9,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.elliptical(100, 150),
                      bottomLeft: Radius.elliptical(100, 150),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top,
              padding: EdgeInsets.only(
                left: 50.0,
                right: 50.0,
              ),
              child: Form(
                key: _loginForm,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 80,
                      child: Image.asset(
                        'assets/educhamp.png',
                        width: 240,
                        // height: 100,
                      ),
                    ),
                    Image.asset(
                      'assets/login.jpg',
                      width: 120,
                      height: 120,
                    ),
                    Text(
                      'For all your needs in School & College',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextFormField(
                      controller: _mobile_no,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Mobile number required';
                        }
                        if (value.length > 10 || value.length < 10) {
                          return 'Enter valid mobile no.';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      decoration:
                          _border('10 Digits Mobile No.', Icons.person_outline),
                    ),
                    TextFormField(
                        controller: _password,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Password required';
                          }
                          return null;
                        },
                        obscureText: true,
                        decoration: _border('Password', Icons.lock_outline)),
                    InkWell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'Forgot Password?',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).textSelectionColor),
                          ),
                        ],
                      ),
                      onTap: _forgotPassword,
                    ),
                    SizedBox(
                      width: 130,
                      child: CommonButton('LOG IN', _logIn, _isButtonEnabled),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Or Sign in with',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        RaisedButton.icon(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: Color(0xFF3B5998),
                          onPressed: () {},
                          icon: FaIcon(
                            FontAwesomeIcons.facebookF,
                            color: Theme.of(context).accentColor,
                            size: 14,
                          ),
                          label: Text(
                            'Facebook',
                            style:
                                TextStyle(color: Theme.of(context).accentColor),
                          ),
                        ),
                        RaisedButton.icon(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          onPressed: _handleSignIn,
                          color: Color(0xFFDB4A39),
                          icon: FaIcon(
                            FontAwesomeIcons.google,
                            color: Theme.of(context).accentColor,
                            size: 14,
                          ),
                          label: Text(
                            'Google',
                            style:
                                TextStyle(color: Theme.of(context).accentColor),
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 30),
                          child: Text("Don't have an account?"),
                        ),
                        SizedBox(
                          width: 48,
                        ),
                        SizedBox(
                          height: 40,
                        )
                        // RaisedButton(
                        //   child: Text(
                        //     'Register',
                        //     style:
                        //         TextStyle(color: Theme.of(context).accentColor),
                        //   ),
                        //   color: Theme.of(context).primaryColor,
                        //   elevation: 5,
                        //   shape: StadiumBorder(),
                        //   onPressed: () {
                        //     Navigator.of(context).pushNamed(Signup.routeName);
                        //   },
                        // ),
                      ],
                    ),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                        color: Colors.deepOrange[50],
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Teacher? click to download another app'),
                          SizedBox(
                            width: 5,
                          ),
                          FaIcon(
                            FontAwesomeIcons.chevronCircleRight,
                            color: Theme.of(context).buttonColor,
                            size: 18,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
          // overflow: Overflow.visible,
        ),
      ),
    );
  }
}
