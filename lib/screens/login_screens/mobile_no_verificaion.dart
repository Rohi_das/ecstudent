import 'dart:convert';

import 'package:ec_student/providers/apiCall.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/common_button.dart';
import '../../widgets/layouts/side_screen.dart';
import '../../screens/login_screens/otp.dart';

class MobileNoVerification extends StatelessWidget {
  static const routeName = '/mobile-no-verification';
  final _mobileConfirm = GlobalKey<FormState>();
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
      final _mobile_no = TextEditingController();
  @override
  Widget build(BuildContext context) {
    _submitMobileNo() async {
      print('Submit Mobile Number called....');
      final _isValid=_mobileConfirm.currentState.validate();
      if(!_isValid){
        return;
      }
    
      _mobileConfirm.currentState.save();

      final String url = 'api/forgot_password';

      final data = jsonEncode(<String, dynamic>{
        'mobile_number': _mobile_no.text,
        'login_for':1
      });

      final response = await postData(url, data, false);

      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        print(data);
         SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('token', data['token']);
              Navigator.of(context).pushNamed(
        OTP.routeName,
        arguments: <String, dynamic>{'isForgotPassword': false, 'mobile_no':_mobile_no.text},
      );
      

      } else {
        final data = jsonDecode(response.body);
        final snackBar = SnackBar(
          content: Text(
            data['message'], textAlign: TextAlign.center,
            // style: TextStyle(color: Colors.red),
          ),
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    


    }

    _borders(hintText, iconName) {
      return InputDecoration(
        hintText: hintText,
        isDense: true,
        contentPadding: EdgeInsets.fromLTRB(10, 30, 10, 0),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: const BorderSide(color: Color(0xFFC77DC1), width: 1.5),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: const BorderSide(color: Colors.red, width: 1.5),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: const BorderSide(color: Colors.red, width: 1.5),
        ),
        prefixIcon: Icon(
          iconName,
          color: Colors.grey,
        ),
      );
    }

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            Container(
              // margin: EdgeInsets.only( top:MediaQuery.of(context).padding.top),
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/educhamp.png',
                          width: 240, alignment: Alignment.center,
                          // height: 100,
                        ),
                      ],
                    ),
                  ),
                  AppBarBackButton(false, false),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 50),
                    height: 420,
                    padding: EdgeInsets.symmetric(
                      horizontal: 60.0,
                    ),
                    child: Form(
                      key: _mobileConfirm,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/login.jpg',
                            width: 120,
                            height: 120,
                          ),
                          Text(
                            'Confirmation',
                            style: TextStyle(
                              fontSize: 30,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Enter your registered mobile number',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          TextFormField(
                            controller: _mobile_no,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Mobile number required';
                              }
                              if (value.length > 10 || value.length < 10) {
                                return 'Enter valid mobile no.';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.phone,
                            decoration: _borders(
                                '10 Digits Mobile No.', Icons.smartphone),
                          ),
                          SizedBox(
                              width: 130,
                              child: CommonButton('SUBMIT', _submitMobileNo,true)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SideScreen(true),
          ],
        ),
      ),
    );
  }
}
