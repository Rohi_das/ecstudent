import 'dart:convert';

import 'package:ec_student/providers/apiCall.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/side_screen.dart';
import '../../screens/login_screens/login.dart';
import '../../widgets/layouts/common_button.dart';

class ForgotPassword extends StatefulWidget {
  static const routeName = '/ForgotPassword';
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _password = TextEditingController();
  final _cPassword = TextEditingController();
    final _formCP = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {

    _changePassword() async{
          final isValid = _formCP.currentState.validate();
    if (!isValid) {
      return;
    }
    _formCP.currentState.save();
      print('_changePassword Called........');
       final String url = 'api/set_new_password';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final data = jsonEncode(<String, dynamic>{
      'token': prefs.getString('token'),
      'new_password': _password.text,
      'confirm_password': _cPassword.text,
    });

    final response = await postData(url, data, false);
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('token', data['token']);
      Navigator.of(context)
          .pushNamedAndRemoveUntil(Login.routeName, (route) => false);
    } else {
      final data = jsonDecode(response.body);
      final snackBar = SnackBar(
        content: Text(
          data['message'], textAlign: TextAlign.center,
          // style: TextStyle(color: Colors.red),
        ),
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
      // Navigator.of(context).pushNamedAndRemoveUntil(Login.routeName, (route) => false);
    }



    
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            SingleChildScrollView(
                          child: Container(
                // margin: EdgeInsets.only( top:MediaQuery.of(context).padding.top),
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).padding.top + 5.0),
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/educhamp.png',
                            width: 240, alignment: Alignment.center,
                            // height: 100,
                          ),
                        ],
                      ),
                    ),
                    AppBarBackButton(false,false),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only( top:50),
                      height: 420,
                      padding: EdgeInsets.symmetric(horizontal: 60.0,),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/login.jpg',
                            width: 120,
                            height: 120,
                          ),
                          Text(
                            'Change Password',
                            style: TextStyle(
                              fontSize: 30,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('Enter new password and ', style: TextStyle(fontWeight: FontWeight.bold),),
                          Text('confirm it by re-tying new password', style: TextStyle(fontWeight: FontWeight.bold,),),
                            ],
                          ),
                          Form(key: _formCP, child: Column(children: [

                          TextFormField(
                             controller: _password,
                              validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please confirm your new password.';
                                  }
                                  if (value.length < 8) {
                                    return 'Please provide 8 digit password';
                                  }
                                  if(_password.text!=null || _password.text!=''){
                                  if (_password.text !=
                                      _cPassword.text) {
                                    return 'Password not matched.';
                                  }

                                  }
                                  return null;
                                },
                            // decoration: InputDecoration(
                            //   hintText: "Enter new password",
                            //   isDense: true,
                            //   contentPadding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                            //   enabledBorder: OutlineInputBorder(
                            //     borderRadius: BorderRadius.circular(50),
                            //     borderSide: const BorderSide(
                            //         color: Colors.grey, width: 1.0),
                            //   ),
                            //   focusedBorder: OutlineInputBorder(
                            //     borderRadius: BorderRadius.circular(50),
                            //     borderSide: const BorderSide(
                            //         color: Color(0xFFC77DC1), width: 1.5),
                            //   ),
                            //   prefixIcon: Icon(
                            //       Icons.lock_outline,
                            //       color: Colors.grey,
                            //     ),
                            // ),
                            decoration: InputDecoration(
                                hintText: "Enter new password",
                                isDense: true,
                                contentPadding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  borderSide: const BorderSide(
                                      color: Colors.grey, width: 1.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  borderSide: const BorderSide(
                                      color: Color(0xFFC77DC1), width: 1.5),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  borderSide:
                                      BorderSide(color: Colors.red, width: 1.5),
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  borderSide:
                                      BorderSide(color: Colors.red, width: 1.5),
                                ),
                                    prefixIcon: Icon(
                                  Icons.lock_outline,
                                  color: Colors.grey,
                                ),
                              ),
                          ),
                          SizedBox(height: 10,),
                          TextFormField(
                            controller: _cPassword,
                            validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please confirm your new password.';
                                  }
                                  if (value.length < 8) {
                                    return 'Please provide 8 digit password';
                                  }
                                  if(_cPassword.text!=null || _cPassword.text!=''){
                                  if (_password.text !=
                                      _cPassword.text) {
                                    return 'Password not matched.';
                                  }

                                  }
                                  return null;
                                },
                          decoration: InputDecoration(
                                hintText: "Enter new password",
                                isDense: true,
                                contentPadding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  borderSide: const BorderSide(
                                      color: Colors.grey, width: 1.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  borderSide: const BorderSide(
                                      color: Color(0xFFC77DC1), width: 1.5),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  borderSide:
                                      BorderSide(color: Colors.red, width: 1.5),
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  borderSide:
                                      BorderSide(color: Colors.red, width: 1.5),
                                ),
                                    prefixIcon: Icon(
                                  Icons.lock_outline,
                                  color: Colors.grey,
                                ),
                              ),
                          ),
                          SizedBox(
                            width: 130, child: CommonButton('CHANGE', _changePassword,true)),
                          ],),
                          
                          ),
                          
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SideScreen(true),
          ],
        ),
      ),
    );
  }
}
