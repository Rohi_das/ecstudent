import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';
class AboutUs extends StatelessWidget {
  static const routeName='/about-us';
//   final String _url = "";
// _launchURL() async {
//   if (await canLaunch(_url)) {
//     await launch(_url);
//   } else {
//     throw 'Could not launch $_url';
//   }
// }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'About US'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 20,
                ),
                child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(left: 6.0, right: 6.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'EDUCATIONCHAMP',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                      fontFamily: "Arial"),
                ),
                Text(
                  'Updated On : 20th January 2019 \n',
                  style: TextStyle(
                      fontSize: 15.0,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w600),
                ),
                Column(
                  children: [
                    RichText(
                      text: TextSpan(children: [
                        new TextSpan(
                          text:
                              "We are a diversified business house with business interests in Electronics, Telecommunication, Surveillance and Aviation.  With a humble beginning way back in 1995 as a proprietorship firm, we organised ourselves as a Private Limited company in the year 2002 and later reorganised the company as a Public Limited company (unlisted) in the year 2004. With diverse business verticals, our company has been operational for past 17 years and has now ventured into e-commerce business with it's brand names",
                          style: TextStyle(fontSize: 14.0, color: Colors.black),
                        ),
                        new TextSpan(
                          text: ' "education champ" ',
                          style: new TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 14.0,
                              fontWeight: FontWeight.w600),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () {
                              launch('http://www.educationchamp.com/');
                            },
                        ),
                        new TextSpan(
                          text:
                              '(for everything a student needs during his education) and',
                          style: TextStyle(fontSize: 14.0, color: Colors.black),
                        ),
                        new TextSpan(
                          text: ' Electrocon ',
                          style: new TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 14.0,
                              fontWeight: FontWeight.w600),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () {
                              launch('http://www.electrocon.net/');
                            },
                        ),
                        new TextSpan(
                          text: ' (home and office automation products). \n ',
                          style: TextStyle(fontSize: 14.0, color: Colors.black),
                        ),
                        new TextSpan(
                          text:
                              '\n Our business enterprise consists of following group companies : \n',
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.black),
                        ),
                      ]),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '•',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                    RichText(
                        text: TextSpan(children: [
                      new TextSpan(
                        text: ' Electrocon Consumer Electronics (I) Ltd. ',
                        style: new TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () {
                            launch('http://www.electrocon.net/');
                          },
                      ),
                    ]))
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '•',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                    RichText(
                        text: TextSpan(children: [
                      new TextSpan(
                        text: ' AeroChamp Aviation (Intl.) Pvt. Ltd. ',
                        style: new TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () {
                            launch('http://www.aerochamp.net/');
                          },
                      ),
                    ]))
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '•',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                    Expanded(
                                          child: RichText(
                          text: TextSpan(children: [
                        new TextSpan(
                          text:
                              ' Academy of Professional Studies and Skill Development ',
                          style: new TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 15.0,
                              fontWeight: FontWeight.w600),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () {
                              launch('http://www.apssd.net/');
                            },
                        ),
                      ])),
                    )
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '•',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                    Expanded(
                                          child: RichText(
                          text: TextSpan(children: [
                        new TextSpan(
                          text: ' Kinsfolk Technology Pvt.Ltd. ',
                          style: new TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 15.0,
                              fontWeight: FontWeight.w600),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () {
                              launch('https://www.kinsfolksolutions.com/');
                            },
                        ),
                      ])),
                    )
                    
                  ],
                ),
                SizedBox(height: 10,),
                Column(
                  children: [
                    RichText(
                        text: TextSpan(children: [
                      new TextSpan(
                        text:
                            "Our founders have a strong Educational background combined with vast industry experience in each of the business verticals; this differentiates us from other companies in the same industry. Our founders have vision to position each business vertical as a leading brand in its domain. \n \n In today's era of internet and technology, customers should be able to purchase products and services at their convenience from home or office, which will be delivered hassle free at their location. Improvement and stability in global IT infrastructure has helped in bringing such e-commerce services to the benefit of customers. \n \n At EducationChamp we endeavour to bring the benefit of technology to the convenience of students and parents by offering the whole range of education related products and services including tuition services, books, project kits and school/college communication under one App and Website",
                        style: TextStyle(fontSize: 14.0, color: Colors.black),
                      ),
                      new TextSpan(
                        text: ' "education champ" ',
                        style: new TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 14.0,
                            fontWeight: FontWeight.w600),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () {
                            launch('http://www.educationchamp.com/');
                          },
                      ),
                      new TextSpan(
                        text:
                            ". We are evolving every day and upgrading ourselves through extensive research, hence you will find continuous improvement in our App and website to offer full range of education products and services to the students so that they can organise themselves better and focus on their studies.\n \n We clearly understand that customer is the purpose of our business.",
                        style: TextStyle(fontSize: 14.0, color: Colors.black),
                      ),
                      new TextSpan(text: "\n \n ")
                    ]))
                  ],
                ),
                
              ],
            ),
          ),
        ),
      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}