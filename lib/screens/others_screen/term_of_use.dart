import 'package:flutter/material.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';
class TermOfUse extends StatelessWidget {
  static const routeName='/term-of-use';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Term of Use'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 20,
                ),
                child: SingleChildScrollView(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 160.0, top: 20.0),
                child: Text(
                  'Updated on 15th March 2020 \n',
                  style: TextStyle(
                      fontSize: 15.0,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  right: 160.0,
                  top: 5.0,
                ),
                child: Text(
                  'Terms and Conditions',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                      fontFamily: "Arial"),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                      text:
                          '\n The terms and conditions stated below, along with all/any other terms and legal warning placed on our App or websitewww.educationchamp.com (collectively called “Terms”), govern your use of EducationChamp services through its App or Website. If you disagree by any or all Terms, please exit the App or Website and do not use EducationChamp services. Your use of EducationChamp App or Websiteat anytime constitutes a binding agreement by you to abide by the Terms and Conditions.\n \n Registration/Sign Up \n \n Upon registration, you agree to:',
                      style: TextStyle(fontSize: 14.0, color: Colors.black),
                    ),
                    TextSpan(
                        text:
                            '\n \n Make your contact details (including but not limited to mobile number and e-mail) available to EducationChamp a brand of Electrocon Consumer Electronics (I) Ltd (referred as “Company”). \n \n The company may contact you regarding business offerings, products, services, promotions, new introductions and any other relevant information through email, phone, SMS, Social Media, Social Messaging or any other means of communication. \n \n Receive promotional mails/SMS/ or any other special offers from EducationChamp or any of its partner websites. \n \n Be contacted by the Company in accordance with the privacy settings set by you:',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        )),
                    TextSpan(
                      text:
                          '\n \n Restrictions Regarding Materials and other information \n \n 1. All information, documents, software, images, photographs, videos, text, services, study notes, question banks, sample papers and other similar materials (collectively known as “Materials”) contained in the App or Website provided by our company or its third party manufacturers, authors, developers and vendors (“Third Party Providers”) are the copyrighted work of “Electrocon Consumer Electronics (I) Ltd” and/or the respective Third Party Providers. Except for: \n \n a)      The videos, blogsor other contents provided as a part of free circulation, which may be in-house orThird Party videos or content available for FREE on open source video sharing sites like Youtube and other education and non-education website. \n \n b)      NCERT, CBSE and state board e-books which are published by respective boards are made available to users as a courtesy without any obligation. The Company is not involved in re-circulating e-books directly and disclaims any responsibility for any of its users selling NCERT e-books through any means.',
                      style: TextStyle(fontSize: 14.0, color: Colors.black),
                    ),
                    TextSpan(
                      text:
                          '\n \n 2. The videos and e-books are available as an additional offering along with our core content. The Company may charge nominal handling feefor such e-books or Videos towards server and internet operational cost. \n \n 3. The amount that a user pays is towards the Professional fee, Study Material, Sample Papers and our services charges. \n \n 4.  Except as stated herein, none of the Materials may be copied, reproduced, distributed, republished, downloaded, displayed, posted or transmitted in any form or by any means, including, but not limited to, electronic, mechanical, photocopying, recording, or otherwise, without the prior express written permission of ‘Electrocon Consumer Electronics (I) Ltd.’ or the Third Party Provider. No part of the App or Website, including logos, graphics, sounds or images, may be reproduced or retransmitted in any way, or by any means, without the prior express written permission of ‘ Electrocon Consumer Electronics (I) Ltd.’. You also may not, without Electrocon Consumer Electronics (I) Ltd.’s prior express written permission, “mirror” any Materials contained on our App or Website on any other server, website or any other location. \n \n 5. Nothing on our App or Website shall be construed as conferring any license under any of Electrocon Consumer Electronics (I) Ltd. or any Third Party Provider’s intellectual property rights, whether by estoppels, implication, or otherwise. You acknowledge sole responsibility for obtaining any such licenses. \n \n 6. Permission is granted to display, copy, distribute and download Electrocon Consumer Electronics (I) Ltd.’s materials on our App or Website provided that: (1) both the copyright notice identified below and this permission notice appear in the Materials, (2) the use of such Materials is solely for personal, non-commercial and informational use and will not be copied or posted on any networked computer or broadcast in any media, and (3) no modifications of any of the Materials are made. This permission terminates automatically without notice if you breach any of these terms or conditions. Upon termination, you will immediately destroy any downloaded or printed Materials. Any unauthorized use of any Materials contained on our App or Website may violate copyright laws, trademark laws, and other related laws and may have various legal consequences. \n \n 7. Materials provided by Third Party Providers have not been independently authenticated in whole or in part by ‘Electrocon Consumer Electronics (I) Ltd.’ furtherEducationChamp does not provide, sell, license, or lease any of the Materials other than those specifically identified as being provided by ‘Electrocon Consumer Electronics (I) Ltd.’. Our company is committed to respecting others’ intellectual property rights, and we ask our users to do the same. Electrocon Consumer Electronics (I) Ltd  may, in its sole discretion, terminate the access rights of users who infringe or otherwise violate others’ intellectual property rights. \n \n 8. Electrocon Consumer Electronics (I) Ltd. puts in special care for verifying the credentials of the tutors, trainers and teachers listed on our App or Website. However, the Company directly disclaims the responsibility of any wrong information, misbehaviour, fraud, disrespect, harassment (sexual or otherwise) or any other untoward incident by the tutors listed on it’s App or Website. \n \n 9. For Tutors:',
                      style: TextStyle(fontSize: 14.0, color: Colors.black),
                    ),
                    TextSpan(
                        text:
                            '\n \n Registering or signing up as a tutor, trainer or teacher on EducationChamp App or Website platform is completely FREE. \n \n The Company holds the right to ask for all relevant information from the tutors, trainer and teachers for verification purpose.\n \n The Company may get the profiles of the registered and listed tutors, trainer and teachers verified by third party agencies for security purpose as per need. \n \n All the payments to tutors, trainersand teachers will be done by Electrocon Consumer Electronics (I) Ltdthrough on-line transfer into their registered bank account and the company will directly deal with students/parents for collection of payments towards fee or any other charges. \n \n Tutors, Trainers, Counsellors, Teachersor any other service providers listed on EducationChamp platform are in no wayallowed to collect any payment or fee on behalf of the company or individually under any circumstance. \n \n The Company will deduct a suitable proportion of the fees paid by the student(s)/parents, for connecting with the tutor, trainer, counsellor, teacher or any other service provider before making the final payment to them. This proportion will be conveyed to the service provider and may wary on a case-to-case basis on sole discretion of the Company \n \n Any  tutor, trainer, counsellor, teacher or any other service providerviolating these terms will be liable for legal action.',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        )),
                  ]))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10.0, top: 20.0),
                child: Text(
                  ' \n \n DISCLAIMER OF ONLINE AVAILABILITY, IMPRESSIONS, AND CLICK-THROUGHS:',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                      text:
                          ' \n In addition to the other disclaimers and limitations explained under this chapter, there are no guarantees or warranties regarding online availability, impressions and click-through of EducationChamp App or Website, it’s web pages, and any material, information, links, or content presented on the App and web pages of EducationChamp. EducationChamp’s App or web pages, and any material, information, links, or content presented through EducationChamp platform, may be unavailable for online access at anytime. Advertising sponsors and advertisers must be approved by Electrocon Consumer Electronics (I) Ltd before the posting of any advertising material, information, links, content, banners, and graphics on EducationChamp App or website. Electrocon Consumer Electronics (I) Ltd. reserves the right to accept or reject any advertising sponsor or any advertisersolely at its discretion without giving any reason whatsoever. ',
                      style: TextStyle(fontSize: 14.0, color: Colors.black),
                    ),
                  ]))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10.0, top: 20.0),
                child: Text(
                  ' \n \n LIMITATION OF LIABILITY:',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                      text:
                          '\n In no event and under no circumstances and under no legal theory, tort, contract, or otherwise shall Electrocon Consumer Electronics (I) Ltd, its sister concerns or any subsidiaryshall be liable, without limitation, for any damages whatsoever, including direct, indirect, incidental, consequential or punitive damages, arising out of any access to or any use of or any inability to access or use our App or website including any material, information, links, and content accessed through our platform or through any linked external website.',
                      style: TextStyle(fontSize: 14.0, color: Colors.black),
                    ),
                  ]))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10.0, top: 20.0),
                child: Text(
                  ' \n \n LOCAL LAWS',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                      text:
                          '\n Electrocon Consumer Electronics (I) Ltd. controls and operates its App andwebsite from its headquarters in Navi-Mumbai, Maharashtra, India and makes no representation that the materials on the website are appropriate or available for use in other locations. If you use this Website from other locations, you are responsible for compliance with applicable local laws including but not limited to the export and import regulations of other countries. Unless otherwise explicitly stated, all marketing or promotional materials found on this Website are solely directed to individuals, companies or other entities located in India and comply with the laws prevailing for the time being in force in India. Disputes if any shall be subject to the exclusive jurisdiction of Courts in Mumbai.',
                      style: TextStyle(fontSize: 14.0, color: Colors.black),
                    ),
                  ]))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10.0, top: 20.0),
                child: Text(
                  ' \n \n GENERAL',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text:
                            '\n EducationChamp App or Website could include unintended inaccuracies or typographical errors. Electrocon Consumer Electronics (I) Ltd and the Third Party Providers may make improvements and/or changes in the products, services, programs, and prices described in this Website at any time without notice. Changes are periodically made to the Website. \n \n The material, information, links, and content presented on and by EducationChamp App or website are of a general nature only and are not intended to address the specific circumstances, requirements, or any other needs of any particular individual or entity. It cannot be guaranteed that the material, information, links, and content presented on and by our App or website is comprehensive, complete, accurate, sufficient, timely, or up to date for any particular purpose or use. The material, information, links, and content presented on and by our App or website should not be considered as professional, legal, business, financial, investment, or purchasing advice (if you need specific advice, you should always consult a suitably qualified professional). EducationChamp App or website is sometimes linked to external websites over which our company has no control and assumes no responsibility, and are in no way acting as a publisher of material, information, links, and content contained on external linked websites. Links may become invalid, may expire, or may become misdirected at any time. Links are provided as a convenience and do not necessarily constitute, signify, or otherwise imply an endorsement by, or an endorsement for, or a relationship with, or connection to Electrocon Consumer Electronics (I) Ltd. The statements expressed on external linked websites are not those of Electrocon Consumer Electronics (I) Ltd; and users are advised that our company neither maintains editorial control over externally linked websites nor determines the appropriateness regarding the material, information, links, and content contained on external linked websites. Electrocon Consumer Electronics (I) Ltd has no control over any external website or over any external material, information, links, and content linked to EducationChamp App or website. \n \n The subjects and their coverage, study material, assignment, project, tests and their results are to be used between Tutor, Trainer, Counsellor, Teacher and Students directly for tutoring, training and teaching purpose. Our company does not hold any responsibility for correctness and relevance of any such material. \n \n EducationChamp App and Website, and your use of services through App and Website platform will be governed under the laws of India and subject to the jurisdiction of courts in Mumbai.\n \n These Terms represent the entire understanding relating to the use of App and Website and prevail over any prior or contemporaneous, conflicting, additional, or other communications. Electrocon Consumer Electronics (I) Ltd can modify these Terms at any time without notice or consent by updating this posting. \n \n The company may come up with reward points and gratification schemes for its users. The company reserves the right to change or update any such programs without prior notice and any company’s decision in this regard will be final and binding. \n \n In the event any or a portion of the provisions of these Terms are held invalid, illegal or otherwise unenforceable by a Court, such provision shall be deemed amended to be valid, legal and enforceable to the fullest extent permitted under applicable law, and the remaining provisions of these Terms shall remain in full force and effect. These Terms are binding upon you and your heirs, representatives, successors and assigns. The headings of the several articles and subdivisions of these Terms are inserted solely for the convenience of reference and shall have no further meaning, force or effect. No third party shall be a beneficiary of any provision of these Terms, except with the express written consent of Electrocon Consumer Electronics (I) Ltd.',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        )),
                  ]))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10.0, top: 20.0),
                child: Text(
                  ' \n \n LICENSE DISCLAIMER',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                    text: TextSpan(children: [
                      TextSpan(
                          text:
                              '\n Nothing on any Electrocon Consumer Electronics (I) Ltd.App or Website shall be construed as conferring any license under any of Electrocon Consumer Electronics (I) Ltd.’s or any third party’s intellectual property rights, whether by estoppel, implication, or otherwise.',
                          style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.black,
                          )),
                    ]),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10.0, top: 20.0),
                child: Text(
                  ' \n \n CONTENT AND LIABILITY DISCLAIMER',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text:
                            '\n Electrocon Consumer Electronics (I) Ltd. shall not be responsible for any errors or omissions contained on any Electrocon Consumer Electronics (I) Ltd.’s App orWebsite, and reserves the right to make changes anytime without notice. Mention of non-Electrocon Consumer Electronics (I) Ltd. products or services is provided for informational purposes only and constitutes neither an endorsement nor a recommendation by Electrocon Consumer Electronics (I) Ltd. All Electrocon Consumer Electronics (I) Ltd. and third-party information provided on any Electrocon Consumer Electronics (I) Ltd.App or Website is provided on an “as is” basis. \n \n Views expressed by the users are their own, Electrocon Consumer Electronics (I) Ltd. does not endorse the same. No claim as to the accuracy and correctness of the information on the site is made although every attempt is made to ensure that the content is not misleading. In case any inaccuracy is or otherwise improper content is sighted on the website, please report it to report abuse \n \n Electrocon Consumer Electronics (I) Ltd DISCLAIMS ALL WARRANTIES, EXPRESSED OR IMPLIED, WITH REGARDS TO ANY INFORMATION (INCLUDING ANY SOFTWARE, PRODUCTS, OR SERVICES) PROVIDED ON ANY Electrocon Consumer Electronics (I) LtdAPP OR WEBSITE, INCLUDING THE IMPLIED WARRANTIES OF MERCHANT ABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ))
                  ]))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10.0, top: 20.0),
                child: Text(
                  ' \n \n PURCHASING AND ORDERING DISCLAIMER',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  right: 10.0,
                ),
                child: Text(
                  ' \n Make your own decisions:',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text:
                            '\n If you are making important purchasing or selling decisions, whether personal or business decisions, you should look at an actual demonstration model of any product or services you are considering before making your important purchasing or selling decisions. All decisions made would be entirely your prerogative and Electrocon Consumer Electronics (I) Ltd. does not claim to offer any advice, either legal or financial.\n \n In purchasing a product or service you understand and agree to the terms below. If you do not understand and agree with these terms, then do not place an order for the product or service.',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        )),
                  ]))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(
                  right: 10.0,
                ),
                child: Text(
                  ' \n PRICING',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text:
                            '\n All prices are subject to change without notice. Every effort has been made to ensure accurate pricing of the products or services featured on our App or Website. Purchase “as is” you agree that all products and services purchased by you through our App orWebsite are provided under warranties, if any, of the manufacturer or service provider only, and not by Electrocon Consumer Electronics (I) Ltd.All products and services are provided by Electrocon Consumer Electronics (I) Ltd. on “as is” basis with no representations or warranties of any kind from Electrocon Consumer Electronics (I) Ltd.',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        )),
                  ]))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(
                  right: 10.0,
                ),
                child: Text(
                  ' \n YOUR RESPONSIBILITY',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text:
                            '\n You have sole responsibility for use of the products/services you purchase through our App or Website. In no event shall Electrocon Consumer Electronics (I) Ltd. be liable to you in relation to the products/services, or your use, misuse or inability to use the products, for any',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        )),
                    TextSpan(
                        text:
                            '\n indirect, punitive, special, exemplary, incidental, or consequential damage (including loss of business, revenue, profits, use, data or other economic advantage); or \n \n direct damages in excess of the amount you paid Electrocon Consumer Electronics (I) Ltd. for the applicable productor services.',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        )),
                  ]))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(
                  right: 10.0,
                ),
                child: Text(
                  ' \n YOUR REPRESENTATIONS',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text:
                            '\n You represent that you are not under any legal or other disability which limits your ability to comply with these Terms or to install and use the products you purchase with minimal risk of harm to you or others. You further represent that you are not purchasing the products/services for resale to others and will not do so without Electrocon Consumer Electronics (I) Ltd.’s prior written consent.',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        )),
                  ]))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(
                  right: 10.0,
                ),
                child: Text(
                  ' \n USE OF APP AND WEBSITE',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
                ),
              ),
              Column(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text:
                            '\n You represent, warrant and covenant that your use of EducationChamp App and Website shall not:',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        )),
                    TextSpan(
                        text:
                            '\n violate any applicable local, provincial, state, national or international law, statute, ordinance, rule or regulation; \n \n interfere with or disrupt computer networks connected to the App and Website; \n \n impersonate any other person or entity, or make any misrepresentation as to your employment by or affiliation with any other person or entity; \n \n forge headers or in any manner manipulate identifiers in order to disguise the origin of any user information; \n \n upload, post, transmit, publish, or distribute any material or information for which you do not have all necessary rights and licenses; \n \n upload, post, transmit, publish or distribute any material which infringes, violates, breaches or otherwise contravenes the rights of any third party, including any copyright, trademark, patent, rights of privacy or publicity or any other proprietary right; \n \n interfere with or disrupt the use of EducationChamp App or Website by any other user, nor “stalk”, threaten, or in any manner harass another user; \n \n upload, post, transmit, publish, or distribute any material or information which contains a computer virus, or other code, files or programs intending in any manner to disrupt or interfere with the functioning of EducationChamp App or Website, or that of other computer systems; \n \n use EducationChamp App or Website in such a manner as to gain unauthorized entry or access to the computer systems of others; \n \n upload, post, transmit, publish or distribute any material or information which constitutes or encourages conduct that would constitute a criminal offence, give rise to other liability, or otherwise violate applicable law; \n \n upload, post, transmit, publish, or distribute any material or information that is unlawful, or which may potentially be perceived as being harmful, threatening, abusive, harassing, defamatory, libellous, vulgar, obscene, or racially, ethnically, or otherwise objectionable; \n \n reproduce, copy, modify, sell, store, distribute or otherwise exploit for any commercial purposes the Website, or any component thereof (including, but not limited to any materials or information accessible through EducationChamp App or Website); \n \n use any device, software or routine to interfere or attempt to interfere with the proper working of App or Website; \n \n take any action that imposes an unreasonable or disproportionately large load on the App or Website infrastructure; \n \n any interpretation of study material other than as a tool of learning and enhancing knowledge; or \n \n the study material and assignments are created for you to test your understanding of concepts and are not to be interpreted as a sample question paper for any examinations.',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        )),
                    TextSpan(
                        text:
                            '\n \n Use of EducationChamp App or Website is at your own risk, and Electrocon Consumer Electronics (I) Ltd. will not be held liable for any errors or omissions contained in their App or Website. In no event, shall Electrocon Consumer Electronics (I) Ltd. be liable for any special, indirect or consequential damages, or any damages whatsoever resulting from loss of use, data or profits whether contract, negligence or any tort action arising out of, or in connection with, the use or performance of the information available from EducationChamp App or Website. \n \n Electrocon Consumer Electronics (I) Ltd. reserves the right to change these Terms and Conditions without any notice. Please keep visiting this page for latest updates on Terms and Conditions.',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        )),
                    new TextSpan(text: "\n \n ")
                  ]))
                ],
              ),
            ],
          ),
        ),
      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}