import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';

class PrivacyPolicy extends StatelessWidget {
  static const routeName = '/privacy-policy';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Privacy Policy'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 20,
                ),
                child: SingleChildScrollView(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'AS',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Text(
                          'Effective date: Feb 11, 2020 \n',
                          style: TextStyle(
                              fontSize: 15.0,
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.w600),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              new TextSpan(
                                text:
                                    "This privacy policy is an electronic record in the form of an electronic contract formed under the information technology act, 2000 and the rules made thereunder and the amended provisions pertaining to electronic documents / records in various statutes as amended by the information technology act, 2000. This privacy policy does not require any physical, electronic or digital signature.",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    "\n \n This privacy policy is a legally binding document between you and educationchamp (a brand owned by electrocon consumer electronics (I) ltd) (both terms defined below). The terms of this privacy policy will be effective upon your acceptance of the same (directly or indirectly in electronic form, by clicking on the I accept tab or by use of the app/website or by other means) and will govern the relationship between you and educationchamp for your use of the app/website (defined below).",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    "\n \n This document is published and shall be construed in accordance with the provisions of the information technology (reasonable security practices and procedures and sensitive personal data of information) rules, 2011 under information technology act, 2000; that require publishing of the privacy policy for collection, use, storage and transfer of sensitive personal data or information.",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    "\n \n Please read this privacy policy carefully. By using the app/website, you indicate that you understand, agree and consent to this privacy policy. If you do not agree with the terms of this privacy policy, please do not use this app/website. You hereby provide your unconditional consent or agreements to educationchamp as provided under section 43a and section 72a of information technology act, 2000.",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    "\n \n By providing us your Information or by making use of the facilities provided by the App/Website, you hereby consent to the collection, storage, processing and transfer of any or all of Your Personal Information and Non-Personal Information by EducationChamp as specified under this Privacy Policy. You further agree that such collection, use, storage and transfer of Your Information shall not cause any loss or wrongful gain to you or any other person.",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    '\n \n Electrocon consumer electronics (I) ltd. And its subsidiaries and affiliates and associate companies (individually and/or collectively, "Educationchamp") is/are concerned about the privacy of the data and information of users (including sellers and buyers/customers whether registered or non-registered) accessing, offering, selling or purchasing products or services on ',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    "educationchamp's app/websites, mobile sites or mobile applications ",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    '("App/website") on the app/website and otherwise doing business with educationchamp. "Associate companies" here shall have the same meaning as ascribed in companies act, 2013.',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    '\n \n The terms "We" / "Us" / "Our" individually and collectively refer to each entity being part of the definition of EducationChamp and the terms "You" /"Your" / "Yourself" refer to the users. This Privacy Policy is a contract between You and the respective EducationChamp entity whose App/Website You use or access or You otherwise deal with. This Privacy Policy shall be read together with the respective Terms Of Use or other terms and condition of the respective EducationChamp entity and its respective App/Website or nature of business of the App/Website.\n \n \n ',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                            ]))
                          ],
                        ),
                        Text(
                          ' USER INFORMATION',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              new TextSpan(
                                text:
                                    '\n \n To avail certain services on our App/Websites, users are required to provide certain information for the registration process namely: - a) your name, b) email address, c) sex, d) age, e) PIN code, f) credit card or debit card details g) medical records and history h) sexual orientation, i) biometric information, j) password etc., and / or your occupation, interests, and the like. The Information as supplied by the users enables us to improve our sites and provide you the most user-friendly experience.',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    '\n \n All required information is service dependent and we may use the above said user information to, maintain, protect, and improve its services (including advertising services) and for developing new services.',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    '\n \n Such information will not be considered as sensitive if it is freely available and accessible in the public domain or is furnished under the Right to Information Act, 2005 or any other law for the time being in force.\n \n \n ',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              )
                            ])),
                          ],
                        ),
                        Text(
                          ' COOKIES',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              new TextSpan(
                                text:
                                    '\n \n To improve the responsiveness of the sites for our users, we may use "cookies", or similar electronic tools to collect information to assign each visitor a unique, random number as a User Identification (User ID) to understand the ',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    "user's individual interests using the Identified Computer. Unless you voluntarily identify yourself (through registration, for example), we will have no way of knowing who you are, even if we assign a cookie to your computer. The only personal information a cookie can contain is information you supply (an example of this is when you ask for our Personalised Horoscope). A cookie cannot read data off your hard drive. Our advertisers may also assign their own cookies to your browser (if you click on their ads), a process that we do not control.\n \n \n",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              )
                            ]))
                          ],
                        ),
                        Text(
                          ' SMS',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              new TextSpan(
                                text:
                                    '\n \n EducationChamp uses OTP verification through SMS for login to its App. The user hereby grants permission to EducationChamp to send SMS on his/her registered mobile number for OTP authentication for secure user login to the App. EducationChamp also sends important notifications like payment, registration and other relevant confirmations through SMS, which the user has no objection to receive through SMS on his mobile.\n \n \n',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              )
                            ]))
                          ],
                        ),
                        Text(
                          ' EDUCATIONCHAMP has provided this Privacy Policy to familiarise You with:',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              new TextSpan(
                                text:
                                    '\n \n The type of data or information that You share with or provide to EducationChamp and that EducationChamp collects from You; ',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    '\n The purpose for collection of such data or information from You;',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    "\n EducationChamp's information security practices and policies; and",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    "\n EducationChamp's policy on sharing or transferring Your data or information with third parties. This Privacy Policy may be amended/updated from time to time. Upon amending/updating the Privacy Policy, we will accordingly amend the date above. We suggest that you regularly check this Privacy Policy to apprise yourself of any updates. Your continued use of App/Website or provision of data or information thereafter will imply Your unconditional acceptance of such updates to this Privacy Policy.",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                  text:
                                      '\n \n Information collected and storage of such Information: The "Information" (which shall also include data) provided by You to EducationChamp or collected from You by EducationChamp may consist of "Personal Information" and "Non-Personal Information".',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                              new TextSpan(
                                text: '\n Personal Information:',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    '\n Personal Information is the information collected that can be used to uniquely identify or contact You. Personal Information for the purposes of this Privacy Policy shall include, but not be limited to:',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                  text:
                                      '\n Your user name along with Your password,\n Your name,\n School name,\n Class,\n Your address,\n Your telephone/mobile number,\n Your e-mail address or other contact information,\n Your date of birth,\n Your gender,\n Information regarding your transactions on the App/Website, (including sales or purchase history),\n Your financial information such as bank account information or credit card or debit card or other payment instrument details,\n Internet Protocol (IP) address,',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                              new TextSpan(
                                  text:
                                      "\n Any other items of ‘sensitive personal data or information' as such term is defined under the Information Technology (Reasonable Security Practices And Procedures And Sensitive Personal Data Of Information) Rules, 2011 enacted under the Information Technology Act, 2000;\n Identification code of your communication device which You use to access the App/Website or otherwise deal with any EducationChamp entity,\n Any other Information that You provide during Your registration process, if any, on the App/Website. Such Personal Information may be collected in various ways including during the course of You:",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                              new TextSpan(
                                  text:
                                      "\n registering as a user on the App/Website,\n registering as a seller on the App/Website,\n availing certain services offered on the App/Website. Such instances include but are not limited to making an offer for sale, online purchase, participating in any online survey or contest, communicating with EducationChamp's customer service by phone, email or otherwise or posting user reviews on the items available on the App/Website, or",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                              new TextSpan(
                                  text:
                                      '\n • otherwise doing business on the App/Website or otherwise dealing with any EducationChamp entity. We may receive Personal information about you from third parties, such as social media services, commercially available sources and business partners. If you access App/Website through a social media service or connect a service on App/Website to a social media service, the information we collect may include your user name associated with that social media service, any information or content the social media service has the right to share with us, such as your profile picture, email address or friends list, and any information you have made public in connection with that social media service. When you access the App/Website or otherwise deal with any EducationChamp entity through social media services or when you connect any App/Website to social media services, you are authorizing EducationChamp to collect, store, and use and retain such information and content in accordance with this Privacy Policy.',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                              new TextSpan(
                                  text:
                                      '\n Non-Personal Information: EducationChamp may also collect information other than Personal Information from You through the App/Website when You visit and / or use the App/Website. Such information may be stored in server logs. This Non-Personal Information would not assist EducationChamp to identify You personally. This Non-Personal Information may include: \n Your geographic location,',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                              new TextSpan(
                                  text:
                                      "\n details of Your telecom service provider or internet service provider,\n the type of browser (Internet Explorer, Firefox, Opera, Safari, Google Chrome etc.),\n the operating system of Your system, device and the App/Website You last visited before visiting the App/Website,\n The duration of Your stay on the App/Website is also stored in the session along with the date and time of Your access, Non-Personal Information is collected through various ways such through the use of cookies. EducationChamp may store temporary or permanent ‘cookies' on Your computer. You can erase or choose to block these cookies from Your computer. You can configure Your computer's browser to alert You when we attempt to send You a cookie with an option to accept or refuse the cookie. If You have turned cookies off, You may be prevented from using certain features of the App/Website.",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                              new TextSpan(
                                  text:
                                      "\n Ads: EducationChamp may use third-party service providers to serve ads on EducationChamp's behalf across the internet and sometimes on the App/Website. They may collect Non-Personal Information about Your visits to the App/Website, and Your interaction with our products and services on the App/Website. Please do note that Personal Information and Non Personal Information may be treated differently as per this Privacy Policy.\n \n \n",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                            ]))
                          ],
                        ),
                        Text(
                          ' You hereby represent to EDUCATIONCHAMP that:',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              new TextSpan(
                                  text:
                                      "\n The Information you provide to EducationChamp from time to time is and shall be authentic, correct, current and updated and You have all the rights, permissions and consents as may be required to provide such Information to EducationChamp.\n Your providing the Information to EducationChamp and EducationChamp's consequent storage, collection, usage, transfer, access or processing of the same shall not be in violation of any third party agreement, laws, charter documents, judgments, orders and decrees.\n EducationChamp and each of EducationChamp entities officers, directors, contractors or agents shall not be responsible for the authenticity of the Information that You or any other user provide to EducationChamp. You shall indemnify and hold harmless EducationChamp and each of EducationChamp entities officers, directors, contracts or agents and any third party relying on the Information provided by You in the event You are in breach of this Privacy Policy including this provision and the immediately preceding provision above.\n Your Information will primarily be stored in electronic form however certain data can also be stored in physical form. We may store, collect, process and use your data in countries other than Republic of India but under compliance with applicable laws. We may enter into agreements with third parties (in or outside of India) to store or process your information or data. These third parties may have their own security standards to safeguard your information or data and we will on commercial reasonable basis require from such third parties to adopt reasonable security standards to safeguard your information / data.\n Purpose for collecting, using, storing and processing Your Information EducationChamp collects, uses, stores and processes Your Information for any purpose as may be permissible under applicable laws (including where the applicable law provides for such collection, usage, storage or processes in accordance with the consent of the user) connected with a function or activity of each of EducationChamp entities and shall include the following:\n to facilitate Your use of the App/Website or other services of EducationChamp entities;\n to respond to Your inquiries or fulfil Your requests for information about the various products and services offered on the App/Website;\n to provide You with information about products and services available on the App/Website and to send You information, materials, and offers from EducationChamp;\n to send You important information regarding the App/Website, changes in terms and conditions, user agreements, and policies and/or other administrative information;\n to send You surveys and marketing communications that EducationChamp believes may be of interest to You;\n to personalize Your experience on the App/Website by presenting advertisements, products and offers tailored to Your preferences;\n to help You address Your problems incurred on the App/Website including addressing any technical problems;\n if You purchase any product or avail of any service from the App/Website, to complete and fulfil Your purchase, for example, to have Your payments processed, communicate with You regarding Your purchase and provide You with related customer service;\n for proper administering of the App/Website;\n to conduct internal reviews and data analysis for the App/Website (e.g., to determine the number of visitors to specific pages within the App/Website);\n to improve the services, content and advertising on the App/Website;",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                              new TextSpan(
                                  text:
                                      "\n to provide value added services such as Single Sign On. Single Sign On shall mean a session/user authentication process that permits a user to enter his/her name or mobile number or e-mail address or any combination thereof and password in order to access multiple App/Websites and applications;\nto facilitate various programmes and initiatives launched by EducationChamp or third party service providers and business associates\n to analyze how our services are used, to measure the effectiveness of advertisements, to facilitating payments \n to conducting academic research and surveys \n to protect the integrity of the App/Website;\n to respond to legal, judicial, quasi judicial process and provide information to law enforcement agencies or in connection with an investigation on matters related to public safety, as permitted by law; \n to conduct analytical studies on various aspects including user behaviour, user preferences etc.; \n to permit third parties who may need to contact users who have bought products from the App/Website to facilitate installation, service and any other product related support; \n to implement information security practices; \n to determine any security breaches, computer contaminant or computer virus; \n • to investigate, prevent, or take action regarding illegal activities and suspected fraud, \n • to undertake forensics of the concerned computer resource as a part of investigation or internal audit; \n • to trace computer resources or any person who may have contravened, or is suspected of having or being likely to contravene, any provision of law including the Information Technology Act, 2000 that is likely to have an adverse impact on the services provided on any App/Website or by EducationChamp;",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                              new TextSpan(
                                  text:
                                      '\n • to enable a potential buyer or investor to evaluate the business of EducationChamp (as further described in paragraph 8 of the Privacy Policy). [Individually and collectively referred to as ("Purposes")] You hereby agree and acknowledge that the Information so collected is for lawful purpose connected with a function or activity of each of the EducationChamp entities or any person on their respective behalf, and the collection of Information is necessary for the Purposes. ',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  ))
                            ]))
                          ],
                        ),
                        Text(
                          '\n \n \n Sharing and disclosure of Your Information',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              new TextSpan(
                                text:
                                    '\n \n You hereby unconditionally agree and permit that EducationChamp may transfer, share, disclose or part with all or any of Your Information, within and outside of the Republic of India to various EducationChamp entities and to third party service providers / partners / banks and financial institutions for one or more of the Purposes or as may be required by applicable law. In such case we will contractually oblige the receiving parties of the Information to ensure the same level of data protection that is adhered to by EducationChamp under applicable law.\n \n You acknowledge and agree that, to the extent permissible under applicable laws, it is adequate that when EducationChamp transfers Your Information to any other entity within or outside Your country of residence, EducationChamp will place contractual obligations on the transferee which will oblige the transferee to adhere to the provisions of this Privacy Policy.',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    '\n \n EducationChamp may share statistical data and other details (other than Your Personal Information) without your express or implied consent to facilitates various programmes or initiatives launched by EducationChamp, its affiliates, agents, third party service providers, partners or banks & financial institutions, from time to time. We may transfer/disclose/share Information (other than Your Personal Information) to those parties who support our business, such as providing technical \n \n infrastructure services, analyzing how our services are used, measuring the effectiveness of advertisements, providing customer / buyer services, facilitating payments, or conducting academic research and surveys. These affiliates and third party service providers shall adhere to confidentiality obligations consistent with this Privacy Policy. Notwithstanding the above, We use other third parties such as a credit/debit card processing company, payment gateway, pre-paid cards etc. to enable You to make payments for buying products or availing services on EducationChamp. When You sign up for these services, You may have the ability to save Your card details for future reference and faster future payments. In such case, We may share Your relevant Personal Information as necessary for the third parties to provide such services, including your name, residence and email address. The processing of payments or authorization is solely in accordance with these third parties policies, terms and conditions and we are not in any manner responsible or liable to You or any third party for any delay or failure at their end in processing the payments. \n \n EducationChamp may also share Personal Information if EducationChamp believe it is necessary in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of various terms and conditions or our policies.\n \n We reserve the right to disclose your information when required to do so by law or regulation, or under any legal obligation or order under law or in response to a request from a law enforcement or governmental agency or judicial, quasi-judicial or any other statutory or constitutional authority or to establish or exercise our legal rights or defend against legal claims. \n \n You further agree that such disclosure, sharing and transfer of Your Personal Information and Non-Personal Information shall not cause any wrongful loss to You or to any third party, or any wrongful gain to us or to any third party.',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                            ])),
                          ],
                        ),
                        Text(
                          '\n \n \n Links to third party App/Websites',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: new TextSpan(children: [
                              new TextSpan(
                                text:
                                    '\n \n Links to third-party advertisements, third-party App/Websites or any third party electronic communication service may be provided on the App/Website which are operated by third parties and are not controlled by, or affiliated to, or associated with, EducationChamp unless expressly specified on the App/Website.\n \n EducationChamp is not responsible for any form of transmission, whatsoever, received by You from any third party App/Website. Accordingly, EducationChamp does not make any representations concerning the privacy practices or policies of such third parties or terms of use of such third party App/Websites, nor does \n \n EducationChamp control or guarantee the accuracy, integrity, or quality of the information, data, text, software, music, sound, photographs, graphics, videos, messages or other materials available on such third party App/Websites. The inclusion or exclusion does not imply any endorsement by EducationChamp of the third party',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                              new TextSpan(
                                text:
                                    "\n \n App/Websites, the App/Website's provider, or the information on the App/Website. The information provided by You to such third party App/Websites shall be governed in accordance with the privacy policies of such third party App/Websites and it is recommended that You review the privacy policy of such third party App/Websites prior to using such App/Websites.",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                            ]))
                          ],
                        ),
                        Text(
                          '\n \n \n Security & Retention',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                text:
                                    '\n \n The security of your Personal Information is important to us. EducationChamp strives to ensure the security of Your Personal Information and to protect Your Personal Information against unauthorized access or unauthorized alteration, disclosure or destruction. For this purpose, EducationChamp adopts internal reviews of the data collection, storage and processing practices and security measures, including appropriate encryption and physical security measures to guard against unauthorized access to systems where EducationChamp stores Your Personal Information. Each of the EducationChamp entity shall adopt reasonable security practices and procedures as mandated under applicable laws for the protection of Your Information. Provided that Your right to claim damages shall be limited to the right to claim only statutory damages under Information Technology Act, 2000 and You hereby waive and release all EducationChamp entities from any claim of damages under contract or under tort. \n \n If you choose a payment gateway to complete any transaction on App/Website then Your credit card data may be stored in compliance with industry standards/ recommended data security standard for security of financial information such as the Payment Card Industry Data Security Standard (PCI-DSS).\n \n EducationChamp may share your Information with third parties under a confidentiality agreement which inter alia provides for that such third parties not disclosing the Information further unless such disclosure is for the Purpose. However, EducationChamp is not responsible for any breach of security or for any actions of any third parties that receive Your Personal Information. EducationChamp is not liable for any loss or injury caused to You as a result of You providing Your Personal Information to third party (including any third party App/Websites, even if links to such third party App/Websites are provided on the App/Website). \n \n Notwithstanding anything contained in this Policy or elsewhere, EducationChamp shall not be held responsible for any loss, damage or misuse of Your Personal Information, if such loss, damage or misuse is attributable to a Force Majeure Event (as defined below).\n \n A Force Majeure Event shall mean any event that is beyond the reasonable control of EducationChamp and shall include, without limitation, sabotage, fire, flood, explosion, acts of God, civil commotion, strikes or industrial action of any kind, riots, insurrection, war, acts of government, computer hacking, unauthorised access to computer, computer system or computer network,, computer crashes, breach of security and encryption (provided beyond reasonable control of EducationChamp), power or electricity failure or unavailability of adequate power or electricity. \n \n While We will endeavor to take all reasonable and appropriate steps to keep secure any Personal Information which We hold about You and prevent unauthorized access, You acknowledge that the internet or computer networks are not fully secure and that We cannot provide any absolute assurance regarding the security of Your Personal Information. \n \n You agree that all Personal Information shall be retained till such time required for the Purpose or required under applicable law, whichever is later. Non-Personal Information will be retained indefinitely.',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                            ]))
                          ],
                        ),
                        Text(
                          '\n \n \n User discretion and opt out',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                text:
                                    '\n \n You agree and acknowledge that You are providing your Information out of your free will. You have an option not to provide or permit EducationChamp to collect Your Personal Information or later on withdraw Your consent with respect to such Personal Information so provided herein by sending an email to the grievance officer or such other electronic address of the respective EducationChamp entity as may be notified to You. In such case, You should neither visit any App/Website nor use any services provided by EducationChamp entities nor shall contact any of EducationChamp entities. Further, EducationChamp may not deliver products to You, upon Your order, or EducationChamp may deny you access from using certain services offered on the App/Website. \n \n You can add or update Your Personal Information on regular basis. Kindly note that EducationChamp would retain Your previous Personal Information in its records.',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                            ]))
                          ],
                        ),
                        Text(
                          '\n \n \n Grievance Officer',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                text:
                                    "\n \n If you find any discrepancies or have any grievances in relation to the collection, storage, use, disclosure and transfer of Your Personal Information under this Privacy Policy or any terms of EducationChamp 's Terms of Use, Term of Sale and other terms and conditions or polices of any EducationChamp entity, please contact the following:",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                            ]))
                          ],
                        ),
                        Text(
                          '\n \n \n For Electrocon Consumer Electronics India \t Limited:',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                  text:
                                      '\n \n Mr. Rajesh Awate, the designated grievance officer under Information Technology Act, 2000 E-mail: ',
                                  style: TextStyle(
                                      fontSize: 14.0, color: Colors.black),
                                ),
                                new TextSpan(
                                  text: ' support@educationchamp.com ',
                                  style: new TextStyle(
                                      color: Colors.blue,
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w600),
                                  recognizer: new TapGestureRecognizer()
                                    ..onTap = () {
                                      launch('support@educationchamp.com');
                                    },
                                ),
                                TextSpan(
                                  text:
                                      '\n \n The details of the grievance officer may be changed by us from time to time by updating this Privacy Policy.',
                                  style: TextStyle(
                                      fontSize: 14.0, color: Colors.black),
                                ),
                              ]),
                            )
                          ],
                        ),
                        Text(
                          '\n \n \n Business / Assets Sale or Transfers',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                text:
                                    '\n \n EducationChamp may sell, transfer or otherwise share some or all of its assets, including Your Information in connection with a merger, acquisition, reorganization or sale of assets or business or in the event of bankruptcy. Should such a sale or transfer occur, such EducationChamp entity will reasonably ensure that the Information you have provided and which we have collected is stored and used by the transferee in a manner that is consistent with this Privacy Policy. Any third party to which any of EducationChamp entity transfers or sells as aforesaid will have the right to continue to use the Information that you provide to us or collected by us immediately prior to such transfer or sale.',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                            ]))
                          ],
                        ),
                        Text(
                          '\n \n \n Further Acknowledgements',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Arial"),
                        ),
                        Column(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                                text:
                                    '\n \n You hereby acknowledge and agree that this Privacy Policy:',
                              ),
                              TextSpan(
                                  text:
                                      '\n \n is clear and easily accessible and provide statements of EducationChamp policies and practices with respective to the Information; \n \n provides for the various types of personal or sensitive personal data of information to be collected; \n \n provides for the purposes of collection and usage of the Information; \n \n provides for disclosure of Information; and \n \n provides for reasonable security practices and procedures.',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  )),
                              new TextSpan(text: "\n \n ")
                            ]))
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
