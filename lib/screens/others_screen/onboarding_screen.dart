import 'dart:math';

import 'package:flutter/material.dart';
import 'package:liquid_swipe/liquid_swipe.dart';
class OnbordingScreen extends StatefulWidget {
  static const routeName='/onboarding';
  @override
  _OnbordingScreenState createState() => _OnbordingScreenState();
}

class _OnbordingScreenState extends State<OnbordingScreen> {
  int page = 0;
  LiquidController liquidController;

  UpdateType updateType;

  @override
  void initState() {
    liquidController = LiquidController();
    super.initState();
  }
  static final style = TextStyle(
    fontSize: 30,
    fontFamily: "Billy",
    fontWeight: FontWeight.w600,
  );


  static const TextStyle descriptionWhiteStyle = TextStyle(
      color: Colors.white,
      fontSize: 15.0,
      // fontWeight: FontWeight.bold,
      fontFamily: "Product Sans");

  static const TextStyle whitestyle = TextStyle(
      color: Colors.white, fontSize: 25.0, fontFamily: "Product Sans");
  static const TextStyle boldstyle = TextStyle(
      color: Colors.white,
      // color: Color(0xFF629AFB),
      // color: Color(0xFFAD5CA9),
      // color: Color(0xFFFD974D),
      fontSize: 30.0,
      fontWeight: FontWeight.bold,
      fontFamily: "Product Sans");
  final pages = [
    Container(
      color: Color(0xFFAD5CA9),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset("assets/ob-1.jpeg"),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Book Tutors / Trainers &  ',
                  style: whitestyle,
                ),
                Text(
                  'Connect To Your School / College',
                  style: boldstyle,
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "Book Tutor / Trainer after reviewing profile, experience and rating. Unconditional refund of fee if you decide not to continue the Tutor / Trainer after the Trial Classes.",
                  style: descriptionWhiteStyle,
                ),
              ],
            ),
          )
        ],
      ),
    ),
    Container(
      color: Color(0xFF629AFB),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset("assets/ob-2.jpeg"),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'For Everything You Need In',
                  style: whitestyle,
                ),
                Text(
                  'School & College.',
                  style: boldstyle,
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "Search expert Tutors, hobby classes, sports Trainers and career counsellors in your locality. Book Tutors and Trainers based on School & College.",
                  style: descriptionWhiteStyle,
                ),
              ],
            ),
          )
        ],
      ),
    ),
    Container(
      color: Color(0xFFFD974D),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Image.asset("assets/ob-3.jpeg"),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Total Control In",
                  style: whitestyle,
                ),
                Text(
                  "Your Hands",
                  style: boldstyle,
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "View Attendence, Receive and Submit Assignments, raise queries, receive notifications, chat with Tutors / Tariners, download notes and study material, buy books, pay fee and many more...",
                  style: descriptionWhiteStyle,
                ),
              ],
            ),
          )
        ],
      ),
    ),
  ];

  Widget _buildDot(int index) {
    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((page ?? 0) - index).abs(),
      ),
    );
    double zoom = 1.0 + (2.0 - 1.0) * selectedness;
    return new Container(
      width: 25.0,
      child: new Center(
        child: new Material(
          color: Colors.white,
          type: MaterialType.circle,
          child: new Container(
            width: 8.0 * zoom,
            height: 8.0 * zoom,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Stack(
          children: <Widget>[
            LiquidSwipe(
              pages: pages,
              onPageChangeCallback: pageChangeCallback,
              //change effect value
              waveType: WaveType.circularReveal,
              // waveType: WaveType.liquidReveal,
              liquidController: liquidController,
              enableSlideIcon: true,
              enableLoop: false,
              initialPage: 0,
              slideIconWidget: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Expanded(child: SizedBox()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List<Widget>.generate(pages.length, _buildDot),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: FlatButton(
                  onPressed: () {
                    liquidController.animateToPage(
                        page: pages.length - 1, duration: 500);
                  },
                  child: Text(
                    "Skip",
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Product Sans",
                        fontWeight: FontWeight.w700),
                  ),
                  color: Colors.white.withOpacity(0.01),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: FlatButton(
                  onPressed: () {
                    liquidController.animateToPage(
                        page: liquidController.currentPage + 1, duration: 500);
                  },
                  child: Text(
                    "Next",
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Product Sans",
                        fontWeight: FontWeight.w700),
                  ),
                  color: Colors.white.withOpacity(0.01),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  pageChangeCallback(int lpage) {
    setState(() {
      page = lpage;
    });
  }
}