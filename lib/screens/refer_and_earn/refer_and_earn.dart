import 'package:carousel_slider/carousel_slider.dart';
import 'package:ec_student/widgets/layouts/common_button.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import '../../providers/dummy_data.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';

class ReferAndEarn extends StatefulWidget {
  static const routeName = '/refer-and-earn';

  @override
  _ReferAndEarnState createState() => _ReferAndEarnState();
}

class _ReferAndEarnState extends State<ReferAndEarn> {
  String text = '';
  @override
  Widget build(BuildContext context) {
    _onRefer() {
      Share.share('GFOQ007A');
      print('Refer called');
    }

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Refer & Earn'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 20,
                ),
                child: Column(
                  children: [
                    Container(
                      child: CarouselSlider(
                        options: CarouselOptions(
                          height: 120.0,
                          initialPage: 0,
                          // scrollDirection: Axis.horizontal,
                          // autoPlay: true,
                          // autoPlayInterval: Duration(seconds: 3),
                        ),
                        items: referEarn.map((refer) {
                          return Builder(
                            builder: (BuildContext context) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.symmetric(horizontal: 5.0),
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  child: Image.asset(
                                    refer.reImage,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              );
                            },
                          );
                        }).toList(),
                      ),
                    ),
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 15.0, right: 15.0),
                          width: double.infinity,
                          height: 225,
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    // Padding(
                                    //     padding:
                                    //         const EdgeInsets.only(left: 45.5)),
                                    Image.asset(
                                      "assets/refer-coin.png",
                                      alignment: Alignment.topCenter,
                                      height: 40,
                                      width: 40,
                                    ),
                                    Padding(
                                        padding:
                                            const EdgeInsets.only(left: 20.0)),
                                    Text(
                                      "Invite Your Friends And \n Earn Points",
                                      style: TextStyle(fontSize: 14.0),
                                      textAlign: TextAlign.center,
                                    ),
                                    Padding(
                                        padding:
                                            const EdgeInsets.only(left: 20.0)),
                                    Image.asset(
                                      "assets/refer-coin.png",
                                      alignment: Alignment.topCenter,
                                      height: 40,
                                      width: 40,
                                    )
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10.0),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/refer-coin.png",
                                      alignment: Alignment.center,
                                      height: 30,
                                      width: 30,
                                    ),
                                    SizedBox(
                                      width: 100,
                                    ),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(left: 50.0),
                                      child: Image.asset(
                                        "assets/refer-coin.png",
                                        alignment: Alignment.center,
                                        height: 30,
                                        width: 30,
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 5.0),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/refer-coin.png",
                                      alignment: Alignment.center,
                                      height: 25,
                                      width: 25,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 30.0, right: 30.0),
                                      child: Image.asset(
                                        "assets/collaboration.png",
                                        alignment: Alignment.center,
                                        height: 35,
                                        width: 35,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 1.0),
                                      child: Image.asset(
                                        "assets/refer-coin.png",
                                        alignment: Alignment.center,
                                        height: 25,
                                        width: 25,
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                    padding: const EdgeInsets.only(top: 30.0)),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "YOUR REFERRAL CODE",
                                      style: TextStyle(
                                          fontSize: 13.5,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                            child: Padding(
                          padding: const EdgeInsets.only(top: 220.0),
                          child: SizedBox(
                            width: 160,
                            height: 44,
                            child: RaisedButton(
                              child: Text(
                                "GFOQ007A",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              color: Colors.blue,
                              textColor: Colors.white,
                              padding: EdgeInsets.all(8.0),
                              onPressed: () {},
                            ),
                          ),
                        )),
                      ],
                    ),
                    Container(
                      height: 200,
                      width: double.infinity,
                      color: Colors.white,
                      margin: const EdgeInsets.only(top: 20.0),
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 64.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      height: 3.0,
                                      width: MediaQuery.of(context).size.width*0.75,
                                      margin: EdgeInsets.only(left: 25.0,right: 25.0),
                                      color: Colors.orange[300],
                                    )
                                    // Container(
                                    //   height: 3.0,
                                    //   width: MediaQuery.of(context).size.width*0.25,
                                    //   margin: EdgeInsets.only(left: 116.0),
                                    //   color: Colors.orange[300],
                                    // ),
                                    // Container(
                                    //   height: 3.0,
                                    //   width: 25.0,
                                    //   margin: EdgeInsets.only(right: 116.0),
                                    //   color: Colors.orange[300],
                                    // ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 30.0,
                                    left: 20.0,
                                    right: 20.0,
                                    bottom: 20.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        height: 70,
                                        margin: EdgeInsets.only(
                                          right: 15,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(4),
                                          border:
                                              Border.all(color: Colors.blue),
                                          color: Colors.blue,
                                        ),
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 15.0),
                                          child: Text(
                                            "\n Invite your \n friends with your referral code",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 10.0,
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        height: 70,
                                        margin: EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(4),
                                          border:
                                              Border.all(color: Colors.blue),
                                          color: Colors.blue,
                                        ),
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 15.0),
                                          child: Text(
                                            "\n Your friends get \n a product from \n us",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 10.0,
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        height: 70,
                                        // color: Colors.blue,
                                        margin: EdgeInsets.only(
                                          left: 15,
                                          // right: 10,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(4),
                                          border:
                                              Border.all(color: Colors.blue),
                                          color: Colors.blue,
                                        ),
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 15.0),
                                          child: Text(
                                            "\n You get rewarded \n by points \n",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 10.0,
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 6.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 8.0,
                                      ),
                                      child: MaterialButton(
                                        onPressed: () {},
                                        color: Colors.deepOrange[300],
                                        textColor: Colors.white,
                                        child: Text(
                                          "1",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        padding: EdgeInsets.all(8),
                                        shape: CircleBorder(),
                                      ),
                                    ),
                                    MaterialButton(
                                      onPressed: () {},
                                      color: Colors.deepOrange[300],
                                      textColor: Colors.white,
                                      child: Text(
                                        "2",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      padding: EdgeInsets.all(8),
                                      shape: CircleBorder(),
                                    ),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 8.0),
                                      child: MaterialButton(
                                        onPressed: () {},
                                        color: Colors.deepOrange[300],
                                        textColor: Colors.white,
                                        child: Text(
                                          "3",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        padding: EdgeInsets.all(8),
                                        shape: CircleBorder(),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              
                            ],
                          ),
                          Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: SizedBox(
                                width: 130,
                                child: CommonButton('REFER NOW', _onRefer,true),
                              )),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
