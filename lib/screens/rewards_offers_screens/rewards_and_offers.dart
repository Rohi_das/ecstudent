import 'package:ec_student/screens/rewards_offers_screens/offers.dart';
import 'package:ec_student/screens/rewards_offers_screens/rewards.dart';
import 'package:flutter/material.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';
class RewardsAndOffers extends StatefulWidget {
  static const routeName='/rewards-and-offers';
  @override
  _RewardsAndOffersState createState() => _RewardsAndOffersState();
}

class _RewardsAndOffersState extends State<RewardsAndOffers> {
  bool _isRewardsOn=false;
  bool _isOffersOn=true;
  bool _toggle=true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Rewards & Offers'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Container(
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top,
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 20,
                  ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(padding: EdgeInsets.only(top: 10.0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        
                        SizedBox(
                          width: MediaQuery.of(context).size.width*0.35,
                          height: 35,
                          child: RaisedButton(
                              child: Text(
                                'OFFERS',
                                style: TextStyle(
                                    fontSize: 12.0, color: _isOffersOn?Color(0xFFAD5CA9):Colors.grey[500]),
                              ),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(16.0)),
                                  side: BorderSide(
                                      color: _isOffersOn?Color(0xFFAD5CA9):Colors.grey[500], width: 2)),
                              onPressed: () {
                                setState(() {
                                  _isRewardsOn=false;
                                  _isOffersOn=true;
                                  _toggle=true;
                                });
                              }),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width*0.35,
                          height: 35,
                          child: RaisedButton(
                              child: Text(
                                'REWARDS',
                                style: TextStyle(
                                    fontSize: 12.0, color: _isRewardsOn?Color(0xFFAD5CA9):Colors.grey[500]),
                              ),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(16.0)),
                                  side: BorderSide(
                                      color: _isRewardsOn?Color(0xFFAD5CA9):Colors.grey[500], width: 2)),
                              onPressed: () {
                                setState(() {
                                  _isRewardsOn=true;
                                  _isOffersOn=false;
                                  _toggle=false;
                                });
                              }),
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 20.0)),
                    _toggle?Offers():Rewards()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}