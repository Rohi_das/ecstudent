import 'package:flutter/material.dart';

class Offers extends StatelessWidget {
  static const routeName = '/offers';
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 5.0),
      child: Column(
        children: [
          Padding(padding: EdgeInsets.only(top: 20.0)),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                  'EducationChamp Offers',
                  style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 5.0)),
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
            elevation: 2.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15),
                      )),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Text(
                          'EDUCHAMP50',
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.w900),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Text(
                          '50% Off on your first booking',
                          style: TextStyle(
                              fontSize: 12.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0, top: 10.0),
                      child: Text(
                        '50% Off on your first booking upto Rs.500.',
                        style: TextStyle(
                            fontSize: 10.8, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 60,
                      child: FlatButton(
                          onPressed: null,
                          child: Text(
                            'T&C',
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.orange[400],
                                fontWeight: FontWeight.bold),
                          )),
                    ),
                    FlatButton(
                        onPressed: null,
                        child: Text(
                          'SELECT',
                          style:
                              TextStyle(fontSize: 14.0, color: Colors.blue[900]),
                        ))
                  ],
                )
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 10.0)),
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
            elevation: 2.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15),
                      )),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Text(
                          'EDUCHAMP50',
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.w900),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Text(
                          '50% Off on your first booking',
                          style: TextStyle(
                              fontSize: 12.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0, top: 10.0),
                      child: Text(
                        '50% Off on your first booking upto Rs.500.',
                        style: TextStyle(
                            fontSize: 10.8, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 60,
                      child: FlatButton(
                          onPressed: null,
                          child: Text(
                            'T&C',
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.orange[400],
                                fontWeight: FontWeight.bold),
                          )),
                    ),
                    FlatButton(
                        onPressed: null,
                        child: Text(
                          'SELECT',
                          style:
                              TextStyle(fontSize: 14.0, color: Colors.blue[900]),
                        ))
                  ],
                )
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 20.0)),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                  'Partners Offers',
                  style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 5.0)),
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
            elevation: 2.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15),
                      )),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Text(
                          'EDUCHAMP50',
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.w900),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Text(
                          '50% Off on your first booking',
                          style: TextStyle(
                              fontSize: 12.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0, top: 10.0),
                      child: Text(
                        '50% Off on your first booking upto Rs.500.',
                        style: TextStyle(
                            fontSize: 10.8, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 60,
                      child: FlatButton(
                          onPressed: null,
                          child: Text(
                            'T&C',
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.orange[400],
                                fontWeight: FontWeight.bold),
                          )),
                    ),
                    FlatButton(
                        onPressed: null,
                        child: Text(
                          'SELECT',
                          style:
                              TextStyle(fontSize: 14.0, color: Colors.blue[900]),
                        ))
                  ],
                )
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 30.0)),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                  'Payment Offers',
                  style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 15.0)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                elevation: 2.0,
                child: Column(children: [
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15),
                        )),
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 8.0, top: 5.0),
                          child: Image.asset(
                            'assets/paytm_img.jpg',
                            height: 50,
                            width: 50,
                            alignment: Alignment.center,
                          ),
                        ),
                        Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(right: 50.0, top: 16.0),
                              child: Text(
                                'PAYTM',
                                style: TextStyle(fontSize: 9.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 15.0),
                              child: Text(
                                'Flat Rs.150 Cashback',
                                style: TextStyle(
                                    fontSize: 9.5, fontWeight: FontWeight.w900),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.0)),
                  Row(
                    children: [
                      Padding(padding: EdgeInsets.only(left: 5.0, right: 5.0)),
                      Icon(
                        Icons.supervisor_account,
                        size: 15.0,
                      ),
                      Text(
                        '10k Claimed',
                        style: TextStyle(fontSize: 10.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: Icon(
                          Icons.calendar_today,
                          size: 15.0,
                        ),
                      ),
                      Text('Ends \t', style: TextStyle(fontSize: 10.0)),
                      Text('Oct 10th',
                          style: TextStyle(
                              fontSize: 10.0, fontWeight: FontWeight.bold))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: 60,
                        child: FlatButton(
                            onPressed: () {},
                            child: Text(
                              'T&C',
                              style: TextStyle(
                                  fontSize: 14.0, color: Colors.orange[400]),
                            )),
                      ),
                      FlatButton(
                          onPressed: () {},
                          child: Text(
                            'SELECT',
                            style: TextStyle(
                                fontSize: 14.0, color: Colors.blue[900]),
                          ))
                    ],
                  )
                ]),
              )),
              Expanded(
                  child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                elevation: 2.0,
                child: Column(children: [
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15),
                        )),
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 8.0, top: 5.0),
                          child: Image.asset(
                            'assets/paytm_img.jpg',
                            height: 40,
                            width: 50,
                            alignment: Alignment.center,
                          ),
                        ),
                        Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(right: 28.0, top: 16.0),
                              child: Text(
                                'AMAZON PAY',
                                style: TextStyle(fontSize: 9.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 15.0),
                              child: Text(
                                'Flat Rs.150 Cashback',
                                style: TextStyle(
                                    fontSize: 9.5, fontWeight: FontWeight.w900),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.0)),
                  Row(
                    children: [
                      Padding(padding: EdgeInsets.only(left: 5.0, right: 5.0)),
                      Icon(
                        Icons.supervisor_account,
                        size: 15.0,
                      ),
                      Text(
                        '10k Claimed',
                        style: TextStyle(fontSize: 10.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: Icon(
                          Icons.calendar_today,
                          size: 15.0,
                        ),
                      ),
                      Text('Ends \t', style: TextStyle(fontSize: 10.0)),
                      Text('Oct 10th',
                          style: TextStyle(
                              fontSize: 10.0, fontWeight: FontWeight.bold))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: 60,
                        child: FlatButton(
                            onPressed: () {},
                            child: Text(
                              'T&C',
                              style: TextStyle(
                                  fontSize: 14.0, color: Colors.orange[400]),
                            )),
                      ),
                      FlatButton(
                          onPressed: () {},
                          child: Text(
                            'SELECT',
                            style: TextStyle(
                                fontSize: 14.0, color: Colors.blue[900]),
                          ))
                    ],
                  )
                ]),
              )),
            ],
          )
        ],
      ),
    );
  }
}
