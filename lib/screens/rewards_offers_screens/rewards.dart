import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../screens/help_and_support.dart/faq.dart';
import '../../widgets/layouts/common_button.dart';

class Rewards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    _onEarn() {
      print('on earn call..');
    }

    _onRedeem() {
      print('on redeem call..');
    }

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 15.0, top: 15.0),
          child: Container(
            height: 420,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    bottomLeft: Radius.circular(20))),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, right: 30.0),
                  child: Text(
                    'YOUR REWARD POINTS',
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, right: 30.0),
                  child: FaIcon(
                    FontAwesomeIcons.trophy,
                    size: 110,
                    color: Color(0xFFAD5CA9),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, right: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Material(
                          type: MaterialType
                              .transparency, //Makes it usable on any background color, thanks @IanSmith
                          child: Ink(
                            decoration: BoxDecoration(
                              border:
                                  Border.all(color: Colors.orange, width: 1.0),
                              color: Colors.white,
                              shape: BoxShape.circle,
                            ),
                            child: InkWell(
                              //This keeps the splash effect within the circle
                              borderRadius: BorderRadius.circular(
                                  100.0), //Something large to ensure a circle
                              // onTap: _messages,
                              child: Icon(
                                Icons.stars,
                                size: 30.0,
                                color: Colors.orange,
                              ),
                            ),
                          )),
                      Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: Text(
                          '1150',
                          style: TextStyle(
                              fontSize: 35.0, fontWeight: FontWeight.w700),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15.0),
                  child: Container(
                    padding: EdgeInsets.only(left: 40.0, right: 20.0),
                    child: LinearPercentIndicator(
                      width: 290,
                      // animation: true,
                      lineHeight: 5.0,
                      percent: 0.57,
                      linearStrokeCap: LinearStrokeCap.roundAll,
                      progressColor: Color(0xFFAD5CA9),
                      backgroundColor: Colors.grey[350],
                      // valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 40.0),
                        child: Text(
                          '0',
                          style: TextStyle(
                              fontSize: 11.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Text(
                        '1150/ 2000 Points',
                        style: TextStyle(
                            fontSize: 12.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w700),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 25.0),
                        child: Text(
                          '2000',
                          style: TextStyle(
                              fontSize: 11.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0, right: 25.0),
                  child: Text(
                    'Earn Points',
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0, right: 30.0),
                  child: Text(
                    '2000 Points = 10₹ Reward',
                    style: TextStyle(
                        fontSize: 12.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30.0, right: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                          width: 130, child: CommonButton('EARN', _onEarn, true)),
                      SizedBox(
                          width: 130, child: CommonButton('REDEEM', _onRedeem,true)),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: SizedBox(
                  width: 110,
                  height: 30,
                  child: RaisedButton(
                      child: Text(
                        'How to earn',
                        style:
                            TextStyle(fontSize: 12.0, color: Colors.blue[900]),
                      ),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(16.0)),
                          side: BorderSide(color: Colors.grey[700], width: 1)),
                      onPressed: () {}),
                ),
              ),
              SizedBox(
                width: 60,
                height: 30,
                child: RaisedButton(
                    child: Text(
                      'FAQ',
                      style: TextStyle(fontSize: 12.0, color: Colors.blue[900]),
                    ),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(16.0)),
                        side: BorderSide(color: Colors.grey[700], width: 1)),
                    onPressed: () {
                      Navigator.of(context).pushNamed(FAQ.routeName);
                    }),
              ),
            ],
          ),
        )
      ],
    );
  }
}
