import 'package:ec_student/widgets/layouts/app_bar_back_button.dart';
import 'package:ec_student/widgets/layouts/app_bar_title.dart';
import 'package:ec_student/widgets/layouts/common_button.dart';
import 'package:ec_student/widgets/layouts/side_screen.dart';
import 'package:flutter/material.dart';
class AddStudent extends StatefulWidget {
  static const routeName='/add-student';
  @override
  _AddStudentState createState() => _AddStudentState();
}

class _AddStudentState extends State<AddStudent> {
  final _signupForm = GlobalKey<FormState>();
  final _fnameCtrl = TextEditingController();
  final _lnameCtrl = TextEditingController();
  final _mobileNumberCtrl = TextEditingController();
  final _emailCtrl = TextEditingController();
  final _passwordCtrl = TextEditingController();


  @override
  Widget build(BuildContext context) {
    _submit(){}
    _borders(hintText, iconName) {
                return InputDecoration(
                  hintText: hintText,
                  isDense: true,
                  contentPadding: EdgeInsets.fromLTRB(10, 30, 10, 0),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: const BorderSide(color: Color(0xFFC77DC1), width: 1.5),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: const BorderSide(color: Colors.red, width: 1.5),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: const BorderSide(color: Colors.red, width: 1.5),
                  ),
                  prefixIcon: Icon(
                    iconName,
                    color: Colors.grey,
                  ),
                );
              }
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Add Student'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 20,
                ),
                child: SingleChildScrollView(
        child: Container(
                    padding: EdgeInsets.only(left: 8, right: 12),
                    height: MediaQuery.of(context).size.height*0.50,
                    
                    child: Form(
                            key: _signupForm,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                 Row(
                                   mainAxisAlignment: MainAxisAlignment.spaceAround,
                                   children: [
                                     Expanded(
                                       child: TextFormField(
                                         controller: _fnameCtrl,
                                         validator: (value) {
                                           String newData=value.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
                                           RegExp regex = new RegExp(r'^[a-zA-Z]+$');
                                           if (value.isEmpty) {
                                             return 'First name required';
                                           }
                                           if (!(regex.hasMatch(newData))) {
                                             return 'Enter valid first name';
                                           }
                                           return null;
                                         },
                                         decoration: InputDecoration(
                                           hintText: "First Name",
                                           isDense: true,
                                           contentPadding:
                                               EdgeInsets.fromLTRB(20, 30, 0, 0),
                                           enabledBorder: OutlineInputBorder(
                                             borderRadius: BorderRadius.circular(50),
                                             borderSide: const BorderSide(
                                                 color: Colors.grey, width: 1.0),
                                           ),
                                           focusedBorder: OutlineInputBorder(
                                             borderRadius: BorderRadius.circular(50),
                                             borderSide: const BorderSide(
                                                 color: Color(0xFFC77DC1),
                                                 width: 1.5),
                                           ),
                                           focusedErrorBorder: OutlineInputBorder(
                                             borderRadius: BorderRadius.circular(50),
                                             borderSide: const BorderSide(
                                                 color: Color(0xFFC77DC1),
                                                 width: 1.5),
                                           ),
                                           errorBorder: OutlineInputBorder(
                                             borderRadius: BorderRadius.circular(50),
                                             borderSide: const BorderSide(
                                                 color: Colors.red, width: 1.5),
                                           ),
                                         ),
                                       ),
                                     ),
                                     SizedBox(
                                       width: 10,
                                     ),
                                     Expanded(
                                       child: TextFormField(
                                         controller: _lnameCtrl,
                                         validator: (value) {
                                           String lastData=value.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
                                           RegExp regex = new RegExp(r'^[a-zA-Z]+$');
                                           if (value.isEmpty) {
                                             return 'Last name required';
                                           }
                                           if (!(regex.hasMatch(lastData))) {
                                             return 'Enter valid last name';
                                           }
                                           return null;
                                         },
                                         decoration: InputDecoration(
                                           hintText: "Last Name",
                                           isDense: true,
                                           contentPadding:
                                               EdgeInsets.fromLTRB(20, 30, 0, 0),
                                           enabledBorder: OutlineInputBorder(
                                             borderRadius: BorderRadius.circular(50),
                                             borderSide: const BorderSide(
                                                 color: Colors.grey, width: 1.0),
                                           ),
                                           focusedBorder: OutlineInputBorder(
                                             borderRadius: BorderRadius.circular(50),
                                             borderSide: const BorderSide(
                                                 color: Color(0xFFC77DC1),
                                                 width: 1.5),
                                           ),
                                           focusedErrorBorder: OutlineInputBorder(
                                             borderRadius: BorderRadius.circular(50),
                                             borderSide: const BorderSide(
                                                 color: Color(0xFFC77DC1),
                                                 width: 1.5),
                                           ),
                                           errorBorder: OutlineInputBorder(
                                             borderRadius: BorderRadius.circular(50),
                                             borderSide: const BorderSide(
                                                 color: Colors.red, width: 1.5),
                                           ),
                                         ),
                                       ),
                                     ),
                                   ],
                                 ),
                                 TextFormField(
                                   controller: _mobileNumberCtrl,
                                   validator: (value) {
                                     if (value.isEmpty) {
                                       return 'Mobile no. required';
                                     }
                                     if (value.length > 10 || value.length < 10) {
                                       return 'Please enter valid mobile no.';
                                     }
                                     return null;
                                   },
                                   keyboardType: TextInputType.phone,
                                   decoration: _borders(
                                       'India Mobile No.(10 Digits)',
                                       Icons.smartphone),
                                 ),
                                 TextFormField(
                                   controller: _emailCtrl,
                                   validator: (value) {
                                     Pattern pattern =
                                         r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                     RegExp regex = new RegExp(pattern);
                                     if (value.isEmpty) {
                                       return 'Email required';
                                     }
                                     if (!(regex.hasMatch(value))) {
                                       return 'Enter valid email';
                                     }
                                     return null;
                                   },
                                   decoration: _borders('Email', Icons.mail_outline),
                                 ),
                                 TextFormField(
                                   controller: _passwordCtrl,
                                   validator: (value) {
                                     if (value.isEmpty) {
                                       return 'Password required';
                                     }
                                     if (value.length < 8) {
                                       return 'Enter 8 digit password';
                                     }
                                     return null;
                                   },
                                   obscureText: true,
                                   decoration: _borders(
                                       'Create Password', Icons.lock_outline),
                                 ),
                                 Text('Have a referral code?'),
                                 SizedBox(
                                     width: 130,
                                     child: CommonButton('SUBMIT', _submit,true)),
                                 Row(
                                   children: [
                                     Text(
                                       'By Submitting, I accept the ',
                                       style: TextStyle(
                                         fontSize: 12,
                                       ),
                                     ),
                                     Text(
                                       'term of use & privacy policy.',
                                       style: TextStyle(
                                         fontSize: 12,
                                         color: Theme.of(context).primaryColor,
                                       ),
                                     ),
                                   ],
                                 ),
                     
                              ],
                            ),
                          ),
                  ),
      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}