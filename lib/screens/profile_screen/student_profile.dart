import 'package:ec_student/screens/login_screens/signup.dart';
import 'package:ec_student/screens/profile_screen/add_student.dart';
import 'package:ec_student/screens/search_screen/on_map_search_screen.dart';
import 'package:ec_student/screens/search_screen/user_location_and_address.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/common_button.dart';
import '../../widgets/layouts/side_screen.dart';

enum Gender { male, female }

class StudentProfile extends StatefulWidget {
  static const routeName = '/student-profile';
  bool _isLoading = true;
  @override
  _StudentProfileState createState() => _StudentProfileState();
}

class _StudentProfileState extends State<StudentProfile> {
  final _studentForm = GlobalKey<FormState>();
  Gender _gender = Gender.male;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String _currentAddress;
  final _addressCtrl = TextEditingController();
  final _houseCtrl = TextEditingController();
  final _builingCtrl = TextEditingController();

  _submit() {
    Navigator.of(context).pushNamed(SearchScreen.routeName);
    print('Submi in called...');
    final isValid = _studentForm.currentState.validate();

    if (!isValid) {
      return;
    }
    print('Log Form');
    _studentForm.currentState.save();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCurrentLocation();
    // print(_currentAddress);
  }

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    print(widget._isLoading);
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.locality}, ${place.postalCode}, ${place.country}";

        _addressCtrl.text = _currentAddress;
        widget._isLoading = false;
      });
    } catch (e) {
      print(e);
    }
    print(widget._isLoading);
  }

  @override
  Widget build(BuildContext context) {
    _border(hintText) {
      return InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(fontSize: 12.0),
        isDense: true,
        contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 0),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: const BorderSide(color: Color(0xFFC77DC1), width: 1.5),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: BorderSide(color: Colors.red, width: 1.5),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          borderSide: BorderSide(color: Colors.red, width: 1.5),
        ),
      );
    }

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Additional Profile'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Container(
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top,
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                left: 20,
              ),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 8.0,
                      left: 8.0,
                      right: 14.0,
                    ),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Select Student',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: InkWell(
                                    onTap: (){
                                      Navigator.of(context).pushNamed(AddStudent.routeName);
                                    },
                                                                      child: Text(
                                      'Add Student',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Theme.of(context)
                                              .textSelectionColor),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 20,
                                  width: 20,
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      Navigator.of(context).pushNamed(AddStudent.routeName);
                                    },
                                    elevation: 2.0,
                                    fillColor: Theme.of(context).primaryColor,
                                    child: Icon(
                                      Icons.add,
                                      size: 15.0,
                                      color: Colors.white,
                                    ),
                                    shape: CircleBorder(),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              height: 30,
                              // width: 140,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 2.0, vertical: 5.0),
                              child: OutlineButton(
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                                child: Text(
                                  'Raj',
                                  style: TextStyle(
                                    color: Theme.of(context).disabledColor,
                                  ),
                                ),
                                onPressed: () {},
                              ),
                            ),
                            Container(
                              height: 20,
                              // width: 140,
                              padding: EdgeInsets.symmetric(horizontal: 2.0),
                              child: OutlineButton(
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                                child: Text(
                                  'Simran',
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                                onPressed: () {},
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Container(
                          width: double.infinity,
                          child: Form(
                            key: _studentForm,
                            child: Column(
                              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Address* :',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.of(context).pushNamed(
                                        UserLocationAndAddress.routeName);
                                  },
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: TextFormField(
                                          enabled: false,
                                          controller: _addressCtrl,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Home location required';
                                            }
                                            return null;
                                          },
                                          decoration: InputDecoration(
                                            suffix: 
                                            // widget._isLoading?                                                ? 
                                                Container(
                                                    height: 20,
                                                    width: 20,
                                                    child: RawMaterialButton(
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pushNamed(
                                                                UserLocationAndAddress
                                                                    .routeName);
                                                      },
                                                      elevation: 2.0,
                                                      fillColor:
                                                          Theme.of(context)
                                                              .primaryColor,
                                                      child: Icon(
                                                        Icons.map,
                                                        size: 15.0,
                                                        color: Colors.white,
                                                      ),
                                                      shape: CircleBorder(),
                                                    ),
                                                  ),
                                                // Text('feacting data',style:TextStyle(color:Colors.grey))
                                                // CircularProgressIndicator(backgroundColor:
                                                //             Theme.of(context)
                                                //                 .primaryColor,)
                                                // : SizedBox(),
                                            // null,
                                            hintText: 'Your Home Location',
                                            hintStyle:
                                                TextStyle(fontSize: 12.0),
                                            isDense: true,
                                            contentPadding: EdgeInsets.fromLTRB(
                                                20, 20, 20, 0),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              borderSide: const BorderSide(
                                                  color: Colors.grey,
                                                  width: 1.0),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              borderSide: const BorderSide(
                                                  color: Colors.grey,
                                                  width: 1.0),
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              borderSide: const BorderSide(
                                                  color: Color(0xFFC77DC1),
                                                  width: 1.5),
                                            ),
                                            errorBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              borderSide: BorderSide(
                                                  color: Colors.red,
                                                  width: 1.5),
                                            ),
                                            focusedErrorBorder:
                                                OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              borderSide: BorderSide(
                                                  color: Colors.red,
                                                  width: 1.5),
                                            ),
                                          ),
                                        ),
                                      ),
                                      // !_isLoading
                                      //         ? SizedBox(
                                      //             height: 20,
                                      //             width: 20,
                                      //             child:
                                      //                 CircularProgressIndicator(
                                      //               backgroundColor:
                                      //                   Theme.of(context)
                                      //                       .primaryColor,
                                      //             ))
                                      //         : SizedBox(),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 2.0,
                                ),
                                Row(
                                  // mainAxisAlignment:
                                  //     MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 15),
                                      child: Text(
                                        '(If your location is wrong, click to select correct location on map.)',
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey[500]),
                                      ),
                                    ),
                                    // Container(
                                    //   height: 20,
                                    //   width: 20,
                                    //   child: RawMaterialButton(
                                    //     onPressed: () {
                                    //       Navigator.of(context).pushNamed(
                                    //           UserLocationAndAddress.routeName);
                                    //     },
                                    //     elevation: 2.0,
                                    //     fillColor:
                                    //         Theme.of(context).primaryColor,
                                    //     child: Icon(
                                    //       Icons.map,
                                    //       size: 15.0,
                                    //       color: Colors.white,
                                    //     ),
                                    //     shape: CircleBorder(),
                                    //   ),
                                    // ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: TextFormField(
                                        controller: _houseCtrl,
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return 'House No./ Flat No. required';
                                          }
                                          return null;
                                        },
                                        decoration:
                                            _border('House No./ Flat No.'),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                        controller: _builingCtrl,
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return 'Building name required';
                                          }
                                          return null;
                                        },
                                        decoration: _border('Building Name'),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  // crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Gender* :',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Radio(
                                      activeColor:
                                          Theme.of(context).primaryColor,
                                      value: Gender.male,
                                      groupValue: _gender,
                                      onChanged: (Gender value) {
                                        setState(() {
                                          _gender = value;
                                        });
                                      },
                                    ),
                                    const Text(
                                      'Male',
                                      style: TextStyle(fontSize: 12),
                                    ),
                                    Radio(
                                      activeColor:
                                          Theme.of(context).primaryColor,
                                      value: Gender.female,
                                      groupValue: _gender,
                                      onChanged: (Gender value) {
                                        setState(() {
                                          _gender = value;
                                        });
                                      },
                                    ),
                                    const Text(
                                      'Female',
                                      style: TextStyle(fontSize: 12),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.account_circle,
                                      color: Theme.of(context).primaryColor,
                                      size: 80,
                                    ),
                                  ],
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Add/Edit Student Photo',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: Theme.of(context)
                                                .textSelectionColor),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  'For Online Classes :',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                TextFormField(
                                  decoration: _border('Skype ID'),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                TextFormField(
                                  decoration: _border('Google Meet ID'),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                TextFormField(
                                  decoration: _border("Parent's Name"),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                TextFormField(
                                  decoration: _border("Parent's Mobile No."),
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    CommonButton('Submit', _submit, true),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
