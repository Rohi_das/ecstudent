import 'dart:io';
import 'dart:async';
import 'package:ec_student/screens/booking_screen/booking.dart';
import 'package:ec_student/screens/login_screens/forgot_password.dart';
import 'package:ec_student/screens/profile_screen/student_profile.dart';
import 'package:ec_student/widgets/layouts/app_bar_back_button.dart';
import 'package:ec_student/widgets/layouts/app_bar_title.dart';
import 'package:ec_student/widgets/layouts/side_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart'; 

class UpdateProfile extends StatefulWidget {
  static const routeName = '/user_profile';
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {
  File _pickedImage;
  File _image;
  final _picker = ImagePicker();
  String name = 'Rohidas Gijare';
  String mobile_no = '8793721421';
  String mail_id = 'rohidas25@gmail.com';
  TextEditingController name_Controller = TextEditingController();
  TextEditingController mobile_no_Controller = TextEditingController();
  TextEditingController mail_id_Controller = TextEditingController();

  final picker = ImagePicker();
  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  _border(hintText, icon) {
    return InputDecoration(
      hintText: hintText,
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(30, 30, 30, 0),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: const BorderSide(color: Colors.grey, width: 1.0),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: const BorderSide(color: Color(0xFFC77DC1), width: 1.5),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: Colors.red, width: 1.5),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: Colors.red, width: 1.5),
      ),
      prefixIcon: Icon(
        icon,
        color: Colors.grey,
      ),
    );
  }

  _loadPicker(ImageSource source) async {
    File picked = await ImagePicker.pickImage(source: source);
    if (picked != null) {
      // setState(() {
      //   _pickedImage = picked;
      // });
      _cropImage(picked);
    }
    Navigator.pop(context);
  }

  //   _loadPicker(ImageSource source) async {
  //   // File _image;
  //   // final pickedFile = await _picker.getImage(source: source);
  //   PickedFile image = await _picker.getImage(source: source);
  //   if (image != null) {
  //     setState(() {
  //     if (image != null) {
  //       _image = File(image.path);
  //     } else {
  //       print('No image selected.');
  //     }
  //   });
  //     _cropImage(_image);
  //   }
  //   Navigator.pop(context);
  // }

  _cropImage(File picked) async {
    File cropped = await ImageCropper.cropImage(
        androidUiSettings: AndroidUiSettings(
          statusBarColor: Colors.red,
          toolbarColor: Colors.red,
          toolbarTitle: "Crop Image",
          toolbarWidgetColor: Colors.white,
        ),
        sourcePath: picked.path,
        // aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio16x9,
          CropAspectRatioPreset.ratio4x3,
        ]);
    if (cropped != null) {
      setState(() {
        _pickedImage = cropped;
      });
    }
  }

  void _showPickedOptionsDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    title: Text("Pick from Gallery"),
                    onTap: () {
                      
                      // _loadPicker(ImageSource.gallery);
                    },
                  ),
                  ListTile(
                    title: Text("Take a picture"),
                    onTap: () {
                      // _loadPicker(ImageSource.camera);
                    },
                  ),
                ],
              ),
            ));
  }

  void _settingModalBottomSheet(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    name_Controller.text = name;
    mobile_no_Controller.text = mobile_no;
    mail_id_Controller.text = mail_id;
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext bc) => Container(
        height: MediaQuery.of(context).size.height * 0.85,
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(16.0),
            topRight: const Radius.circular(16.0),
          ),
        ),
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Container(
            height: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: TextFormField(
                    autofocus: true,
                    controller: name_Controller,
                    validator: (String value) {
                      String pattern = r'^[a-zA-Z\s]+$';
                      RegExp regExp = new RegExp(pattern);

                      if (value.isEmpty) {
                        return 'Name required';
                      } else if (!regExp.hasMatch(value)) {
                        return "Name must be a-z And A-Z";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.name,
                    decoration: _border('Enter Your Name', Icons.person),
                  ),
                ),
                // Text(result),

                Padding(
                  padding: EdgeInsets.all(15),
                  child: TextFormField(
                    controller: mobile_no_Controller,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Mobile number required';
                      }
                      if (value.length > 10 || value.length < 10) {
                        return 'Enter valid mobile no.';
                      }
                      return null;
                    },
                    keyboardType: TextInputType.phone,
                    decoration:
                        _border('Enter Mobile Number', Icons.local_phone),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.all(15),
                  child: TextFormField(
                    controller: mail_id_Controller,
                    validator: (value) {
                      String pattern = r'^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$';
                      RegExp regExp = new RegExp(pattern);
                      if (value.isEmpty) {
                        return 'Mail ID required';
                      } else if (!regExp.hasMatch(value)) {
                        return "Invalid Email id";
                      }

                      return null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    decoration: _border('Enter Mail ID', Icons.email),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RaisedButton(
                      onPressed: () {
                        setState(() {
                          name = name_Controller.text;
                          mobile_no = mobile_no_Controller.text;
                          mail_id = mail_id_Controller.text;
                        });
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'Update',
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      color: Theme.of(context).buttonColor,
                      elevation: 10,
                      shape: StadiumBorder(),
                    ),
                    RaisedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      color: Theme.of(context).buttonColor,
                      elevation: 10,
                      shape: StadiumBorder(),
                      child: Text(
                        'Cancel',
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Profile'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 15,
                ),
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.white,
                        // boxShadow: [
                        //   BoxShadow(
                        //     color: Colors.grey,
                        //     offset: Offset(0.0, 1.0), //(x,y)
                        //     blurRadius: 6.0,
                        //   ),
                        // ],
                      ),
                      height: 120,
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Stack(
                            overflow: Overflow.clip,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  // _showPickedOptionsDialog(context);
                                },
                                child: CircleAvatar(
                                  radius: 50,
                                  child: _pickedImage == null
                                      ? Text('Picture')
                                      : null,
                                  backgroundImage: _pickedImage != null
                                      ? FileImage(_pickedImage)
                                      : null,
                                ),
                              ),
                              Positioned(
                                bottom: -10,
                                right: -8,
                                child: Ink(
                                  decoration: const ShapeDecoration(
                                    color: Colors.white,
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: FaIcon(
                                      FontAwesomeIcons.camera,
                                      color: Color(0xFFAD5CA9),
                                      size: 16,
                                    ),
                                    onPressed: () {
                                      getImage();
                                      // _showPickedOptionsDialog(context);
                                    },
                                    // alignment: Alignment.topRight,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            width: 10.0,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    // width: 180,
                                    width:
                                        MediaQuery.of(context).size.width * 0.4,
                                    child: Text(
                                      '$name \t ',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                          // height: 2.0
                                          color: Color(0xFFAD5CA9)),
                                    ),
                                  ),
                                  Container(
                                    // width: 30,
                                    width:
                                        MediaQuery.of(context).size.width * 0.1,
                                    child: IconButton(
                                      icon: FaIcon(FontAwesomeIcons.pencilAlt,
                                          size: 16, color: Color(0xFFAD5CA9)),
                                      onPressed: () {
                                        _settingModalBottomSheet(context);
                                      },
                                      // alignment: Alignment.topRight,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.local_phone,
                                    color: Colors.grey[800],
                                    size: 16,
                                  ),
                                  Text(
                                    '\t $mobile_no',
                                    style: TextStyle(
                                        // fontSize: 16.0,
                                        //  height: 1.0
                                        color: Colors.grey[800]),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.email,
                                    color: Colors.grey[800],
                                    size: 16,
                                  ),
                                  Text(
                                    '\t $mail_id',
                                    style: TextStyle(color: Colors.grey[800]),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      height: 250,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: EdgeInsets.only(left: 20.0, right: 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 25.0,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.of(context).pushNamed(StudentProfile.routeName);
                              },
                              child: Row(
                                children: [
                                  FaIcon(
                                    FontAwesomeIcons.userEdit,
                                    size: 16,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      'Update Additional Field',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  // FlatButton(
                                  //   onPressed: () {},
                                  //   child: Text(
                                  //     'Update Additional Field',
                                  //   ),
                                  // ),
                                ],
                              ),
                            ),
                            Divider(
                              color: Colors.grey[400],
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.of(context).pushNamed(Booking.routeName);
                              },
                              child: Row(
                                children: [
                                  FaIcon(
                                    FontAwesomeIcons.history,
                                    size: 16.0,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      'Booking History',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  // FlatButton(
                                  //   onPressed: () {},
                                  //   child: Text(
                                  //     'Booking History',
                                  //   ),
                                  // ),
                                ],
                              ),
                            ),
                            Divider(
                              color: Colors.grey[400],
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.of(context).pushNamed(ForgotPassword.routeName);
                              },
                              child: Row(
                                children: [
                                  FaIcon(
                                    FontAwesomeIcons.key,
                                    size: 16.0,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      'Change Password',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  // FlatButton(
                                  //   onPressed: () {},
                                  //   child: Text(
                                  //     'Change Password',
                                  //   ),
                                  // ),
                                ],
                              ),
                            ),
                            Divider(
                              color: Colors.grey[400],
                            ),
                            InkWell(
                              onTap: () {},
                              child: Row(
                                children: [
                                  FaIcon(
                                    FontAwesomeIcons.powerOff,
                                    size: 16.0,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      'Logout',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  // FlatButton(
                                  //     onPressed: () {},
                                  //     child: Text(
                                  //       'Logout',
                                  //     )),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
