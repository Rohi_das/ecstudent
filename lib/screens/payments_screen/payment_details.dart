import 'package:ec_student/models/payments/payments.dart';
import 'package:flutter/material.dart';

import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';

class PaymentDetails extends StatelessWidget {
  static const routeName = '/payment-details';
  @override
  Widget build(BuildContext context) {
    final routeData = ModalRoute.of(context).settings.arguments as Payments;
    print(routeData.name);
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Payment Details'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Padding(
              padding: const EdgeInsets.only(top:8.0 ),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 20,
                ),
                child: Stack(
                  overflow: Overflow.clip,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 40.0),
                      child: Container(
                        width: double.infinity,
                        color: Theme.of(context).accentColor,
                        padding: EdgeInsets.only(top: 70, left: 15, right: 15),
                        height: routeData.paymentType != 'credit'?250:180,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  routeData.name,
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500),
                                ),
                                Row(
                                  children: [
                                    routeData.status == 'Failed'
                                        ? Icon(
                                            Icons.highlight_off,
                                            size: 16,
                                            color: Colors.red,
                                          )
                                        : Icon(
                                            Icons.check_circle_outline,
                                            size: 16,
                                            color: Colors.green,
                                          ),
                                    SizedBox(width: 5.0),
                                    Text(
                                      routeData.status,
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            Text(
                              'Order Id : ' + routeData.orderId,
                              style: TextStyle(color: Colors.grey[500]),
                            ),
                            Text(
                              routeData.date + '. ' + routeData.time,
                              style: TextStyle(color: Colors.grey[500]),
                            ),
                            Divider(
                              color: Colors.grey,
                            ),
                            routeData.paymentType != 'credit'?Text('Payment Mode'): SizedBox(),
                            routeData.paymentType != 'credit'
                                ? Divider(
                                    color: Colors.grey,
                                  )
                                : SizedBox(),
                            routeData.paymentType != 'credit'
                                ? Text(routeData.paymentMode)
                                : SizedBox(),
                            routeData.paymentType != 'credit'
                                ? Divider(
                                    color: Colors.grey,
                                  )
                                : SizedBox(),
                            Text('Transaction Id : ' + routeData.txId),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            height: 80,
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 2.0,
                                  spreadRadius: 1.0,
                                  offset: Offset(0.0,
                                      2.0), // shadow direction: bottom right
                                )
                              ],
                            ),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                routeData.paymentType != 'credit'
                                    ? Text(
                                        'Rs.',
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).accentColor,
                                            fontWeight: FontWeight.bold),
                                      )
                                    : SizedBox(),
                                Text(
                                  routeData.amount,
                                  style: TextStyle(
                                      color: Theme.of(context).accentColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 40),
                                ),
                              ],
                            )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
