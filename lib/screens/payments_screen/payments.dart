import 'package:ec_student/models/payments/payment_type.dart';
import 'package:ec_student/screens/payments_screen/payment_details.dart';
import 'package:flutter/material.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';
import '../../providers/dummy_data.dart';

class Payments extends StatefulWidget {
  static const routeName = '/payments';
  @override
  _PaymentsState createState() => _PaymentsState();
}

class _PaymentsState extends State<Payments> {
  int _selectPaymentType = 0;
  List _payments = payments;
  _filter(String type, int index) {
    _payments = [];
    if (type == 'all') {
      setState(() {
        _payments = payments;
        _selectPaymentType = index;
      });
    } else {
      setState(() {
        _payments =
            payments.where((element) => element.paymentType == type).toList();
        _selectPaymentType = index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Payments'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Container(
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top,
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                left: 20,
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 30,
                      child: ListView.builder(
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  side: BorderSide(
                                    color: _selectPaymentType == index
                                        ? Colors.purple
                                        : Colors.grey[500],
                                  )),
                              color: Theme.of(context).canvasColor,
                              textColor: _selectPaymentType == index
                                  ? Colors.purple
                                  : Colors.grey[500],
                              padding: EdgeInsets.all(8.0),
                              onPressed: () {
                                _filter(paymentType[index].type, index);
                              },
                              child: Text(
                                paymentType[index].title,
                                style: TextStyle(
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                          );
                        },
                        itemCount: paymentType.length,
                        scrollDirection: Axis.horizontal,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height * 0.85,
                      child: ListView.separated(
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).pushNamed(PaymentDetails.routeName, arguments:_payments[index] );
                              },
                              child: ListTile(
                                leading: CircleAvatar(
                                  backgroundColor:
                                      Theme.of(context).primaryColor,
                                  child: Text(
                                    _payments[index].name[0],
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor),
                                  ),
                                ),
                                title: Text(_payments[index].name),
                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(_payments[index].date),
                                    Text(
                                      _payments[index].status,
                                      style: TextStyle(
                                        color:
                                            _payments[index].status == 'Failed'
                                                ? Colors.red
                                                : Colors.green,
                                      ),
                                    ),
                                  ],
                                ),
                                isThreeLine: true,

                                trailing: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(_payments[index].subject),
                                    Text(
                                      _payments[index].paymentType=='credit'?''+_payments[index].amount:'Rs.'+_payments[index].amount,
                                      style: TextStyle(
                                        color:
                                            _payments[index].status == 'Failed'
                                                ? Colors.red
                                                : Colors.green,
                                      ),
                                    ),
                                  ],
                                ),
                                // dense: true,
                              ),
                            ),
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) =>
                            Divider(height: 1),
                        itemCount: _payments.length,
                        scrollDirection: Axis.vertical,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
