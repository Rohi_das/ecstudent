import 'package:ec_student/widgets/layouts/app_bar_back_button.dart';
import 'package:ec_student/widgets/layouts/app_bar_title.dart';
import 'package:ec_student/widgets/layouts/side_screen.dart';
import 'package:flutter/material.dart';

class TransactionDetailsConfimed extends StatelessWidget {
  static const routeName = '/transaction-details-confirmed';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Transaction Detail'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Container(
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top,
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 20,
                  right: 5.0),
              child: Column(
                children: [
                  Container(
                    height: 263,
                    // width: 400,
                    // padding:
                    //     EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15.0),
                        topRight: Radius.circular(15.0),
                      )),
                      child: Column(
                        children: [
                          Container(
                            height: 50,
                            decoration: BoxDecoration(
                                color: Colors.grey[350],
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                )),
                            child: Row(
                              children: [
                                Column(
                                  children: [
                                    Padding(
                                        padding: EdgeInsets.only(top: 10.0)),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(left: 15.0),
                                      child: Text(
                                        'Order Id ',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 12.0),
                                      child: Text(
                                        'EC101',
                                        style: TextStyle(
                                          fontSize: 12.0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 190.0),
                                  child: Text('Confirmed',
                                      style: TextStyle(
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.green[800],
                                      )),
                                ),
                              ],
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 10.0)),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15.0),
                                child: Text(
                                  'Token Amount',
                                  style: TextStyle(fontSize: 14.0),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 190.0),
                                child: Text(
                                  'Rs 150.',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.green[800],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Divider(
                            height: 20.0,
                            color: Colors.grey[350],
                            thickness: 1.5,
                            indent: 14.0,
                            endIndent: 5.0,
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15.0),
                                child: Text('Token Date',
                                    style: TextStyle(fontSize: 14.0)),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 180.0),
                                child: Text('01/10/2020',
                                    style: TextStyle(fontSize: 14.0)),
                              ),
                            ],
                          ),
                          Divider(
                            height: 20.0,
                            color: Colors.grey[350],
                            thickness: 1.5,
                            indent: 14.0,
                            endIndent: 5.0,
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15.0),
                                child: Text('Payment Mode',
                                    style: TextStyle(fontSize: 14.0)),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 200.0),
                                child: Text('Card',
                                    style: TextStyle(fontSize: 14.0)),
                              ),
                            ],
                          ),
                          Divider(
                            height: 20.0,
                            color: Colors.brown[100],
                            thickness: 2.0,
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15.0),
                                child: Text('Remaining Amount',
                                    style: TextStyle(fontSize: 14.0)),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 160.0),
                                child: Text('Rs 1350.',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14.0,
                                      color: Colors.green[800],
                                    )),
                              ),
                            ],
                          ),
                          Divider(
                            height: 20.0,
                            color: Colors.brown[100],
                            thickness: 2.0,
                            // indent: 14.0,
                            // endIndent: 5.0,
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15.0),
                                child: Text('Payment Date',
                                    style: TextStyle(fontSize: 14.0)),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 162.0),
                                child: Text('01/10/2020',
                                    style: TextStyle(fontSize: 14.0)),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
