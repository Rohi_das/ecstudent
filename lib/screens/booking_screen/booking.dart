import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import '../../screens/booking_screen/booking_details.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';
import '../../providers/booking_history/booking_history.dart';
import '../../providers/dummy_data.dart';

class Booking extends StatefulWidget {
  static const routeName = '/booking';

  @override
  _BookingState createState() => _BookingState();
}

class _BookingState extends State<Booking> {
  final List names = [
    'Nayan Swami',
    'Rutuja Dond',
    'Gouri Tele',
    'Sneha Pawar',
    'Prajakta Pawar'
  ];

  String _status;
  int _selectStudent;
  int _selectHistory;

  @override
  void initState() {
    super.initState();
    _onDEF(0, 'all');
  }

  _onDEF(int index, String status) {
    setState(() {
      _selectHistory = index;
      _status = operationList[index].status;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => BookingHistorys(),
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          height: MediaQuery.of(context).size.height,
          color: Theme.of(context).canvasColor,
          child: Stack(
            children: [
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).padding.top + 5.0),
                child: Stack(
                  children: [
                    AppBarTitle(false, 'Booking History'),
                    AppBarBackButton(false, false),
                  ],
                ),
              ),
              SideScreen(true),
              Container(
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 20,
                ),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Select Student',
                            style: TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold),
                          ),
                          IconButton(
                              icon: FaIcon(
                                FontAwesomeIcons.chevronCircleRight,
                                color: Theme.of(context).primaryColor,
                                size: 20.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                      Container(
                        width: double.infinity,
                        height: 40,
                        child: ListView.builder(
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    side: BorderSide(
                                      color: _selectStudent == index
                                          ? Colors.purple
                                          : Colors.grey[500],
                                    )),
                                color: Theme.of(context).canvasColor,
                                textColor: _selectStudent == index
                                    ? Colors.purple
                                    : Colors.grey[500],
                                padding: EdgeInsets.all(8.0),
                                onPressed: () {
                                  setState(() {
                                    _selectStudent = index;
                                  });
                                },
                                child: Text(
                                  names[index],
                                  style: TextStyle(
                                    fontSize: 14.0,
                                  ),
                                ),
                              ),
                            );
                          },
                          itemCount: names.length,
                          scrollDirection: Axis.horizontal,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Booking History For DEF',
                            style: TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold),
                          ),
                          IconButton(
                              icon: FaIcon(
                                FontAwesomeIcons.chevronCircleRight,
                                color: Theme.of(context).primaryColor,
                                size: 20.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                      Container(
                        width: double.infinity,
                        height: 40,
                        child: ListView.builder(
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    side: BorderSide(
                                      color: _selectHistory == index
                                          ? Colors.purple
                                          : Colors.grey[500],
                                    )),
                                color: Theme.of(context).canvasColor,
                                textColor: _selectHistory == index
                                    ? Colors.purple
                                    : Colors.grey[500],
                                padding: EdgeInsets.all(8.0),
                                onPressed: () {
                                  _onDEF(index, operationList[index].status);

                                  // Provider.of(context).sortHistory('rejected');
                                },
                                child: Text(
                                  operationList[index].title,
                                  style: TextStyle(
                                    fontSize: 14.0,
                                  ),
                                ),
                              ),
                            );
                          },
                          itemCount: operationList.length,
                          scrollDirection: Axis.horizontal,
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.62,
                        width: double.infinity,
                        padding: EdgeInsets.only(right: 5.0),
                        child: BookingDetails(_status),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
