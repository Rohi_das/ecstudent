import 'package:ec_student/models/booking_history/history.dart';
import 'package:ec_student/providers/booking_history/booking_history.dart';
import 'package:ec_student/screens/booking_screen/transaction_details_booked.dart';
import 'package:ec_student/screens/booking_screen/transaction_details_confirmed.dart';
import 'package:ec_student/screens/booking_screen/transaction_details_rejected.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BookingDetails extends StatefulWidget {
  String status;
  BookingDetails(this.status);

  @override
  _BookingDetailsState createState() => _BookingDetailsState();
}

class _BookingDetailsState extends State<BookingDetails> {
  // List _data =operationList;
  //   _filter(String status, int index) {
  //   _data = [];
  //   if (status == 'all') {
  //     setState(() {
  //       _data = operationList;
  //       _selectHistory = index;
  //     });
  //   } else {
  // setState(() {
  //   _data =
  //       operationList.where((element) => element.status == status).toList();
  //   _selectHistory = index;
  // });
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    final historyData = Provider.of<BookingHistorys>(context, listen: false);
    List data = historyData.items;

    if (widget.status == 'all') {
      data = [];
      setState(() {
        data = historyData.items;
      });
    } else {
      data = [];
      setState(() {
        data = historyData.items
            .where((element) => element.status == widget.status)
            .toList();
      });
    }
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          elevation: 2.0,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Container(
                      width: 80,
                      height: 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: NetworkImage(data[index].image),
                              fit: BoxFit.fill)),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      data[index].studentName,
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                // VerticalDivider(
                //   width: 10,
                //   thickness: 2,
                //   color: Colors.red[350],
                //   indent: 20.0,
                //   endIndent: 23.0,
                // ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Subject : ' + data[index].subject,
                      style: TextStyle(
                          fontSize: 12.0, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      'Class Type : ' + data[index].classType,
                      style: TextStyle(
                          fontSize: 12.0, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      'End Date: ' + data[index].endDate,
                      style: TextStyle(
                          fontSize: 12.0, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      'Days: ' + data[index].days,
                      style: TextStyle(
                          fontSize: 12.0, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      'Time: ' + data[index].time,
                      style: TextStyle(
                          fontSize: 12.0, fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
                Column(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Text(
                          data[index].status.toUpperCase(),
                          style: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                            color: data[index].status == 'rejected'
                                ? Colors.red
                                : Colors.green,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 45,
                    ),
                    SizedBox(
                      height: 25,
                      child: FlatButton(
                        onPressed: () {
                          if (data[index].status == 'rejected') {
                            Navigator.of(context).pushNamed(
                                TransactionDetailsRejected.routeName);
                          }
                          if (data[index].status == 'confirmed') {
                            Navigator.of(context).pushNamed(
                                TransactionDetailsConfimed.routeName);
                          }
                          if (data[index].status == 'ongoing') {
                            Navigator.of(context)
                                .pushNamed(TransactionDetailsBooked.routeName);
                          }
                        },
                        child: Text(
                          'VIEW DETAILS',
                          style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.blue[900]),
                        ),
                      ),
                    ),

                    // SizedBox(
                    //   height: 22,
                    // ),
                    // Padding(
                    //   padding: const EdgeInsets.only(top:32.0),
                    //   child: FlatButton(
                    //       onPressed: () {
                    //         // Navigator.push(
                    //         //   context,
                    //         //   MaterialPageRoute(
                    //         //       builder: (context) => ViewDetails()),
                    //         // );
                    //       },
                    //       child: Text(
                    //         'VIEW DETAILS',
                    //         style: TextStyle(
                    //             fontSize: 12.0,
                    //             fontWeight: FontWeight.bold,
                    //             color: Colors.blue[900]),
                    //       )),
                    // )
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
