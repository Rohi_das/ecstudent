import 'package:flutter/material.dart';
import '../../widgets/layouts/common_button.dart';

class DataNotFound extends StatelessWidget {
  static const routeName='/data-not-found';
  @override
  Widget build(BuildContext context) {
    _onTryAgain(){
Navigator.of(context).pop();
    }
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/search.jpeg',height:200 ,),
            Text(
              "Opp's",
              style: TextStyle(fontSize: 24.0, color: Colors.grey[700]),
            ),
            Text(
              "Data not found",
              style: TextStyle(fontSize: 14.0, color: Colors.grey[700]),
            ),
            CommonButton('Go Back', _onTryAgain,true),
          ],
        ),
      ),
    );
  }
}
