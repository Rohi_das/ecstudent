import 'package:ec_student/widgets/layouts/common_button.dart';
import 'package:flutter/material.dart';
class NoConnection extends StatelessWidget {
  static const routeName='/no-connection';
  @override
  Widget build(BuildContext context) {
    _onTryAgain(){

    }
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/no-connection.jpeg'),
            Text(
              "Please check your internet connection and try again",
              style: TextStyle(fontSize: 14.0, color: Colors.grey[700]),
            ),
            CommonButton('TRY AGAIN', _onTryAgain,true),
          ],
        ),
      ),
    );
  }
}