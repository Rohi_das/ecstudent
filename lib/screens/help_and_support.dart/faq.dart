import 'package:flutter/material.dart';

import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';
import '../../providers/dummy_data.dart';

class FAQ extends StatefulWidget {
  static const routeName='/faq';
  @override
  _FAQState createState() => _FAQState();
}

class _FAQState extends State<FAQ> {
    _onExpansion(int index) {
    for (var i = 0; i <= faq.length-1; i++) {
      if (i == index) {
        setState(() {
          faq[i].isExpanded = !faq[i].isExpanded;
        });
      } else {
        setState(() {
          faq[i].isExpanded = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Frequently Asked Questions'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Container(
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top,
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                left: 20,
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(right: 5.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            
                            Container(
                              padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                              child: Column(
                                children: faq.map((e) {
                                  return Padding(
                                    padding: const EdgeInsets.only(
                                        left: 12.0, right: 12.0),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              child: Expanded(
                                                child: Text(
                                                  e.question,
                                                  style: TextStyle(
                                                    fontSize: 14.0,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              child: IconButton(
                                                icon: e.isExpanded
                                                    ? Icon(
                                                        Icons.remove,
                                                        color: Colors
                                                            .deepOrange[300],
                                                        size: 20.0,
                                                      )
                                                    : Icon(
                                                        Icons.add,
                                                        color: Colors
                                                            .deepOrange[300],
                                                        size: 20.0,
                                                      ),
                                                onPressed: () {
                                                  var index = faq.indexOf(e);
                                                  _onExpansion(index);
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                        e.isExpanded
                                            ? Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 72.0, bottom: 5.0),
                                                child: Text(
                                                  e.answer,
                                                  style:
                                                      TextStyle(fontSize: 14.0),
                                                ),
                                              )
                                            : SizedBox(),
                                        Divider(
                                          height: 5,
                                          color: Colors.grey,
                                          thickness: 1.5,
                                          endIndent: 16.0,
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
  
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}