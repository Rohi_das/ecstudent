import 'package:ec_student/screens/help_and_support.dart/faq.dart';
import 'package:flutter/material.dart';
import '../../providers/dummy_data.dart';
import '../../models/help_support/help_support.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';

class HelpAndSupport extends StatefulWidget {
  static const routeName = '/help-and-support';
  @override
  _HelpAndSupportState createState() => _HelpAndSupportState();
}

class _HelpAndSupportState extends State<HelpAndSupport> {
  List<HelpSupport> _list = [];

  @override
  void initState() {
    super.initState();

    for (int i = 0; i <= 3; i++) {
      setState(() {
        _list.add(HelpSupport(
            hsId: faq[i].hsId,
            question: faq[i].question,
            answer: faq[i].answer,
            isExpanded: faq[i].isExpanded));
      });
    }
  }

  _onExpansion(int index) {
    for (var i = 0; i <= _list.length; i++) {
      if (i == index) {
        setState(() {
          _list[i].isExpanded = !_list[i].isExpanded;
        });
      } else {
        setState(() {
          _list[i].isExpanded = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Help & Support'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Container(
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top,
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                left: 20,
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(right: 5.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Transform(
                                transform:
                                    Matrix4.translationValues(-20, 0.0, 0.0),
                                child: Text(
                                  "FAQ",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              leading: Icon(
                                Icons.help_outline,
                                color: Colors.blue,
                                size: 25,
                              ),
                            ),
                            Divider(
                              height: 5,
                              color: Colors.grey,
                              thickness: 1.5,
                              indent: 12.0,
                              endIndent: 28.0,
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                              child: Column(
                                children: _list.map((e) {
                                  return Padding(
                                    padding: const EdgeInsets.only(
                                        left: 12.0, right: 12.0),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              child: Expanded(
                                                child: Text(
                                                  e.question,
                                                  style: TextStyle(
                                                    fontSize: 14.0,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              child: IconButton(
                                                icon: e.isExpanded
                                                    ? Icon(
                                                        Icons.remove,
                                                        color: Colors
                                                            .deepOrange[300],
                                                        size: 20.0,
                                                      )
                                                    : Icon(
                                                        Icons.add,
                                                        color: Colors
                                                            .deepOrange[300],
                                                        size: 20.0,
                                                      ),
                                                onPressed: () {
                                                  var index = _list.indexOf(e);
                                                  _onExpansion(index);
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                        e.isExpanded
                                            ? Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 72.0),
                                                child: Text(
                                                  e.answer,
                                                  style:
                                                      TextStyle(fontSize: 14.0),
                                                ),
                                              )
                                            : SizedBox(),
                                        Divider(
                                          height: 5,
                                          color: Colors.grey,
                                          thickness: 1.5,
                                          endIndent: 16.0,
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                            FlatButton(
                              padding:
                                  EdgeInsets.only(bottom: 25.0, left: 260.0),
                              onPressed: () {
                                Navigator.of(context).pushNamed(FAQ.routeName);
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(builder: (context) => FirstRoute()),
                                // );
                              },
                              child: Text(
                                "More...",
                                style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.deepOrange[300]),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 20, bottom: 5.0, right: 5.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: 80,
                                width: 120,
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  color: Colors.white,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 12.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text('Live Chat'),
                                        Text('9 AM - 6 AM'),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: 80,
                                width: 120,
                                child: Card(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    color: Colors.white,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text('Write to us'),
                                      ],
                                    )),
                              ),
                              Container(
                                height: 80,
                                width: 120,
                                padding: EdgeInsets.only(bottom: 5.0),
                                child: Card(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    color: Colors.white,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 12.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text('Customer'),
                                          Text('Support'),
                                        ],
                                      ),
                                    )),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            RawMaterialButton(
                              onPressed: () {},
                              elevation: 2.0,
                              fillColor: Theme.of(context).buttonColor,
                              child: Icon(
                                Icons.message,
                                size: 16.0,
                                color: Theme.of(context).accentColor,
                              ),
                              // padding: EdgeInsets.all(15.0),
                              shape: CircleBorder(),
                            ),
                            RawMaterialButton(
                              onPressed: () {},
                              elevation: 2.0,
                              fillColor: Theme.of(context).buttonColor,
                              child: Icon(
                                Icons.email,
                                size: 16.0,
                                color: Theme.of(context).accentColor,
                              ),
                              // padding: EdgeInsets.all(15.0),
                              shape: CircleBorder(),
                            ),
                            RawMaterialButton(
                              onPressed: () {},
                              elevation: 2.0,
                              fillColor: Theme.of(context).buttonColor,
                              child: Icon(
                                Icons.headset_mic,
                                size: 16.0,
                                color: Theme.of(context).accentColor,
                              ),
                              // padding: EdgeInsets.all(15.0),
                              shape: CircleBorder(),
                            )
                          ],
                        ),
                      ],
                    ),
                    Center(
                      child: Container(
                        height: 150,
                        width: 500,
                        padding: EdgeInsets.only(
                            left: 5.0, right: 5.0, top: 10.0, bottom: 5.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          color: Colors.white,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              ListTile(
                                leading: Icon(
                                  Icons.location_on,
                                  size: 25,
                                  color: Colors.blue,
                                ),
                                title: Transform(
                                  transform:
                                      Matrix4.translationValues(-20, 0.0, 0.0),
                                  child: Text(
                                    "Address",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              Divider(
                                height: 10,
                                color: Colors.grey,
                                thickness: 1.5,
                                indent: 20.0,
                                endIndent: 20.0,
                              ),
                              Center(
                                child: Container(
                                  padding:
                                      EdgeInsets.only(top: 10.0, left: 18.0),
                                  child: Text(
                                    "106 Raheja Arcade, Sec 11 CBD Belapur,Navi Mumbai-400614,Maharashtra,INDIA",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
