import 'package:carousel_slider/carousel_slider.dart';
import 'package:ec_student/providers/dummy_data.dart';
import 'package:ec_student/screens/payments_screen/payments.dart';
import 'package:ec_student/screens/rewards_offers_screens/rewards_and_offers.dart';
import 'package:ec_student/screens/search_screen/search_categories.dart';
import 'package:ec_student/widgets/calendar/calendar.dart';
import 'package:ec_student/widgets/layouts/app_bar_back_button.dart';
import 'package:ec_student/widgets/layouts/app_bar_title.dart';
import 'package:ec_student/widgets/layouts/side_screen.dart';
import 'package:ec_student/widgets/student_dashboard/booked_teachers.dart';
import 'package:ec_student/widgets/student_dashboard/upcoming.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Student Dashboard'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 60,
                  left: 15,
                ),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        child: CarouselSlider(
                          options: CarouselOptions(
                            height: 120.0,
                            initialPage: 0,
                            // scrollDirection: Axis.horizontal,
                            // autoPlay: true,
                            // autoPlayInterval: Duration(seconds: 3),
                          ),
                          items: referEarn.map((refer) {
                            return Builder(
                              builder: (BuildContext context) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                                  child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    child: Image.asset(
                                      refer.reImage,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                );
                              },
                            );
                          }).toList(),
                        ),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      Container(
                        height: 320,
                        child: Column(
                          // mainAxisAlignment: MainAxisAlignment.spaceAround,0xFFAD5CA9
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                MaterialButton(
                                  height: 60,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(SearchCategories.routeName);
                                  },
                                  color: Colors.white,
                                  textColor: Color(0xFFAD5CA9),
                                  child: Icon(
                                    Icons.search,
                                    size: 30,
                                  ),
                                  // padding: EdgeInsets.all(16),
                                  shape: CircleBorder(),
                                  elevation:8,

                                ),
                                MaterialButton(
                                  height: 60,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(BookedTeacher.routeName,arguments: 'current');
                                  },
                                  color: Colors.white,
                                  textColor: Color(0xFFAD5CA9),
                                  child: Icon(
                                    Icons.supervisor_account,
                                    size: 30,
                                  ),
                                  padding: EdgeInsets.all(16),
                                  shape: CircleBorder(),
                                  elevation:8,
                                ),
                                MaterialButton(
                                  height: 60,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(Calendar.routeName);
                                  },
                                  color: Colors.white,
                                  textColor: Color(0xFFAD5CA9),
                                  child: Icon(
                                    Icons.calendar_today,
                                    size: 30,
                                  ),
                                  padding: EdgeInsets.all(16),
                                  shape: CircleBorder(),
                                  elevation:8,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 20,
                                right: 30,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    'Search & Book \n New Teacher',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width: 25,
                                  ),
                                  Text(
                                    'Current \n Teacher',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width: 35,
                                  ),
                                  Text(
                                    'My \n Time Table',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  // Column(
                                  //   children: [
                                  //     Text(
                                  //       'Current',
                                  //       style: TextStyle(
                                  //           fontSize: 12.0,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //     Text(
                                  //       'Teacher',
                                  //       style: TextStyle(
                                  //           fontSize: 12.0,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //   ],
                                  // ),
                                  // Column(
                                  //   children: [
                                  //     Text(
                                  //       'My',
                                  //       style: TextStyle(
                                  //           fontSize: 12.0,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //     Text(
                                  //       'Time Table',
                                  //       style: TextStyle(
                                  //           fontSize: 12.0,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //   ],
                                  // ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                MaterialButton(
                                  height: 60,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(Payments.routeName);
                                  },
                                  color: Colors.white,
                                  textColor: Color(0xFFAD5CA9),
                                  child: Icon(
                                    Icons.account_balance_wallet,
                                    size: 30,
                                  ),
                                  padding: EdgeInsets.all(16),
                                  shape: CircleBorder(),
                                  elevation:8,
                                ),
                                MaterialButton(
                                  height: 60,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(BookedTeacher.routeName,arguments: 'past');
                                  },
                                  color: Colors.white,
                                  textColor: Color(0xFFAD5CA9),
                                  child: Icon(
                                    Icons.supervisor_account,
                                    size: 30,
                                  ),
                                  padding: EdgeInsets.all(16),
                                  shape: CircleBorder(),
                                  elevation:8,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Text(
                                    'Payments',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                // SizedBox(width: 30,),
                                Padding(
                                  padding: const EdgeInsets.only(left: 20),
                                  child: Text(
                                    'Past Teachers',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                MaterialButton(
                                  height: 60,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(Upcoming.routeName);
                                  },
                                  color: Colors.white,
                                  textColor: Color(0xFFAD5CA9),
                                  child: FaIcon(
                                    FontAwesomeIcons.bookOpen,
                                    size: 25,
                                  ),
                                  padding: EdgeInsets.all(16),
                                  shape: CircleBorder(),
                                  elevation:8,
                                ),
                                MaterialButton(
                                  height: 60,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(Upcoming.routeName);
                                  },
                                  color: Colors.white,
                                  textColor: Color(0xFFAD5CA9),
                                  child: FaIcon(
                                    FontAwesomeIcons.graduationCap,
                                    size: 25,
                                  ),
                                  padding: EdgeInsets.all(16),
                                  shape: CircleBorder(),
                                  elevation:8,
                                ),
                                MaterialButton(
                                  clipBehavior: Clip.antiAlias,
                                  height: 60,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(RewardsAndOffers.routeName);
                                  },
                                  color: Colors.white,
                                  textColor: Color(0xFFAD5CA9),
                                  child: FaIcon(
                                    FontAwesomeIcons.tags,
                                    size: 25,
                                  ),
                                  padding: EdgeInsets.all(16),
                                  shape: CircleBorder(),
                                  elevation:8,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 40,
                                right: 55,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Connect to \n My School',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Text(
                                    'Connect to \n My College',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width: 40,
                                  ),
                                  Text(
                                    'Offers',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  // Column(
                                  //   children: [
                                  //     Text(
                                  //       'Connect to My School',
                                  //       style: TextStyle(
                                  //           fontSize: 12.0,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //     Text(
                                  //       'My School',
                                  //       style: TextStyle(
                                  //           fontSize: 12.0,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //   ],
                                  // ),
                                  // Column(
                                  //   children: [
                                  //     Text(
                                  //       'Connect to My College',
                                  //       style: TextStyle(
                                  //           fontSize: 12.0,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //     Text(
                                  //       'My College',
                                  //       style: TextStyle(
                                  //           fontSize: 12.0,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //   ],
                                  // ),
                                  // Text(
                                  //   'Offers',
                                  //   style: TextStyle(
                                  //       fontSize: 12.0,
                                  //       fontWeight: FontWeight.bold),
                                  // ),
                                ],
                              ),
                            ),

                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.center,
                            //   children: [
                            //     IconButton(
                            //         icon: FaIcon(
                            //           FontAwesomeIcons.chevronCircleLeft,
                            //           color: Colors.blue,
                            //           size: 20.0,
                            //         ),
                            //         onPressed: () {}),
                            //     IconButton(
                            //         icon: FaIcon(
                            //           FontAwesomeIcons.chevronCircleRight,
                            //           color: Colors.blue,
                            //           size: 20.0,
                            //         ),
                            //         onPressed: () {}),
                            //   ],
                            // )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
