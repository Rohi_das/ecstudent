import 'package:ec_student/widgets/student_dashboard/cart.dart';
import 'package:flutter/material.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/curve_bottom.dart';
class CartScreen extends StatefulWidget {
  static const routeName='/cart';
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    return Cart();
  }
}

// import 'dart:async';
// import 'dart:convert';

// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;

// Future<Album> createAlbum(String title) async {
//   final http.Response response = await http.post(
//     'https://jsonplaceholder.typicode.com/albums',
//     headers: <String, String>{
//       'Content-Type': 'application/json; charset=UTF-8',
//     },
//     body: jsonEncode(<String, String>{
//       'title': title,
//     }),
//   );
// print(response.statusCode);
//   if (response.statusCode == 201) {
//     return Album.fromJson(jsonDecode(response.body));
//   } else {
//     throw Exception('Failed to create album.');
//   }
// }

// class Album {
//   final int id;
//   final String title;

//   Album({this.id, this.title});

//   factory Album.fromJson(Map<String, dynamic> json) {
//     return Album(
//       id: json['id'],
//       title: json['title'],
//     );
//   }
// }



// class CartScreen extends StatefulWidget {
//   static const routeName='/cart';
//   CartScreen({Key key}) : super(key: key);

//   @override
//   _CartScreenState createState() {
//     return _CartScreenState();
//   }
// }

// class _CartScreenState extends State<CartScreen> {
//   final TextEditingController _controller = TextEditingController();
//   Future<Album> _futureAlbum;

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Create Data Example',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text('Create Data Example'),
//         ),
//         body: Container(
//           alignment: Alignment.center,
//           padding: const EdgeInsets.all(8.0),
//           child: (_futureAlbum == null)
//               ? Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     TextField(
//                       controller: _controller,
//                       decoration: InputDecoration(hintText: 'Enter Title'),
//                     ),
//                     RaisedButton(
//                       child: Text('Create Data'),
//                       onPressed: () {
//                         setState(() {
//                           _futureAlbum = createAlbum(_controller.text);
//                         });
//                       },
//                     ),
//                   ],
//                 )
//               : FutureBuilder<Album>(
//                   future: _futureAlbum,
//                   builder: (context, snapshot) {
//                     if (snapshot.hasData) {
//                       return Text(snapshot.data.title);
//                     } else if (snapshot.hasError) {
//                       return Text("${snapshot.error}");
//                     }

//                     return CircularProgressIndicator();
//                   },
//                 ),
//         ),
//       ),
//     );
//   }
// }
