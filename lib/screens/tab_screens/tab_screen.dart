import 'package:ec_student/screens/tab_screens/dashboard.dart';
import 'package:ec_student/widgets/drawer/student_drawer.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

import 'calender_screen.dart';
import 'cart_screen.dart';
import 'home_screen.dart';
import 'profile_screen.dart';

class TabScreen extends StatefulWidget {
  static const routeName='/home';
  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen> {
  final List<Map<String, Object>> _pages = [
    {'page': HomeScreen()},
    {'page':Dashboard()},
    {'page': CalenderScreeen()},
    {'page': CartScreen()},
    {'page': ProfileScreen()},
  ];

  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    
    return Stack(
      children: [

                 Column(
        children: [
      Container(
        // padding: EdgeInsets.only(top: 40.0,),
       height: MediaQuery.of(context).size.height,
        color:Colors.transparent,
        child: Scaffold(


          drawer: StudentDrawer(),
          body: _pages[_selectedIndex]['page'],
          bottomNavigationBar: CurvedNavigationBar(
            
              height: 55.0,
              color: Theme.of(context).primaryColor,
              buttonBackgroundColor: Color(0xFFAD5CA9),
              backgroundColor: Colors.transparent,
              animationDuration: Duration(milliseconds: 200),
              items: <Widget>[
                
                Icon(
                  Icons.home,
                  size: 25,
                  color: Colors.white,
                ),
                Icon(
                  Icons.dashboard,
                  size: 25,
                  color: Colors.white,
                ),
                Icon(
                  Icons.calendar_today,
                  size: 25,
                  color: Colors.white,
                ),
                Icon(
                  Icons.shopping_cart,
                  size: 25,
                  color: Colors.white,
                ),
                Icon(
                  Icons.person,
                  size: 25,
                  color: Colors.white,
                )
              ],
              onTap: (index) {
                setState(() {
                  _selectedIndex=index;
                });
              }),
              
        ),
      ),
        ],
      ),
      // Positioned(bottom: 50, child: Text('data'))
      ],
    );
  }
}
