import 'package:ec_student/screens/login_screens/login.dart';
import 'package:ec_student/widgets/calendar/calendar.dart';
import 'package:flutter/material.dart';
import '../../widgets/layouts/side_screen.dart';
import '../../widgets/layouts/curve_top.dart';
class CalenderScreeen extends StatefulWidget {
  static const routeName='/calender';
  @override
  _CalenderScreeenState createState() => _CalenderScreeenState();
}

class _CalenderScreeenState extends State<CalenderScreeen> {
  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.width);
    return Container(
      padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top),
      child: Calendar());
  }
}