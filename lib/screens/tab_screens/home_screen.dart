import 'package:ec_student/screens/others_screen/onboarding_screen.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

import '../../screens/profile_screen/student_profile.dart';
import '../../widgets/layouts/common_button.dart';
import '../../widgets/layouts/app_bar_back_button.dart';
import '../../providers/dummy_data.dart';
import '../../widgets/layouts/side_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  _schoolConnect() {}

  _collegeConnect() {}

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).padding.top,
      ),
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top * 0.9,
            ),
            color: Theme.of(context).accentColor,
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 13.0,right: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/educhamp.png',
                        width: 180, alignment: Alignment.center,
                        // height: 100,
                      ),
                    ],
                  ),
                ),
                AppBarBackButton(false, true),
                Positioned(
                  right: 50,
                  child: MaterialButton(
                    height: 35,
                    minWidth: 35,
                    onPressed: () {},
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    child: Icon(
                      Icons.chat_bubble_outline,
                      size: 18,
                    ),
                    padding: EdgeInsets.all(6),
                    shape: CircleBorder(),
                  ),
                ),
                Positioned(
                  right: 5,
                  child: MaterialButton(
                    height: 35,
                    minWidth: 35,
                    onPressed: () {
                      Navigator.of(context).pushNamed(OnbordingScreen.routeName);
                    },
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    child: Icon(
                      Icons.notifications_none,
                      size: 20,
                    ),
                    padding: EdgeInsets.all(6),
                    shape: CircleBorder(),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + 50,
              // right: 10,
              left: 15,
            ),
            height: MediaQuery.of(context).size.height * 0.8,
            child: SingleChildScrollView(
              // scrollDirection:Axis.vertical,
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 6.0, right: 0, top: 4, bottom: 4),
                    child: Text(
                      'Search Categories',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.of(context).pushNamed(StudentProfile.routeName);
                    },
                                      child: Container(
                      height: 100,
                      width: double.infinity,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          // padding: const EdgeInsets.only(left: 4),
                          itemCount: categories.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 2,
                                      child: Container(
                                        width: 70,
                                        height: 70,
                                        child: 
                                        // Image.asset('name')
                                        Icon(
                                          Icons.access_time,
                                          size: 40,
                                          color: Color(0xFFE376BD),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  width: 60,
                                  child: Text(categories[index].categoryName,
                                      style: TextStyle(
                                        fontSize: 8,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      textAlign: TextAlign.center),
                                ),
                              ],
                            );
                          }),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 6.0, right: 0, top: 4, bottom: 4),
                    child: Text(
                      'Addition Categories',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    height: 150,
                    width: double.infinity,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: additionalCategories.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Card(
                              clipBehavior: Clip.antiAlias,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              elevation: 2,
                              child: Container(
                                width: 100,
                                height: 130,
                                child: Column(
                                  children: [
                                    Image.asset(
                                        additionalCategories[index].image),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: Text(
                                        additionalCategories[index].adName,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        CommonButton('CONNECT SCHOOL', _schoolConnect, true),
                        CommonButton('CONNECT COLLEGE', _collegeConnect, true),
                      ],
                    ),
                  ),
                  Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 6.0,
                          right: 0,
                          top: 4,
                        ),
                        child: Text(
                          'Explore',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        height: 280,
                        child: GridView.count(
                          primary: false,
                          padding: const EdgeInsets.only(
                              top: 30, right: 10, left: 6),
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10,
                          crossAxisCount: 3,
                          children: explore.map((e) {
                            return Container(
                              // padding: const EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Color(0xFFE376BD),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                              ),
                              child: Column(
                                children: [
                                  Image.asset(
                                    e.exploreImage,
                                    width: 90,
                                    height: 90,
                                  ),
                                  Text(
                                    e.exploreName,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10),
                                    textAlign: TextAlign.center,
                                  )
                                ],
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 6.0, right: 0, top: 4, bottom: 10),
                    child: Text(
                      'News Flash',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    child: CarouselSlider(
                      options: CarouselOptions(
                        height: 120.0,
                        scrollDirection: Axis.horizontal,
                        autoPlay: true,
                        autoPlayInterval: Duration(seconds: 3),
                      ),
                      items: newsFlash.map((news) {
                        return Builder(
                          builder: (BuildContext context) {
                            return Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                child: Image.asset(
                                  news.newsImage,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            );
                          },
                        );
                      }).toList(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 6.0,
                      bottom: 6.0,
                      top: 15,
                    ),
                    child: Text(
                      'Help Yourself',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    height: 180,
                    width: double.infinity,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: helpYourSelf.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Card(
                              clipBehavior: Clip.antiAlias,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              elevation: 2,
                              child: Container(
                                width: 150,
                                height: 180,
                                child: Column(
                                  children: [
                                    Image.asset(
                                      helpYourSelf[index].hyImage,
                                      fit: BoxFit.fitHeight,
                                      height: 140,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: Text(
                                        helpYourSelf[index].hyName,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 6.0, right: 0, top: 10, bottom: 10),
                    child: Text(
                      'Refer & Earn',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    child: CarouselSlider(
                      options: CarouselOptions(
                        height: 120.0,
                        initialPage: 0,
                        // scrollDirection: Axis.horizontal,
                        // autoPlay: true,
                        // autoPlayInterval: Duration(seconds: 3),
                      ),
                      items: referEarn.map((refer) {
                        return Builder(
                          builder: (BuildContext context) {
                            return Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                child: Image.asset(
                                  refer.reImage,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            );
                          },
                        );
                      }).toList(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 6.0, right: 0, top: 10, bottom: 10),
                    child: const Text(
                      'Enhance Your Knowledge',
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 114,
                    margin: const EdgeInsets.symmetric(horizontal: 5.0),
                    child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          // padding: const EdgeInsets.only(left: 4),
                          itemCount: enhanceKnowledge.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only( right:8.0),
                              child: Column(
                          children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 3.5,
                                height: 100,
                                padding: const EdgeInsets.only(bottom: 10),
                                child: ClipRRect(
                                  borderRadius:
                                      const BorderRadius.all(Radius.circular(10)),
                                  child: Image.asset(
                                    enhanceKnowledge[index].image,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Text(
                                enhanceKnowledge[index].title,
                                style: const TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                          ],
                        ),
                            );
                          })

                      
                      
                      //  enhanceKnowledge.map((e) {
                      //   return Column(
                      //     children: [
                      //       Container(
                      //         width: MediaQuery.of(context).size.width / 3.5,
                      //         height: 100,
                      //         padding: const EdgeInsets.only(bottom: 10),
                      //         child: ClipRRect(
                      //           borderRadius:
                      //               const BorderRadius.all(Radius.circular(10)),
                      //           child: Image.asset(
                      //             e.image,
                      //             fit: BoxFit.cover,
                      //           ),
                      //         ),
                      //       ),
                      //       Text(
                      //         e.title,
                      //         style: const TextStyle(
                      //             fontSize: 12, fontWeight: FontWeight.bold),
                      //         textAlign: TextAlign.center,
                      //       ),
                      //     ],
                      //   );
                      // }).toList(),
                    
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 6.0,
                      bottom: 6.0,
                      top: 15,
                    ),
                    child: Text(
                      'Testimonial',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                     height: 290,
                    width: double.infinity,
                    
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: testimonial.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Stack(
                          children: [
                            Container(
                              
                              margin: EdgeInsets.only(top: 45, left: 4, right: 4),
                              height: 200,
                              width: MediaQuery.of(context).size.width / 2.65,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    blurRadius: 2.0,
                                    spreadRadius: 0.0,
                                    offset: Offset(
                                        2.0, 2.0), // shadow direction: bottom right
                                  )
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 40.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(testimonial[index].name),
                                    Text(testimonial[index].rating),
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              top: 8,
                              left: 42,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(35.0),
                                child: Image.asset(
                                  testimonial[index].image,
                                  height: 70.0,
                                  width: 70.0,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Container(
                              height: 200,
                              margin: EdgeInsets.only(top: 90),
                              width: MediaQuery.of(context).size.width / 2.5,
                              color: Colors.transparent,
                              child: CustomPaint(
                                painter: CurvePainter(context),
                              ),
                            ),
                            Positioned(
                              top: 230,
                              left: 20,
                              child: Text(
                                testimonial[index].info,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontStyle: FontStyle.italic,
                                    color: Theme.of(context).accentColor),
                              ),
                            ),
                          ],
                      ),
                        );
                      },
                    ),
             
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 6.0,
                      bottom: 6.0,
                      top: 15,
                    ),
                    child: Text(
                      'Videos',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    height: 110,
                    width: double.infinity,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: videos.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Card(
                              clipBehavior: Clip.antiAlias,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              elevation: 2,
                              child: Container(
                                width: 150,
                                height: 110,
                                child: Column(
                                  children: [
                                    Image.asset(
                                      videos[index].videoDetails,
                                      fit: BoxFit.fitHeight,
                                      height: 100,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 6.0,
                      bottom: 6.0,
                      top: 15,
                    ),
                    child: Text(
                      'Best Offers',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    height: 110,
                    width: double.infinity,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: bestOffers.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Card(
                              clipBehavior: Clip.antiAlias,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              elevation: 2,
                              child: Container(
                                width: 150,
                                height: 110,
                                child: Column(
                                  children: [
                                    Image.asset(
                                      bestOffers[index].image,
                                      fit: BoxFit.fitHeight,
                                      height: 100,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          SideScreen(true),
        ],
      ),
    );
  }
}

class CurvePainter extends CustomPainter {
  final context;
  CurvePainter(this.context);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Theme.of(context).primaryColor;
    paint.style = PaintingStyle.fill;

    var path = Path();

    path.moveTo(0, size.height * 0);
    path.quadraticBezierTo(size.width * 0.125, size.height * 0.01,
        size.width * 0.20, size.height * 0.125);

    path.quadraticBezierTo(size.width * 0.27, size.height * 0.25,
        size.width * 0.29, size.height * 0.3);
    path.quadraticBezierTo(size.width * 0.34, size.height * 0.40,
        size.width * 0.42, size.height * 0.47);
    path.quadraticBezierTo(size.width * 0.50, size.height * 0.55,
        size.width * 0.70, size.height * 0.60);
    path.quadraticBezierTo(size.width * 0.94, size.height * 0.67,
        size.width * 0.96, size.height * 0.70);
    path.quadraticBezierTo(size.width * 0.98, size.height * 0.72,
        size.width * 0.99, size.height * 0.74);
    path.quadraticBezierTo(size.width * 0.99, size.height * 0.76,
        size.width * 1.0, size.height * 0.78);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
