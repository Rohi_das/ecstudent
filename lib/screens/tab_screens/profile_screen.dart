import 'package:ec_student/screens/profile_screen/update_profile.dart';

import '../../screens/profile_screen/student_profile.dart';
import 'package:flutter/material.dart';


class ProfileScreen extends StatefulWidget {
  static const routeName = '/profile';
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return UpdateProfile();
  }
}
