import 'package:ec_student/helper/location_helpert.dart';
import 'package:ec_student/providers/dummy_data.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';

import '../../widgets/layouts/app_bar_back_button.dart';
import '../../widgets/layouts/app_bar_title.dart';
import '../../widgets/layouts/side_screen.dart';

class SearchScreen extends StatefulWidget {
  static const routeName = '/search';
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  String _previewImageUrl;

  int _selectSearchType = 0;
  List _search = searchResult;
  _filter(String type, int index) {
    _search = [];
    if (type == 'all') {
      setState(() {
        _search = searchResult;
        _selectSearchType = index;
      });
    } else {
      setState(() {
        _search = searchResult
            .where((element) => element.paymentType == type)
            .toList();
        _selectSearchType = index;
      });
    }
  }

  // Future<void> _getCurrentLocation(BuildContext context) async{
  // final width=int.parse((MediaQuery.of(context).size.width).toStringAsFixed(0));
  // final hieght=int.parse((MediaQuery.of(context).size.height*0.80).toStringAsFixed(0));
  Future<void> _getCurrentLocation() async {
    final width = 400;
    final height = 2400;
    final locData = await Location().getLocation();
    final staticMapUrl = LocationHelper.generateLocationPreviewImage(
        latitude: locData.latitude,
        longitude: locData.longitude,
        width: width.toString(),
        height: height.toString());
    print(locData.latitude.toString() + ' ' + locData.longitude.toString());
    setState(() {
      _previewImageUrl = staticMapUrl;
    });
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      _getCurrentLocation();
    });

    _onOpen();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    //          showModalBottomSheet(
    //   context: context,
    //   isScrollControlled: true, // set this to true
    //   builder: (_) {
    //     return DraggableScrollableSheet(
    //       expand: false,
    //       builder: (_, controller) {
    //         return Container(
    //           color: Colors.blue[500],
    //           child: Column(
    //   children: [
    //     Text('data'),
    //     Text('data'),
    //     Text('data'),
    //   ],
    // ),
    //         );
    //       },
    //     );
    //   },
    // );
  }

  _onOpen() {
    Future.delayed(Duration(seconds: 0)).then((_) {
      showModalBottomSheet(
        context: context,
        isScrollControlled: false, // set this to true
        // backgroundColor: Colors.yellow,
        builder: (context) {
          return DraggableScrollableSheet(
            initialChildSize: 0.2,
            minChildSize: 0.2,
            maxChildSize: 0.6,
            builder: (context, scrollCtrl){
           
return SingleChildScrollView(
  controller: scrollCtrl,
  dragStartBehavior:DragStartBehavior.start ,
  child:
  
   Column(
    children: [
      Text('data'),
      Text('data'),
      Text('data'),
    ],
  ),
);
});
          
          //  DraggableScrollableSheet(
          //   expand: false,
          //   builder: (_, controller) {
          //     return Container(
          //       color: Colors.blue[500],
          //       child: Column(
          //         children: [
          //           Text('data'),
          //           Text('data'),
          //           Text('data'),
          //         ],
          //       ),
          //     );
          //   },
          // );
        },
      );
      
    });
  }

  @override
  Widget build(BuildContext context) {
    _onShowModal() {
      DraggableScrollableSheet(builder: (context, scrollCtrl) {
        return SingleChildScrollView(
          controller: scrollCtrl,
          child: Column(
            children: [
              Text('data'),
              Text('data'),
              Text('data'),
            ],
          ),
        );
      });
    }

    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: SingleChildScrollView(
          child: Stack(
            fit: StackFit.loose,
            // alignment: AlignmentDirectional.center,
            children: [
              
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top,
                // alignment: Alignment.center,
                child: _previewImageUrl == null
                    ?Container(
                      width: 20,
                      height: 20,
                      child: Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Theme.of(context).primaryColor,
                          ),
                        ),
                    )
                    : Image.network(_previewImageUrl, fit: BoxFit.fitHeight),
              ),
              Positioned(
                top: 20,
                child: Padding(
                  padding: const EdgeInsets.only(left:10.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 30,
                    child: ListView.builder(
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                                side: BorderSide(
                                  color: _selectSearchType == index
                                      ? Colors.purple
                                      : Colors.grey[500],
                                )),
                            color: Theme.of(context).canvasColor,
                            textColor: _selectSearchType == index
                                ? Colors.purple
                                : Colors.grey[500],
                            padding: EdgeInsets.all(8.0),
                            onPressed: () {
                              _onOpen();
                              _filter(searchType[index].type, index);
                            },
                            child: Text(
                              searchType[index].title,
                              style: TextStyle(
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: searchType.length,
                      scrollDirection: Axis.horizontal,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),

      // Container(
      //   padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      //   height: MediaQuery.of(context).size.height,
      //   color: Theme.of(context).canvasColor,
      //   child: Stack(
      //     children: [
      //       Container(
      //         padding: EdgeInsets.only(
      //             top: MediaQuery.of(context).padding.top + 5.0),
      //         child: Stack(
      //           children: [
      //             AppBarTitle(false, 'Search Result'),
      //             AppBarBackButton(false, false),
      //           ],
      //         ),
      //       ),
      //       SideScreen(true),
      //       Container(
      //         height: MediaQuery.of(context).size.height -
      //             MediaQuery.of(context).padding.top,
      //         margin: EdgeInsets.only(
      //           top: MediaQuery.of(context).padding.top + 60,
      //           left: 20,

      //         ),
      //         // width: 100,
      //         child: SingleChildScrollView(
      //           scrollDirection: Axis.vertical,
      //           child: Column(
      //             children: [
      //               // RaisedButton(child:Text('Get Location', textAlign: TextAlign.center,),onPressed: (){
      //               //   _getCurrentLocation(context);
      //               // }),
      //               SingleChildScrollView(
      //                 child: Column(
      //                   children: [
      //                     Container(
      //                 width: double.infinity,
      //                 height: 30,
      //                 child: ListView.builder(
      //                   itemBuilder: (BuildContext context, int index) {
      //                     return Padding(
      //                       padding: const EdgeInsets.only(right: 10.0),
      //                       child: FlatButton(
      //                         shape: RoundedRectangleBorder(
      //                             borderRadius: BorderRadius.circular(15.0),
      //                             side: BorderSide(
      //                               color: _selectSearchType == index
      //                                   ? Colors.purple
      //                                   : Colors.grey[500],
      //                             )),
      //                         color: Theme.of(context).canvasColor,
      //                         textColor: _selectSearchType == index
      //                             ? Colors.purple
      //                             : Colors.grey[500],
      //                         padding: EdgeInsets.all(8.0),
      //                         onPressed: () {
      //                           _filter(searchType[index].type, index);
      //                         },
      //                         child: Text(
      //                           searchType[index].title,
      //                           style: TextStyle(
      //                             fontSize: 14.0,
      //                           ),
      //                         ),
      //                       ),
      //                     );
      //                   },
      //                   itemCount: searchType.length,
      //                   scrollDirection: Axis.horizontal,
      //                 ),
      //               ),
      //                     Container(
      //                 width: double.infinity,
      //                 height: MediaQuery.of(context).size.height * 0.60,
      //                 alignment: Alignment.center,
      //                 child:_previewImageUrl == null
      //                     ? CircularProgressIndicator(
      //                       backgroundColor: Theme.of(context).primaryColor,
      //                     )
      //                     : Image.network(_previewImageUrl, fit: BoxFit.fill),
      //                     )
      //                   ],
      //                 ),
      //               ),
      //             ],
      //           ),
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
    );
  }
}
