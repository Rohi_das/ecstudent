
import 'package:ec_student/providers/dummy_data.dart';
import 'package:ec_student/screens/search_screen/select_category.dart';
import 'package:ec_student/widgets/layouts/app_bar_back_button.dart';
import 'package:ec_student/widgets/layouts/app_bar_title.dart';
import 'package:ec_student/widgets/layouts/side_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SearchCategories extends StatefulWidget {
  static const routeName = '/search-categories';

  @override
  _SearchCategoriesState createState() => _SearchCategoriesState();
}

class _SearchCategoriesState extends State<SearchCategories> {
  int _selectedIndex;

  @override
  Widget build(BuildContext context) {
    onSelect(int index, String title) {
      setState(() {
        _selectedIndex = index;
      });
      Navigator.of(context).pushNamed(SelectCategory.routeName, arguments:title );
    }

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, 'Select Category'),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top,
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 50,
                left: 15,
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 10.0),
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      height: MediaQuery.of(context).size.height*0.72,
                      child: ListView.builder(
                        itemCount: searchCategories.length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  onSelect(index, searchCategories[index].title);
                                },
                                child: Container(
                                  // margin: EdgeInsets.all(15.0),
                                  // padding: EdgeInsets.only(left: 10.0, right: 10.0),
                                  height:45,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(18),
                                      border: Border.all(
                                          width: 1.5,
                                          color: _selectedIndex == index
                                              ? Color(0xFFAD5CA9)
                                              : Colors.grey),
                                      color: Colors.white),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          SizedBox(
                                            width: 10,
                                          ),
                                          FaIcon(
                                            searchCategories[index].icon,
                                            size: 15,
                                            color: _selectedIndex == index
                                                ? Color(0xFFAD5CA9)
                                                : Colors.grey[600],
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            searchCategories[index].title,
                                            style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.w600,
                                              color: _selectedIndex == index
                                                  ? Color(0xFFAD5CA9)
                                                  : Colors.grey[600],
                                            ),
                                          ),
                                        ],
                                      ),
                                      IconButton(
                                          icon: FaIcon(
                                            FontAwesomeIcons.chevronCircleRight,
                                            color: _selectedIndex == index
                                                ? Theme.of(context).primaryColor
                                                : Colors.grey[600],
                                            size: 22.0,
                                          ),
                                          onPressed: () {
                                            onSelect(index,searchCategories[index].title);
                                          }),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Select a category to search teachers.',
                      style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey[600],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
