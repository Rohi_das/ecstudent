import 'package:ec_student/screens/search_screen/on_map_search_screen.dart';
import 'package:ec_student/widgets/layouts/app_bar_back_button.dart';
import 'package:ec_student/widgets/layouts/app_bar_title.dart';
import 'package:ec_student/widgets/layouts/common_button.dart';
import 'package:ec_student/widgets/layouts/side_screen.dart';
import 'package:flutter/material.dart';

enum Tution { school, college }

class SelectCategory extends StatefulWidget {
  static const routeName = '/select-category';
  @override
  _SelectCategoryState createState() => _SelectCategoryState();
}

class _SelectCategoryState extends State<SelectCategory> {
  Tution _tutionType = Tution.school;
  String dropdownValue = 'One';
  bool _isFDSelected=false;
  @override
  Widget build(BuildContext context) {
    final routeData = ModalRoute.of(context).settings.arguments as String;
    onSearch(){
      Navigator.of(context).pushNamed(SearchScreen.routeName);
    }
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: MediaQuery.of(context).size.height,
        color: Theme.of(context).canvasColor,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + 5.0),
              child: Stack(
                children: [
                  AppBarTitle(false, routeData),
                  AppBarBackButton(false, false),
                ],
              ),
            ),
            SideScreen(true),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top,
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 55,
                left: 15,
              ),
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: [
                      routeData == 'Tuition'
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              // crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Radio(
                                  activeColor: Theme.of(context).primaryColor,
                                  value: Tution.school,
                                  groupValue: _tutionType,
                                  onChanged: (Tution value) {
                                    setState(() {
                                      _tutionType = value;
                                    });
                                  },
                                ),
                                const Text(
                                  'School',
                                  style: TextStyle(fontSize: 14),
                                ),
                                Radio(
                                  activeColor: Theme.of(context).primaryColor,
                                  value: Tution.college,
                                  groupValue: _tutionType,
                                  onChanged: (Tution value) {
                                    setState(() {
                                      _tutionType = value;
                                    });
                                  },
                                ),
                                const Text(
                                  'College',
                                  style: TextStyle(fontSize: 14),
                                ),
                              ],
                            )
                          : SizedBox(),
                      Container(
                        // height:                                      MediaQuery.of(context).size.height * 0.06,
                        width: double.infinity,
                        padding: EdgeInsets.only(left: 15.0,right: 12),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25.0),
                          border: Border.all(
                              color: _isFDSelected?Theme.of(context).primaryColor:Colors.grey,
                              style: BorderStyle.solid,
                              width: 0.80),
                        ),
                        child: DropdownButton<String>(

                          isExpanded: true,
                          value: dropdownValue,
                          icon: Icon(Icons.arrow_drop_down),
                          iconSize: 30,
                          elevation: 16,
                          
                          // style: _isFDSelected?Theme.of(context).primaryColor:Colors.grey,
                          underline: SizedBox(),
                          onChanged: (String newValue) {
                            setState(() {
                              dropdownValue = newValue;
                              _isFDSelected=true;

                            });
                          },
                          items: <String>['One', 'Two', 'Free', 'Four']
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),

                      CommonButton('Search', onSearch, true),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
