import 'dart:async';

import 'package:ec_student/widgets/layouts/app_bar_back_button.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class UserLocationAndAddress extends StatefulWidget {
  static const routeName = 'user-location-and-address';
  @override
  _UserLocationAndAddressState createState() => _UserLocationAndAddressState();
}

class _UserLocationAndAddressState extends State<UserLocationAndAddress> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _ecGooglePlex = CameraPosition(
    target: LatLng(18.633591, 73.8041518),
    zoom: 14.4746,
  );

  Future<void> _getCurrentLocation() async {
    final locData = await Location().getLocation();

    print(locData.latitude.toString() + ' ' + locData.longitude.toString());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCurrentLocation();
  }

  // static final CameraPosition _cPosition = CameraPosition(
  //     bearing: 192.8334901395799,
  //     target: LatLng(18.633591, 73.8041518),
  //     tilt: 59.440717697143555,
  //     zoom: 19.151926040649414);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: _ecGooglePlex,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
            myLocationEnabled: true,
          ),
          Positioned( 
            top: 60,
            child: SizedBox(
              width: 40,
              child: FlatButton(
                padding: EdgeInsets.only(right: 10),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.only(
                    bottomRight: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: Icon(
                  Icons.arrow_back,
                  size: 30.0,
                ),
                color: Theme.of(context).accentColor,
                // textColor: Colors.white,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
          ),
        ],
      ),
      // floatingActionButton: FloatingActionButton.extended(
      //   onPressed: _goToTheLake,
      //   label: Text('To the lake!'),
      //   icon: Icon(Icons.directions_boat),
      // ),
    );
  }

  // Future<void> _goToTheLake() async {
  //   final GoogleMapController controller = await _controller.future;
  //   controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  // }
}
