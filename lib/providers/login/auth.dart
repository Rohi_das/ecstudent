
import 'package:http/http.dart' as http;
final String BaseAPI='http://codetentacles-005-site17.htempurl.com/ecapp/public/';

postData(url,data) async {
  final String finalUrl=BaseAPI+url;
  return await http.post(
    finalUrl,
    headers: <String, String>{
      'Content-Type':'application/json',
      'APPKEY':'ecapp@123.'
    },
    body: data,
  );
}

