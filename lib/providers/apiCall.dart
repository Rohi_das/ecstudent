import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
final String baseAPI='http://codetentacles-005-site17.htempurl.com/ecapp/public/';


postData(url, data, tokenReq) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final String finalUrl = baseAPI + url;
  print( prefs.getString('token'));
  var postData;
  if (tokenReq) {
    postData = jsonDecode(data);
    postData['token'] = prefs.getString('token') ?? '';
    postData=jsonEncode(postData);
  }else{
    postData=data;
  }
  print(postData);
  return await http.post(
    finalUrl,
    headers: <String, String>{
      'Content-Type':'application/json',
      'APPKEY':'ecapp@123.'
    },
    body: postData,
  );
}






