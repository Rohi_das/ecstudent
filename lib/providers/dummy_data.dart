import 'package:ec_student/models/search/search_categories.dart';
import 'package:ec_student/models/search/search_type.dart';
import 'package:ec_student/models/user_info/user_loacation.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../models/booking_history/def.dart';
import '../models/booking_history/history.dart';
import '../models/help_support/help_support.dart';
import '../models/payments/payment_type.dart';
import '../models/payments/payments.dart';
import '../models/watch_videos/useful_info.dart';
import 'package:flutter/material.dart';
import '../models/drawer/drawer_list.dart';
import '../models/home/additional_categories.dart';
import '../models/home/best_offers.dart';
import '../models/home/enhance_knowledge.dart';
import '../models/home/explore.dart';
import '../models/home/help_yourself.dart';
import '../models/home/news_flash.dart';
import '../models/home/refer_earn.dart';
import '../models/home/testimonial.dart';
import '../models/home/videos.dart';
import '../models/home/categories.dart';

var categories = [
  Categories(cId: '1', categoryName: 'Tuition', icon: 'add_business_outlined'),
  Categories(
      cId: '2', categoryName: 'Hobby Classes', icon: 'add_business_outlined'),
  Categories(
    cId: '3',
    categoryName: 'Entrance Exam Coaching',
    icon: 'add_business_outlined',
  ),
  Categories(
    cId: '4',
    categoryName: 'Competitive Exam Coaching',
    icon: 'add_business_outlined',
  ),
  Categories(
    cId: '5',
    categoryName: 'Career Counselling',
    icon: 'add_business_outlined',
  ),
  Categories(
    cId: '6',
    categoryName: 'Sports Training',
    icon: 'add_business_outlined',
  ),
  Categories(
    cId: '7',
    categoryName: 'Fitness Training',
    icon: 'add_business_outlined',
  ),
  Categories(
    cId: '8',
    categoryName: 'Admission Assistance',
    icon: 'add_business_outlined',
  ),
  Categories(
    cId: '9',
    categoryName: 'Other',
    icon: 'add_business_outlined',
  ),
];

var additionalCategories = [
  AdditionalCategory(
      adId: '1', adName: 'Join Coaching Classes', image: 'assets/login.jpg'),
  AdditionalCategory(adId: '2', adName: 'Buy Books', image: 'assets/login.jpg'),
  AdditionalCategory(adId: '3', adName: 'Buy Notes', image: 'assets/login.jpg'),
  AdditionalCategory(
      adId: '4', adName: 'Test Series', image: 'assets/login.jpg'),
  AdditionalCategory(
      adId: '5', adName: 'Crash Courses', image: 'assets/login.jpg'),
  AdditionalCategory(adId: '6', adName: 'Others', image: 'assets/login.jpg'),
];

var explore = [
  Explore(
      eId: '1',
      exploreName: 'Download Notes',
      exploreImage: 'assets/vector.jpg'),
  Explore(
      eId: '2',
      exploreName: 'Question Papers',
      exploreImage: 'assets/vector-1.jpg'),
  Explore(
      eId: '3',
      exploreName: 'Download e-Books',
      exploreImage: 'assets/vector-2.jpg'),
  Explore(
      eId: '1',
      exploreName: 'Download Notes',
      exploreImage: 'assets/vector.jpg'),
  Explore(
      eId: '2',
      exploreName: 'Question Papers',
      exploreImage: 'assets/vector-1.jpg'),
  Explore(
      eId: '3',
      exploreName: 'Download e-Books',
      exploreImage: 'assets/vector-2.jpg'),
];

var newsFlash = [
  NewsFlash(nfId: '1', newsDetails: '', newsImage: 'assets/banner.jpg'),
  NewsFlash(nfId: '2', newsDetails: '', newsImage: 'assets/banner-re.jpg'),
  NewsFlash(nfId: '3', newsDetails: '', newsImage: 'assets/banner.jpg'),
  NewsFlash(nfId: '4', newsDetails: '', newsImage: 'assets/banner-re.jpg'),
];

var helpYourSelf = [
  HelpYourself(
      hyId: '1', hyName: 'Earn While you Learn', hyImage: 'assets/help.jpg'),
  HelpYourself(
      hyId: '2', hyName: 'Build your Career', hyImage: 'assets/help-1.jpeg'),
  HelpYourself(
      hyId: '3',
      hyName: 'How to Attend Job Interview',
      hyImage: 'assets/help-2.jpeg'),
  HelpYourself(hyId: '4', hyName: 'Get a Mentor', hyImage: 'assets/help.jpg'),
  HelpYourself(
      hyId: '5', hyName: 'Seminar & Workshops', hyImage: 'assets/help-1.jpeg'),
  HelpYourself(
      hyId: '6',
      hyName: 'Skill Enhancement for Students ',
      hyImage: 'assets/help-2.jpeg'),
];

var referEarn = [
  ReferEarn(
      reID: '1',
      reName: 'asd',
      reImage: 'assets/refer.jpg',
      exDate: '',
      reCode: ''),
  ReferEarn(
      reID: '2',
      reName: 'asd',
      reImage: 'assets/refer-1.jpg',
      exDate: '',
      reCode: ''),
  ReferEarn(
      reID: '3',
      reName: 'asd',
      reImage: 'assets/refer-2.jpg',
      exDate: '',
      reCode: ''),
];

var videos = [
  Videos(videoId: '1', videoTitle: '', videoDetails: 'assets/login.jpg'),
  Videos(videoId: '2', videoTitle: '', videoDetails: 'assets/login.jpg'),
  Videos(videoId: '3', videoTitle: '', videoDetails: 'assets/login.jpg'),
  Videos(videoId: '4', videoTitle: '', videoDetails: 'assets/login.jpg'),
];

var bestOffers = [
  BestOffers(boId: '1', boTitle: '', image: 'assets/best-offer.png'),
  BestOffers(boId: '2', boTitle: '', image: 'assets/best-offer-2.jpeg'),
  BestOffers(boId: '3', boTitle: '', image: 'assets/best-offer.png'),
  BestOffers(boId: '4', boTitle: '', image: 'assets/best-offer-2.jpeg'),
];

var enhanceKnowledge = [
  EnhanceKnowledge(
      ekId: '1', title: 'Brain games', image: 'assets/enhance.jpg'),
  EnhanceKnowledge(
      ekId: '2', title: 'Dictionary', image: 'assets/enhance-1.jpg'),
  EnhanceKnowledge(
      ekId: '3', title: 'Fun Academy', image: 'assets/enhance-2.jpeg'),
        EnhanceKnowledge(
      ekId: '1', title: 'Brain games', image: 'assets/enhance.jpg'),
  EnhanceKnowledge(
      ekId: '2', title: 'Dictionary', image: 'assets/enhance-1.jpg'),
  EnhanceKnowledge(
      ekId: '3', title: 'Fun Academy', image: 'assets/enhance-2.jpeg'),
  // EnhanceKnowledge(ekId: '4', title: 'Parenting', image: 'assets/enhance.jpg'),
];

var testimonial = [
  Testimonial(
      testId: '1',
      name: 'Raj',
      image: 'assets/help.jpg',
      rating: '4',
      info: 'asd asd asd asd'),
  Testimonial(
      testId: '2',
      name: 'Raj',
      image: 'assets/help.jpg',
      rating: '4',
      info: 'asd asd asd asd'),
  Testimonial(
      testId: '3',
      name: 'Raj',
      image: 'assets/help.jpg',
      rating: '4',
      info: 'asd asd asd asd'),
  Testimonial(
      testId: '4',
      name: 'Raj',
      image: 'assets/help.jpg',
      rating: '4',
      info: 'asd asd asd asd'),
];

var drawer = [
  DrawerList(
      title: 'Booking History',
      icon: Icons.history,
      tag: false,
      routeName: '/booking'),
  DrawerList(
      title: 'Payments',
      icon: Icons.account_balance_wallet,
      tag: false,
      routeName: '/payments'),
  DrawerList(
      title: 'Watch Videos',
      icon: Icons.video_library,
      tag: false,
      routeName: '/useful-information'),
  DrawerList(
      title: 'Rewards & Offers',
      icon: Icons.local_offer,
      tag: false,
      routeName: '/rewards-and-offers'),
  DrawerList(
      title: 'Rate Us', icon: Icons.star_half, tag: false, routeName: '/data-not-found'),
  DrawerList(
      title: 'Refer & Earn',
      icon: Icons.group_work,
      tag: false,
      routeName: '/refer-and-earn'),
  DrawerList(
      title: 'Help & Support',
      icon: Icons.help,
      tag: false,
      routeName: '/help-and-support'),
  DrawerList(
      title: 'About Us', icon: Icons.description, tag: false, routeName: '/about-us'),
  DrawerList(
      title: 'Privacy Policy',
      icon: Icons.security,
      tag: false,
      routeName: '/privacy-policy'),
  DrawerList(
      title: 'Term of Use', icon: Icons.note, tag: false, routeName: '/term-of-use'),
];

List a = [
  "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  "https://cdn.pixabay.com/photo/2016/05/05/02/37/sunset-1373171_960_720.jpg",
  "https://cdn.pixabay.com/photo/2017/02/01/22/02/mountain-landscape-2031539_960_720.jpg",
  "https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg",
  "https://cdn.pixabay.com/photo/2016/08/09/21/54/yellowstone-national-park-1581879_960_720.jpg",
];

var usefulInfo = [
  UsefulInfo(
      uId: '1',
      image: 'assets/review.png',
      title: 'User Review',
      videoInfo: userReviews),
  UsefulInfo(
      uId: '2',
      image: 'assets/triangle.png',
      title: 'Orientation',
      videoInfo: orientation),
  UsefulInfo(
      uId: '3',
      image: 'assets/choice.png',
      title: "Do's & Dont's",
      videoInfo: doDont),
  UsefulInfo(
      uId: '4',
      image: 'assets/video.png',
      title: 'All Videos',
      videoInfo: allVideos),
  UsefulInfo(
      uId: '5',
      image: 'assets/enhance.jpg',
      title: 'Prepare for Exam',
      videoInfo: exam),
  UsefulInfo(
      uId: '6',
      image: 'assets/info.png',
      title: 'Help For Students',
      videoInfo: help),
];

var userReviews = [
  VideoInfo(
    vId: '1',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '2',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '3',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '4',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '5',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
];

var orientation = [
  VideoInfo(
    vId: '1',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '2',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '3',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '4',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '5',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
];
var doDont = [
  VideoInfo(
    vId: '1',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '2',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '3',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '4',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '5',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
];
var allVideos = [
  VideoInfo(
    vId: '1',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '2',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '3',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '4',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '5',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
];
var exam = [
  VideoInfo(
    vId: '1',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '2',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '3',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '4',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '5',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
];
var help = [
  VideoInfo(
    vId: '1',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '2',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '3',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '4',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
  VideoInfo(
    vId: '5',
    url:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
  ),
];

var faq = [
  HelpSupport(
      hsId: '1',
      question: "What's the benefit of registering on eC App",
      answer:
          'sdfhsndfmsdfksfjmscskjskckckslmkckjhk sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '2',
      question: "What's the benefit of registering on eC App",
      answer: 'sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '3',
      question: "What's the benefit of registering on eC App",
      answer:
          'sdfhsndfmsdfksfjmscskjskckckslmkckjhk sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '4',
      question: "What's the benefit of registering on eC App",
      answer: 'sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '5',
      question: "What's the benefit of registering on eC App",
      answer: 'sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '6',
      question: "What's the benefit of registering on eC App",
      answer: 'sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '7',
      question: "What's the benefit of registering on eC App",
      answer: 'sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '8',
      question: "What's the benefit of registering on eC App",
      answer: 'sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '9',
      question: "What's the benefit of registering on eC App",
      answer: 'sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '10',
      question: "What's the benefit of registering on eC App",
      answer:
          'sdfhsndfmsdfksfjmscskjskckckslmkckjhk sdfhsndfmsdfksfjmscskjskckckslmkckjhk sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '11',
      question: "What's the benefit of registering on eC App",
      answer: 'sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
  HelpSupport(
      hsId: '12',
      question: "What's the benefit of registering on eC App",
      answer: 'sdfhsndfmsdfksfjmscskjskckckslmkckjhk',
      isExpanded: false),
];

const List launchUrl = [
  'https://www.youtube.com/channel/UCjZtGAJn-JsYpguZZPKxCEQ?view_as=subscriber',
  'https://www.facebook.com/educationChamp/',
  'https://www.instagram.com/educationchamp',
  'https://twitter.com/ChampEducation',
  'https://in.pinterest.com/educationchamp',
  'https://www.linkedin.com/company/educationchamp'
];

var currentTeacher=[
  BookingHistory(
      studentName: 'Nayan Swami',
      image:
          'https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg',
      subject: 'Physics',
      classType: 'Online',
      endDate: '30 Sep 2020',
      days: 'Mon Wed Fri',
      time: '6-7 PM',
      status: 'confirmed'),
];

var pastTeachers=[
  BookingHistory(
      studentName: 'Nayan Swami',
      image:
          'https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg',
      subject: 'Physics',
      classType: 'Online',
      endDate: '30 Sep 2020',
      days: 'Mon Wed Fri',
      time: '6-7 PM',
      status: 'confirmed'),
        BookingHistory(
      studentName: 'Nayan Swami',
      image:
          'https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg',
      subject: 'Physics',
      classType: 'Online',
      endDate: '30 Sep 2020',
      days: 'Mon Wed Fri',
      time: '6-7 PM',
      status: 'confirmed'),
  
];

var bookingHistory = [
  BookingHistory(
      studentName: 'Nayan Swami',
      image:
          'https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg',
      subject: 'Physics',
      classType: 'Online',
      endDate: '30 Sep 2020',
      days: 'Mon Wed Fri',
      time: '6-7 PM',
      status: 'confirmed'),
  BookingHistory(
      studentName: 'Nayan Swami',
      image:
          'https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg',
      subject: 'Physics',
      classType: 'Online',
      endDate: '30 Sep 2020',
      days: 'Mon Wed Fri',
      time: '6-7 PM',
      status: 'rejected'),
  BookingHistory(
      studentName: 'Nayan Swami',
      image:
          'https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg',
      subject: 'Physics',
      classType: 'Online',
      endDate: '30 Sep 2020',
      days: 'Mon Wed Fri',
      time: '6-7 PM',
      status: 'confirmed'),
  BookingHistory(
      studentName: 'Nayan Swami',
      image:
          'https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg',
      subject: 'Physics',
      classType: 'Online',
      endDate: '30 Sep 2020',
      days: 'Mon Wed Fri',
      time: '6-7 PM',
      status: 'ongoing'),
  BookingHistory(
      studentName: 'Nayan Swami',
      image:
          'https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg',
      subject: 'Physics',
      classType: 'Online',
      endDate: '30 Sep 2020',
      days: 'Mon Wed Fri',
      time: '6-7 PM',
      status: 'confirmed'),
];

   var operationList = [
    DEF(title: 'All', status: 'all'),
   DEF(title: 'Demo Pending & Confirm', status: 'confirmed'),
   DEF(title: 'Ongoing', status: 'ongoing'),
   DEF(title: 'Conmirmed', status: 'confirmed'),

];

var payments=[
  Payments(txId: '000000123465', orderId: 'EC101', name: 'Shubham Survase',paymentType:'payment', paymentMode: 'Credit Card/Debit Card', status: 'Paid', amount: '100', date: '2 Oct 2020', time: '8:24 PM', subject: 'CBSE-Mathematics'),
  Payments(txId: '000000123466', orderId: 'EC102', name: 'Sawta Akhade',paymentType:'payment', paymentMode: 'Credit Card/Debit Card', status: 'Failed', amount: '10', date: '12 Oct 2020', time: '7:24 PM', subject: 'ICSE-English'),
  Payments(txId: '000000123467', orderId: 'EC103', name: 'Shubham Survase',paymentType:'refund', paymentMode: 'UPI', status: 'Refund', amount: '100', date: '12 Oct 2020', time: '8:42 PM', subject: 'CBSE-Mathematics'),
  Payments(txId: '000000123468', orderId: 'EC104', name: 'Sanket Darekar',paymentType:'credit', paymentMode: '', status: 'Points', amount: '100', date: '13 Oct 2020', time: '8:24 PM', subject: 'Referal Points'),
  Payments(txId: '000000123469', orderId: 'EC105', name: 'Rajkumar Bhagwati',paymentType:'credit', paymentMode: '', status: 'Points', amount: '100', date: '23 Oct 2020', time: '8:24 PM', subject: 'Referal Points'),
  
];

var paymentType=[
  PaymentType(title: 'All', type: 'all'),
  PaymentType(title: 'Payments Made', type: 'payment'),
  PaymentType(title: 'Credits Received', type: 'credit'),
  PaymentType(title: 'Refund', type: 'refund'),
];

var searchResult=[
  Payments(txId: '000000123465', orderId: 'EC101', name: 'Shubham Survase',paymentType:'payment', paymentMode: 'Credit Card/Debit Card', status: 'Paid', amount: '100', date: '2 Oct 2020', time: '8:24 PM', subject: 'CBSE-Mathematics'),
  Payments(txId: '000000123466', orderId: 'EC102', name: 'Sawta Akhade',paymentType:'payment', paymentMode: 'Credit Card/Debit Card', status: 'Failed', amount: '10', date: '12 Oct 2020', time: '7:24 PM', subject: 'ICSE-English'),
  Payments(txId: '000000123467', orderId: 'EC103', name: 'Shubham Survase',paymentType:'refund', paymentMode: 'UPI', status: 'Refund', amount: '100', date: '12 Oct 2020', time: '8:42 PM', subject: 'CBSE-Mathematics'),
  Payments(txId: '000000123468', orderId: 'EC104', name: 'Sanket Darekar',paymentType:'credit', paymentMode: '', status: 'Points', amount: '100', date: '13 Oct 2020', time: '8:24 PM', subject: 'Referal Points'),
  Payments(txId: '000000123469', orderId: 'EC105', name: 'Rajkumar Bhagwati',paymentType:'credit', paymentMode: '', status: 'Points', amount: '100', date: '23 Oct 2020', time: '8:24 PM', subject: 'Referal Points'),
  
];

var searchType=[
  SearchType(title: 'All', type: 'all'),
  SearchType(title: 'Online', type: 'online'),
  SearchType(title: 'Student Place', type: 'student_place'),
  SearchType(title: 'Tutor/Teacher Place', type: 'tutor_teacher_place'),
];

var searchCategories=[
  SearchCategories(title: 'Tuition',icon:FontAwesomeIcons.userGraduate , routeName: '',isMaterialIcon:false),
  SearchCategories(title: 'Hobby Classes',icon:FontAwesomeIcons.palette , routeName: '',isMaterialIcon:false),
  SearchCategories(title: 'Enterance Exam Coaching',icon: FontAwesomeIcons.edit, routeName: '',isMaterialIcon:false),
  SearchCategories(title: 'Competitive Exam Coaching',icon:FontAwesomeIcons.edit , routeName: '',isMaterialIcon:false),
  SearchCategories(title: 'Career Counselling',icon:FontAwesomeIcons.search , routeName: '',isMaterialIcon:false),
  SearchCategories(title: 'Sports Training',icon: FontAwesomeIcons.futbol, routeName: '',isMaterialIcon:false),
  SearchCategories(title: 'Fitness Training',icon:FontAwesomeIcons.futbol , routeName: '',isMaterialIcon:false),
  SearchCategories(title: 'Admission Assistance',icon:FontAwesomeIcons.idCard , routeName: '',isMaterialIcon:false),
  SearchCategories(title: 'Other',icon:FontAwesomeIcons.ellipsisH , routeName: '',isMaterialIcon:false),

];

var userLocaion=[
  UserLocation(id: 'U1', title: 'A', location:Place(latitude:19.020840 , longitude:73.039490 , address: 'asd'),),
  UserLocation(id: 'U2', title: 'B', location:Place(latitude:19.03, longitude:73.01 , address: 'qwe'),),
  UserLocation(id: 'U3', title: 'C', location:Place(latitude:19.021 , longitude:73.0490 , address: 'zxc'),),
  UserLocation(id: 'U4', title: 'D', location:Place(latitude:19.04 , longitude:73.03 , address: 'sdc'),),
  UserLocation(id: 'U5', title: 'E', location:Place(latitude:19.02 , longitude:73.02 , address: 'lkj'),),
];
