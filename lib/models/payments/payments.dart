import 'package:flutter/material.dart';

class Payments {
  final String txId;
  final String orderId;
  final String name;
  final String paymentType;
  final String paymentMode;
  final String status;
  final String amount;
  final String date;
  final String time;
  final String subject;

  Payments({
    @required this.txId,
    @required this.orderId,
    @required this.name,
    @required this.paymentType,
    @required this.paymentMode,
    @required this.status,
    @required this.amount,
    @required this.date,
    @required this.time,
    @required this.subject,
  });
}
