import 'package:flutter/foundation.dart';

class PaymentType{
  final String title;
  final String type;

  PaymentType({@required this.title, @required this.type});
}