import 'package:flutter/foundation.dart';

class BookingHistory {
  final String studentName;
  final String image;
  final String subject;
  final String classType;
  final String endDate;
  final String days;
  final String time;
  final String status;

  BookingHistory(
      {@required this.studentName,
      @required this.image,
      @required this.subject,
      @required this.classType,
      @required this.endDate,
      @required this.days,
      @required this.time,
      @required this.status});
}
