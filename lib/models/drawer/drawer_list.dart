import 'package:flutter/material.dart';
class DrawerList{
  final String title;
  final IconData icon;
  final bool tag;
  final String routeName; 
  DrawerList({@required this.title, @required this.icon, @required this.tag, @required this.routeName});
}