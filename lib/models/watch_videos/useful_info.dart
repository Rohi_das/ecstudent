import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class UsefulInfo{
  final String uId; 
  final String image;
  final String title;
  final List videoInfo;

  UsefulInfo({@required this.uId, @required this.image, @required this.title, this.videoInfo});
}

class VideoInfo{
  final String vId; 
  final String url;

  VideoInfo({@required this.vId, @required this.url});
}