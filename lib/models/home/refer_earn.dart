import 'package:flutter/foundation.dart';

class ReferEarn{
  final String reID;
  final String reName;
  final String reImage;
  final String exDate;
  final String reCode;

  ReferEarn({ @required this.reID, @required this.reName, @required this.reImage, @required this.exDate, @required this.reCode,});
}