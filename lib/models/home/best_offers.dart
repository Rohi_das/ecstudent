import 'package:flutter/foundation.dart';

class BestOffers{
  final String boId;
  final String boTitle;
  final String image;

  BestOffers({ @required this.boId, @required this.boTitle, @required this.image,});
}