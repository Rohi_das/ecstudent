import 'package:flutter/foundation.dart';

class Testimonial{
  final String testId;
  final String name;
  final String image;
  final String rating;
  final String info;

  Testimonial({ @required this.testId, @required this.name, @required this.image, @required this.rating, @required this.info,});
}