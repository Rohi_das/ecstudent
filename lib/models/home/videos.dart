import 'package:flutter/foundation.dart';

class Videos{
  final String videoId;
  final String videoTitle;
  final String videoDetails;

  Videos({ @required this.videoId, @required this.videoTitle, @required this.videoDetails});
}