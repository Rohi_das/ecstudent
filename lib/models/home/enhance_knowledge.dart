import 'package:flutter/foundation.dart';
class EnhanceKnowledge{
  final String ekId;
  final String title;
  final String image;

  EnhanceKnowledge({@required this.ekId, @required this.title, @required this.image});
}