import 'package:flutter/foundation.dart';

class NewsFlash{
  final String nfId;
  final String newsDetails;
  final String newsImage;

  NewsFlash({ @required this.nfId, @required this.newsDetails, @required this.newsImage, });
}