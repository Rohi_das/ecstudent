import 'package:flutter/foundation.dart';

class HelpYourself{
  final String hyId;
  final String hyName;
  final String hyImage;

  HelpYourself({ @required this.hyId, @required this.hyName, @required this.hyImage,});
}