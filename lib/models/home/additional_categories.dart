import 'package:flutter/foundation.dart';

class AdditionalCategory{
  final String adId;
  final String adName;
  final String image;

  AdditionalCategory({@required this.adId,@required this.adName, @required this.image, });
}