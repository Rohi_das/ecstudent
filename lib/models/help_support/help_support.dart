import 'package:flutter/material.dart';

class HelpSupport{
  final String hsId;
  final String question;
  final String answer;
  bool isExpanded;

  HelpSupport({ @required this.hsId, @required this.question, @required this.answer, @required this.isExpanded });
}