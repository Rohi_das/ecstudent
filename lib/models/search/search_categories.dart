import 'package:ec_student/screens/search_screen/search_categories.dart';
import 'package:flutter/material.dart';

class SearchCategories{
  final String title;
  final IconData icon;
  final String routeName;
  final bool isMaterialIcon;

  SearchCategories({ @required this.title, @required this.icon, @required this.routeName, @required this.isMaterialIcon }); 
}