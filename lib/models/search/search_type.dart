import 'package:flutter/foundation.dart';

class SearchType{
  final String title;
  final String type;

  SearchType({@required this.title, @required this.type});
}