import 'package:flutter/foundation.dart';
class Place{
  final double latitude;
  final double longitude;
  final String address;

  Place({@required this.latitude, @required this.longitude, @required this.address});
}

class UserLocation{
  final String id;
  final String title;
  final Place location;

  UserLocation({@required this.id, @required this.title, @required this.location});
}