import 'package:flutter/foundation.dart';

class UserSignup {
  final String name;
  final String mobileNumber;
  final String email;
  final String password;
  final String loginFor;

  UserSignup({
    @required this.name,
    @required this.mobileNumber,
    @required this.email,
    @required this.password,
    @required this.loginFor,
  });

  Map toJson() => {
        'name': name,
        'mobile_number': mobileNumber,
        'email': email,
        'password': password,
        'login_for': loginFor,
      };
}
